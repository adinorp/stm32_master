
#ifndef __UTILITIES_H__
#define __UTILITIES_H__
#include <stdint.h>
#include <stdbool.h>
#include "stm32f4xx_hal.h"

uint32_t DWT_Delay_Init(void);
void DWT_Delay_us(volatile uint32_t microseconds);
void LF__DelayMs(uint8_t delay);




#endif // __UTILITIES_H__