#include "LF_GPIO_Driver.h"
#include "LF_LCD_Driver.h"





/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void LF_GPIO_Init(void)
{

    GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, Boost_EN_PWR_Pin|SD_CD_Pin|INT_1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, VCC_A2_EN_PWR_Pin|LED_OE_Pin|CPU_CPU_IO_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, SPI1_CS_Pin|BL_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : Boost_EN_PWR_Pin SD_CD_Pin INT_1_Pin */
  GPIO_InitStruct.Pin = Boost_EN_PWR_Pin|SD_CD_Pin|INT_1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : CNTR_Pump_Control_Pin */
  GPIO_InitStruct.Pin = CNTR_Pump_Control_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
  HAL_GPIO_Init(CNTR_Pump_Control_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : VCC_A2_EN_PWR_Pin LED_OE_Pin CPU_CPU_IO_Pin */
  GPIO_InitStruct.Pin = VCC_A2_EN_PWR_Pin|LED_OE_Pin|CPU_CPU_IO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : D_SW3_Dip_Switch_Pin */
  GPIO_InitStruct.Pin = D_SW3_Dip_Switch_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(D_SW3_Dip_Switch_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : CNTR_V1_Valve_Control_Pin */
  GPIO_InitStruct.Pin = CNTR_V1_Valve_Control_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
  HAL_GPIO_Init(CNTR_V1_Valve_Control_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : SPI1_CS_Pin BL_LED_Pin */
  GPIO_InitStruct.Pin = SPI1_CS_Pin|BL_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PC10 PC11 PC12 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PB5 PB6 PB7 */
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : D_SW1_Dip_Switch_Pin D_SW2_Dip_Switch_Pin */
  GPIO_InitStruct.Pin = D_SW1_Dip_Switch_Pin|D_SW2_Dip_Switch_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}


/** GPIO Intererupt Handler 
        
        * EXTI
*/

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  
   switch(GPIO_Pin)
   {
     //SW Start
     case GPIO_PIN_10:
     LF__ST7036__SetCursor(0,0);
     LF__ST7036__LCDwrite("Start!");
     break;
     //SW Util1  
     case GPIO_PIN_11:
     LF__ST7036__SetCursor(1,0);
     LF__ST7036__LCDwrite("Sw1!");
     break;
     //SW Util2 
     case GPIO_PIN_12:
     LF__ST7036__LCDclear();
     break;  
       
   }
   
}


 /**
* @brief  Enable Boost
*/

void LF_GPIO_EnableBoost(void)
{
    HAL_GPIO_WritePin(GPIOC, Boost_EN_PWR_Pin, GPIO_PIN_SET);
}

 /**
* @brief  Disable Boost
*/
void LF_GPIO_DisableBoost(void)
{
  HAL_GPIO_WritePin(GPIOC, Boost_EN_PWR_Pin, GPIO_PIN_RESET);
  
}

 /**
* @brief  Enable Analog Supply
*/
void LF_GPIO_EnableAnalogSupply(void)
{
  HAL_GPIO_WritePin(GPIOA, VCC_A2_EN_PWR_Pin, GPIO_PIN_SET);
}


 /**
* @brief  Disable Analog Supply
*/
void LF_GPIO_DisableAnalogSupply(void)
{
  HAL_GPIO_WritePin(GPIOA, VCC_A2_EN_PWR_Pin, GPIO_PIN_RESET);
}



 /**
* @brief  Disable Analog Supply
*/


 /**
* @brief  Read Switch One State
*/
bool LF_GPIO_SwitchOneOn(void)
{
   bool status = false;
   if(HAL_GPIO_ReadPin(GPIOB,D_SW1_Dip_Switch_Pin)==GPIO_PIN_SET)
   {
     status = true;
   }
   return status; 
   
}
 /**
* @brief  Read Switch Two State
*/
bool LF_GPIO_SwitchTwoOn(void)
{
   bool status = false;
   if(HAL_GPIO_ReadPin(GPIOB,D_SW2_Dip_Switch_Pin)==GPIO_PIN_SET)
   {
     status = true;
   }
   return status;
  
}
 /**
* @brief  Read Switch Three State
*/
bool LF_GPIO_SwitchThreeOn(void)
{
  bool status = false;
   if(HAL_GPIO_ReadPin(GPIOA,D_SW3_Dip_Switch_Pin)==GPIO_PIN_SET)
   {
     status = true;
   }
   return status;
  
  
}

