#include "LF__ADS1115_Driver.h"
#include "stm32f4xx_hal.h"

extern I2C_HandleTypeDef hi2c1;


#define ADS1115_ADDRESS 0x90
unsigned char ADSwrite[6];
int16_t reading;
float voltage[4];
const float voltageConv = 6.114 / 32768.0;

HAL_StatusTypeDef Status;



static uint8_t   m_i2cAddress = ADS1015_ADDRESS;
static uint8_t   m_conversionDelay;
static uint8_t   m_bitShift;
static adsGain_t m_gain;



/*
External DAC 

*/

uint8_t mcp4725_address = 0xC0; //0xC4 - other address
uint8_t mcp4725_read = 0x01;
uint8_t mcp4725_dac = 0x40;  // Writes data to the DAC
uint8_t mcp4725_dac_eeprom = 0x60;  // Writes data to the DAC and the EEPROM (persisting the assigned value after reset)
uint8_t buffer[3]={0x00};




 /**
    @brief  Writes 16-bits to the specified destination register
*/
/**************************************************************************/
void LF__ADS115__WriteRegister(uint8_t i2c_address, uint8_t reg, uint16_t value)
{
  
  uint8_t dataBuffer[3]={0};
  dataBuffer[0] = reg;
  dataBuffer[1] = ((uint8_t)(value>>8));
  dataBuffer[2] = ((uint8_t)(value&0xff));
  HAL_I2C_Master_Transmit(&hi2c1,i2c_address, dataBuffer, 3, 1000);
}


/**
    @brief  Read 16 bits from the specified destination register.
*/
/**************************************************************************/
uint16_t LF__ADS115__ReadRegister(uint8_t i2c_address, uint8_t reg)
{
  uint16_t result = 0;  
  uint8_t dataBuffer[3]={0};
  dataBuffer[0] = reg;
  HAL_I2C_Master_Receive(&hi2c1,i2c_address, dataBuffer, 3, 1000);
  result  = dataBuffer[1]<< 8;
  result |= dataBuffer[0];

  return result;
}


/*!
    @brief  Init ADC Parameters
*/
/**************************************************************************/
void LF__ADS115__Init(uint8_t i2cAddress)
{
    // shift 7 bit address 1 left: read expects 8 bit address
    //m_i2cAddress = i2cAddress << 1;
    m_conversionDelay = ADS1115_CONVERSIONDELAY;
    m_bitShift = 0;
    m_gain = GAIN_SIXTEEN; /* +/- 6.144V range (limited to VDD +0.3V max!) */
    
}
 
/**************************************************************************/
/*!
    @brief  Sets the gain and input voltage range
*/
/**************************************************************************/
void LF__ADS115__SetGain(adsGain_t gain)
{
    m_gain = gain;
}
 
/**************************************************************************/
/*!
    @brief  Gets a gain and input voltage range
*/
/**************************************************************************/
adsGain_t LF__ADS115__GetGain(void)
{
    return m_gain;
}
 
/**************************************************************************/
/*!
    @brief  Gets a single-ended ADC reading from the specified channel
*/
/**************************************************************************/
uint16_t LF__ADS115__ReadSingleEnded(uint8_t channel)
{
    if (channel > 3) {
        return 0;
    }
 
    // Start with default values
    uint16_t config = ADS1015_REG_CONFIG_CQUE_NONE    | // Disable the comparator (default val)
                      ADS1015_REG_CONFIG_CLAT_NONLAT  | // Non-latching (default val)
                      ADS1015_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
                      ADS1015_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
                      ADS1015_REG_CONFIG_DR_250SPS   | // 1600(ADS1015) or 250(ADS1115) samples per second (default)
                      ADS1015_REG_CONFIG_MODE_SINGLE;   // Single-shot mode (default)
 
    // Set PGA/voltage range
    config |= m_gain;
 
    // Set single-ended input channel
    switch (channel) {
        case (0):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_0;
            break;
        case (1):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_1;
            break;
        case (2):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_2;
            break;
        case (3):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_3;
            break;
    }
 
    // Set 'start single-conversion' bit
    config |= ADS1015_REG_CONFIG_OS_SINGLE;
 
    // Write config register to the ADC
    LF__ADS115__WriteRegister(m_i2cAddress, ADS1015_REG_POINTER_CONFIG, config);
 
    // Wait for the conversion to complete
    HAL_Delay(m_conversionDelay);
 
    // Read the conversion results
    // Shift 12-bit results right 4 bits for the ADS1015
    return LF__ADS115__ReadRegister(m_i2cAddress, ADS1015_REG_POINTER_CONVERT) >> m_bitShift;
}
 
/**************************************************************************/
/*!
    @brief  Reads the conversion results, measuring the voltage
            difference between the P (AIN0) and N (AIN1) input.  Generates
            a signed value since the difference can be either
            positive or negative.
*/
/**************************************************************************/
int16_t LF__ADS115__ReadDifferential_0_1(void)
{
    // Start with default values
    uint16_t config = ADS1015_REG_CONFIG_CQUE_NONE    | // Disable the comparator (default val)
                      ADS1015_REG_CONFIG_CLAT_NONLAT  | // Non-latching (default val)
                      ADS1015_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
                      ADS1015_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
                      ADS1015_REG_CONFIG_DR_1600SPS   | // 1600(ADS1015) or 250(ADS1115) samples per second (default)
                      ADS1015_REG_CONFIG_MODE_SINGLE;   // Single-shot mode (default)
 
    // Set PGA/voltage range
    config |= m_gain;
 
    // Set channels
    config |= ADS1015_REG_CONFIG_MUX_DIFF_0_1;          // AIN0 = P, AIN1 = N
 
    // Set 'start single-conversion' bit
    config |= ADS1015_REG_CONFIG_OS_SINGLE;
 
    // Write config register to the ADC
    LF__ADS115__WriteRegister(m_i2cAddress, ADS1015_REG_POINTER_CONFIG, config);
 
    // Wait for the conversion to complete
    HAL_Delay(m_conversionDelay);
 
    // Read the conversion results
    uint16_t res = LF__ADS115__ReadRegister(m_i2cAddress, ADS1015_REG_POINTER_CONVERT) >> m_bitShift;
    if (m_bitShift == 0) 
    {
        return (int16_t)res;
    } 
    else 
    {
        // Shift 12-bit results right 4 bits for the ADS1015,
        // making sure we keep the sign bit intact
        if (res > 0x07FF) 
        {
            // negative number - extend the sign to 16th bit
            res |= 0xF000;
        }
        return (int16_t)res;
    }
}
 
/**************************************************************************/
/*!
    @brief  Reads the conversion results, measuring the voltage
            difference between the P (AIN2) and N (AIN3) input.  Generates
            a signed value since the difference can be either
            positive or negative.
*/
/**************************************************************************/
int16_t LF__ADS115__ReadDifferential_2_3(void)
{
    // Start with default values
    uint16_t config = ADS1015_REG_CONFIG_CQUE_NONE    | // Disable the comparator (default val)
                      ADS1015_REG_CONFIG_CLAT_NONLAT  | // Non-latching (default val)
                      ADS1015_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
                      ADS1015_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
                      ADS1015_REG_CONFIG_DR_1600SPS   | // 1600(ADS1015) or 250(ADS1115) samples per second (default)
                      ADS1015_REG_CONFIG_MODE_SINGLE;   // Single-shot mode (default)
 
    // Set PGA/voltage range
    config |= m_gain;
 
    // Set channels
    config |= ADS1015_REG_CONFIG_MUX_DIFF_2_3;          // AIN2 = P, AIN3 = N
 
    // Set 'start single-conversion' bit
    config |= ADS1015_REG_CONFIG_OS_SINGLE;
 
    // Write config register to the ADC
    LF__ADS115__WriteRegister(m_i2cAddress, ADS1015_REG_POINTER_CONFIG, config);
 
    // Wait for the conversion to complete
    HAL_Delay(m_conversionDelay);
 
    // Read the conversion results
    uint16_t res = LF__ADS115__ReadRegister(m_i2cAddress, ADS1015_REG_POINTER_CONVERT) >> m_bitShift;
    if (m_bitShift == 0) 
    {
        return (int16_t)res;
    } 
    else 
    {
        // Shift 12-bit results right 4 bits for the ADS1015,
        // making sure we keep the sign bit intact
        if (res > 0x07FF) 
        {
            // negative number - extend the sign to 16th bit
            res |= 0xF000;
        }
        return (int16_t)res;
    }
}
 
/**************************************************************************/
/*!
    @brief  Sets up the comparator to operate in basic mode, causing the
            ALERT/RDY pin to assert (go from high to low) when the ADC
            value exceeds the specified threshold.
 
            This will also set the ADC in continuous conversion mode.
*/
/**************************************************************************/
void LF__ADS115__StartComparatorSingleEnded(uint8_t channel, int16_t threshold)
{
    // Start with default values
    uint16_t config = ADS1015_REG_CONFIG_CQUE_1CONV   | // Comparator enabled and asserts on 1 match
                      ADS1015_REG_CONFIG_CLAT_LATCH   | // Latching mode
                      ADS1015_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
                      ADS1015_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
                      ADS1015_REG_CONFIG_DR_1600SPS   | // 1600(ADS1015) or 250(ADS1115) samples per second (default)
                      ADS1015_REG_CONFIG_MODE_CONTIN  | // Continuous conversion mode
                      ADS1015_REG_CONFIG_MODE_CONTIN;   // Continuous conversion mode
 
    // Set PGA/voltage range
    config |= m_gain;
 
    // Set single-ended input channel
    switch (channel) 
    {
        case (0):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_0;
            break;
        case (1):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_1;
            break;
        case (2):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_2;
            break;
        case (3):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_3;
            break;
    }
 
    // Set the high threshold register
    // Shift 12-bit results left 4 bits for the ADS1015
    LF__ADS115__WriteRegister(m_i2cAddress, ADS1015_REG_POINTER_HITHRESH, threshold << m_bitShift);
 
    // Write config register to the ADC
    LF__ADS115__WriteRegister(m_i2cAddress, ADS1015_REG_POINTER_CONFIG, config);
}
 
/**************************************************************************/
/*!
    @brief  In order to clear the comparator, we need to read the
            conversion results.  This function reads the last conversion
            results without changing the config value.
*/
/**************************************************************************/
int16_t LF__ADS115__GetLastConversionResults(void)
{
    // Wait for the conversion to complete
    HAL_Delay(m_conversionDelay);
 
    // Read the conversion results
    uint16_t res = LF__ADS115__ReadRegister(m_i2cAddress, ADS1015_REG_POINTER_CONVERT) >> m_bitShift;
    if (m_bitShift == 0) 
    {
        return (int16_t)res;
    } 
    else 
    {
        // Shift 12-bit results right 4 bits for the ADS1015,
        // making sure we keep the sign bit intact
        if (res > 0x07FF) 
        {
            // negative number - extend the sign to 16th bit
            res |= 0xF000;
        }
        return (int16_t)res;
    }
}

float LF__ADS115__ReadPressure(void)
{
  
  uint16_t AtoDcount;
  static bool init = false; 
  uint8_t outBuffer[2]={0};
  float pressure= 0.0;
  
  
  if(!init )
  {
    uint8_t dataBuffer[3]={0x1,0x42,0xe3};
    HAL_I2C_Master_Transmit(&hi2c1,m_i2cAddress, dataBuffer, 3, 1000);
    dataBuffer[0] = 0x0;
    HAL_I2C_Master_Transmit(&hi2c1,m_i2cAddress, dataBuffer, 1, 1000);
    init = true; 
  }
  
  
  HAL_I2C_Master_Receive(&hi2c1,m_i2cAddress, outBuffer, 2, 1000);
  AtoDcount = 256*outBuffer[0] + outBuffer[1];
  pressure = (AtoDcount - 1600) * 9.167e-3;
  
  return pressure; 
  
  
}


float LF__ADS115__ReadBattVoltage(void)
{
  
  uint16_t AtoDcount; 
  uint8_t index =0; 
  uint8_t outBuffer[2]={0};
  float voltage= 0.0;
  
  
  uint8_t dataBuffer[3]={0x1,0x60,0xe3};
 
  for(index=0; index<6; index++)
  {
     HAL_I2C_Master_Transmit(&hi2c1,m_i2cAddress, dataBuffer, 3, 1000);
  dataBuffer[0] = 0x0;
  HAL_I2C_Master_Transmit(&hi2c1,m_i2cAddress, dataBuffer, 1, 1000);
  LF__DelayMs(5);
    
    
    
    HAL_I2C_Master_Receive(&hi2c1,m_i2cAddress, outBuffer, 2, 1000);
    AtoDcount = 256*outBuffer[0] + outBuffer[1];
    voltage += 2 * 6.144 * ((float) AtoDcount) / (32768);
      
    
    LF__DelayMs(1);
  }
  
   voltage /= 6;
   voltage += 0.17;  // Offset due to Zener diode on input voltage divider.
  
  return voltage; 
  
  
}

void mcp4725SetVoltage( uint16_t output, char writeEEPROM )
{

  // Clear write bufferfer
  uint32_t i;
  for ( i = 0; i < 3; i++ )
  {
    buffer[i] = 0x00;
  }

  if (writeEEPROM == 1)  // command and config bits  (C2.C1.C0.x.x.PD1.PD0.x)
  {
    buffer[0] = mcp4725_dac_eeprom;
  }
  else
  {
    buffer[0] = mcp4725_dac;
  }
  buffer[1] = (output / 16);       // Upper data bits     (D11.D10.D9.D8.D7.D6.D5.D4)
  buffer[2] = (output % 16) << 4;  // Lower data bits     (D3.D2.D1.D0.x.x.x.x)
  HAL_I2C_Master_Transmit(&hi2c1, mcp4725_address, buffer, 3, 1000);
}

void Read_AllChannels(void)
{
  for( int i = 0; i < 4; i++) 
  {
    ADSwrite[0] = 0x01;
    switch(i) 
    {
	case(0):
	ADSwrite[1] = 0xC1; // 11000001
	break;
	case(1):
	ADSwrite[1] = 0xD1;// 11010001
	break;
	case(2):
	ADSwrite[1] = 0xE1;
	break;
	case(3):
	ADSwrite[1] = 0xF1;
	break;
    }
    ADSwrite[2] = 0x83; // 10000011
    Status = HAL_I2C_Master_Transmit(&hi2c1, m_i2cAddress, ADSwrite, 3, 100);
    ADSwrite[0] = 0x00;
    Status = HAL_I2C_Master_Transmit(&hi2c1, m_i2cAddress, ADSwrite, 1, 100);
    HAL_Delay(20);
    Status = HAL_I2C_Master_Receive(&hi2c1, m_i2cAddress, ADSwrite, 2, 100);
    reading = (ADSwrite[0] << 8 | ADSwrite[1]);
    if(reading < 0 ) 
    {
	reading = 0;
    }
    voltage[i] = reading * voltageConv;
    //memcpy(ADCResult,voltage,4);
}
  
  
  
  
}


