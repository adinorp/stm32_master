#include "LF__LedController__Driver.h"


static void  initialize( void );
static char    pwm_register_access( int port );
static char    current_register_access( int port );

const int       n_of_ports;




void initialize(void )
{
    char init_array[] = 
    {
        AUTO_INCREMENT | REGISTER_START,  //  Command
        0x00, 0x00,                                 //  MODE1, MODE2
        0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,         //  LEDOUT[5:0]
        0x80, 0x00,                                 //  GRPPWM, GRPFREQ
    };

    pwm( ALLPORTS, 0.0 );    
    current( ALLPORTS, 0.1 );    

    write_buffer( init_array, sizeof( init_array ) );
}

void pwm_register_access(int port )
{
   if ( port < n_of_ports )
   {
    return ( PWM_REGISTER_START + port );
   }

    return ( PWMALL );
}

char current_register_access( int port )
{
    if ( port < n_of_ports )
    {
        return ( IREF_REGISTER_START + port );
    }
    return ( IREFALL );
}

int number_of_ports( void )
{
    return ( n_of_ports );
}


 

void reset(void)
{
  uint8_t data = 0x06;
  HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)DEV_ADDRESS, &data, 1, 1000);
 
}



 void pwm1( uint8_t port, float v )
 {
   char   reg_addr;
   reg_addr    = pwm_register_access( port );
   write( reg_addr, (char)(v * 255.0) );
 }


void  pwm2( float *vp )
{
   int     n_of_ports  = number_of_ports();
   char    data[ n_of_ports + 1 ];
   *data    = pwm_register_access( 0 );
 
    for ( int i = 1; i <= n_of_ports; i++ )
    {
        data[ i ]   = (char)(*vp++ * 255.0);
    }  
    write_buffer( data, sizeof( data ) );
 }

 void  current1( float *vP )
 {
    int     n_of_ports  = number_of_ports();
    char    data[ n_of_ports + 1 ];
    
    *data    = pwm_register_access( 0 );
    
    for ( int i = 1; i <= n_of_ports; i++ )
    {
        data[ i ]   = (char)(*vp++ * 255.0);
    }  
    write_buffer( data, sizeof( data ) );
      
 }


  void  current2( uint8_t port, float vp )
  {
    char    reg_addr;
    
    reg_addr    = current_register_access( port );
    write( reg_addr, (char)(v * 255.0) );
  }



    
    void  write( char reg_addr, char data )
    {
      
       uint8_t buffer[2]={0};
       buffer[0] = reg_addr;
       buffer[1] = data;
       HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)Write_Address, buffer, 2, 1000);
            
      
 
      
    }
    void  write_buffer( char *data, int length )
    {
      *data   |= AUTO_INCREMENT;
      HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)Write_Address, data, length, 1000);
    }
    char  read( char reg_addr )
    {
      reg_addr    |= 0x80;
      //i2c.write( address, (char *)(&reg_addr), 1, true );
      HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)Write_Address, reg_addr, 1, 1000);
      i2c.read(  address, data, length );
      
       return ( reg_addr );
    }
    void  read_buffer( char reg_addr, char *data, int length )
    {
      i2c.write( address, (char *)(&reg_addr), 1, true );
      i2c.read(  address, (char *)(&reg_addr), 1 );

     
      
      
    }














/*
static send_i2c(uint8_t *data,uint8_t size)
{
  uint8_t buffer[2]={0};
  buffer[0] = mode;
  buffer[1] = value;
  HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)Write_Address, buffer, 2, 1000);
 
  
}*/