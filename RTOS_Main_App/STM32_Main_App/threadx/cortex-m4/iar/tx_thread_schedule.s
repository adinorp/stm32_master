;/**************************************************************************/ 
;/*                                                                        */ 
;/*            Copyright (c) 1996-2018 by Express Logic Inc.               */ 
;/*                                                                        */ 
;/*  This software is copyrighted by and is the sole property of Express   */ 
;/*  Logic, Inc.  All rights, title, ownership, or other interests         */ 
;/*  in the software remain the property of Express Logic, Inc.  This      */ 
;/*  software may only be used in accordance with the corresponding        */ 
;/*  license agreement.  Any unauthorized use, duplication, transmission,  */ 
;/*  distribution, or disclosure of this software is expressly forbidden.  */ 
;/*                                                                        */
;/*  This Copyright notice may not be removed or modified without prior    */ 
;/*  written consent of Express Logic, Inc.                                */ 
;/*                                                                        */ 
;/*  Express Logic, Inc. reserves the right to modify this software        */ 
;/*  without notice.                                                       */ 
;/*                                                                        */ 
;/*  Express Logic, Inc.                     info@expresslogic.com         */
;/*  11423 West Bernardo Court               http://www.expresslogic.com   */
;/*  San Diego, CA  92127                                                  */
;/*                                                                        */
;/**************************************************************************/
;
;
;/**************************************************************************/
;/**************************************************************************/
;/**                                                                       */ 
;/** ThreadX Component                                                     */ 
;/**                                                                       */
;/**   Thread                                                              */
;/**                                                                       */
;/**************************************************************************/
;/**************************************************************************/
;
;
;#define TX_SOURCE_CODE
;
;
;/* Include necessary system files.  */
;
;#include "tx_api.h"
;#include "tx_thread.h"
;#include "tx_timer.h"
;   
        EXTERN  _tx_thread_current_ptr
        EXTERN  _tx_thread_execute_ptr
        EXTERN  _tx_timer_time_slice
        EXTERN  _tx_thread_system_stack_ptr
        EXTERN  _tx_execution_thread_enter
        EXTERN  _tx_execution_thread_exit
        EXTERN  _tx_thread_preempt_disable
;
;
        SECTION `.text`:CODE:NOROOT(2)
        THUMB
;/**************************************************************************/ 
;/*                                                                        */ 
;/*  FUNCTION                                               RELEASE        */ 
;/*                                                                        */ 
;/*    _tx_thread_schedule                               Cortex-M4/IAR     */ 
;/*                                                           5.7          */ 
;/*  AUTHOR                                                                */ 
;/*                                                                        */ 
;/*    William E. Lamie, Express Logic, Inc.                               */ 
;/*                                                                        */ 
;/*  DESCRIPTION                                                           */ 
;/*                                                                        */ 
;/*    This function waits for a thread control block pointer to appear in */ 
;/*    the _tx_thread_execute_ptr variable.  Once a thread pointer appears */ 
;/*    in the variable, the corresponding thread is resumed.               */ 
;/*                                                                        */ 
;/*  INPUT                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  OUTPUT                                                                */ 
;/*                                                                        */ 
;/*    None                                                                */
;/*                                                                        */ 
;/*  CALLS                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */
;/*                                                                        */ 
;/*  CALLED BY                                                             */ 
;/*                                                                        */ 
;/*    _tx_initialize_kernel_enter          ThreadX entry function         */ 
;/*    _tx_thread_system_return             Return to system from thread   */ 
;/*    _tx_thread_context_restore           Restore thread's context       */ 
;/*                                                                        */ 
;/*  RELEASE HISTORY                                                       */ 
;/*                                                                        */ 
;/*    DATE              NAME                      DESCRIPTION             */ 
;/*                                                                        */ 
;/*  10-10-2010     William E. Lamie         Initial Version 5.0           */ 
;/*  07-15-2011     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.1    */ 
;/*  04-15-2012     William E. Lamie         Modified comment(s), added    */ 
;/*                                            generic vector table        */ 
;/*                                            symbols, and added FPU      */ 
;/*                                            support, resulting in       */ 
;/*                                            version 5.2                 */  
;/*  01-01-2014     William E. Lamie         Modified comment(s), removed  */ 
;/*                                            unnecessary code, added     */ 
;/*                                            optimizations, moved FPU    */ 
;/*                                            control field, added        */ 
;/*                                            interrupt protection over   */ 
;/*                                            optional thread exit call,  */ 
;/*                                            and placed conditional      */ 
;/*                                            around WFI instruction,     */ 
;/*                                            resulting in version 5.3    */  
;/*  04-15-2014     William E. Lamie         Modified comment(s), and      */ 
;/*                                            added code to clear the     */ 
;/*                                            current thread pointer to   */ 
;/*                                            avoid race condition in     */ 
;/*                                            context restore's setting   */ 
;/*                                            of the save FPU regs bit,   */ 
;/*                                            resulting in version 5.4    */ 
;/*  09-01-2014     William E. Lamie         Modified comment(s), and      */ 
;/*                                            changed logic to save FPU   */ 
;/*                                            registers on solicited      */ 
;/*                                            return from threads,        */ 
;/*                                            resulting in version 5.5    */ 
;/*  06-01-2017     William E. Lamie         Modified comment(s), added    */ 
;/*                                            automatic VFP save/restore, */ 
;/*                                            added optimizations, and    */ 
;/*                                            changed logic to use PendSV */ 
;/*                                            instead of SVC, resulting   */ 
;/*                                            in version 5.6              */ 
;/*  03-11-2018     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.7    */ 
;/*                                                                        */ 
;/**************************************************************************/
;VOID   _tx_thread_schedule(VOID)
;{
    PUBLIC  _tx_thread_schedule
_tx_thread_schedule:
;
;    /* This function should only ever be called on Cortex-M
;       from the first schedule request. Subsequent scheduling occurs
;       from the PendSV handling routines below. */
;
;    /* Clear the preempt-disable flag to enable rescheduling after initialization on Cortex-M targets.  */
;     
    MOV     r0, #0                                  ; Build value for TX_FALSE
    LDR     r2, =_tx_thread_preempt_disable         ; Build address of preempt disable flag
    STR     r0, [r2, #0]                            ; Clear preempt disable flag
;
;    /* Enable interrupts */
;
    CPSIE   i
;            
;    /* Enter the scheduler for the first time.  */
;
    MOV     r0, #0x10000000                         ; Load PENDSVSET bit
    MOV     r1, #0xE000E000                         ; Load NVIC base
    STR     r0, [r1, #0xD04]                        ; Set PENDSVBIT in ICSR
    NOP                                             ; 
    NOP                                             ; 
    NOP                                             ; 
    NOP                                             ; 
;
;    /* We should never get here - ever!  */
;     
    BKPT    0xEF                                    ; Setup error conditions
    BX      lr                                      ; 
;}
;
;    /* Generic context PendSV handler.  */
;     
    PUBLIC  PendSV_Handler
    PUBLIC  __tx_PendSVHandler
PendSV_Handler:
__tx_PendSVHandler:
;
;    /* Get current thread value and new thread pointer.  */
;       
__tx_ts_handler: 

#ifdef TX_ENABLE_EXECUTION_CHANGE_NOTIFY
;
;    /* Call the thread exit function to indicate the thread is no longer executing.  */
;
    CPSID   i                                       ; Disable interrupts
    PUSH    {r0, lr}                                ; Save LR (and r0 just for alignment)
    BL      _tx_execution_thread_exit               ; Call the thread exit function
    POP     {r0, lr}                                ; Recover LR
    CPSIE   i                                       ; Enable interrupts
#endif
    MOV32   r0, _tx_thread_current_ptr              ; Build current thread pointer address
    MOV32   r2, _tx_thread_execute_ptr              ; Build execute thread pointer address
    MOV     r3, #0                                  ; Build NULL value
    LDR     r1, [r0]                                ; Pickup current thread pointer
;
;    /* Determine if there is a current thread to finish preserving.  */
;       
    CBZ     r1, __tx_ts_new                         ; If NULL, skip preservation
;
;    /* Recover PSP and preserve current thread context.  */
;
    STR     r3, [r0]                                ; Set _tx_thread_current_ptr to NULL
    MRS     r12, PSP                                ; Pickup PSP pointer (thread's stack pointer)
    STMDB   r12!, {r4-r11}                          ; Save its remaining registers
#ifdef __ARMVFP__
    TST     LR, #0x10                               ; Determine if the VFP extended frame is present
    BNE     _skip_vfp_save
    VSTMDB  r12!,{s16-s31}                          ; Yes, save additional VFP registers
_skip_vfp_save:
#endif
    MOV32   r4, _tx_timer_time_slice                ; Build address of time-slice variable
    STMDB   r12!, {LR}                              ; Save LR on the stack
;
;    /* Determine if time-slice is active. If it isn't, skip time handling processing.  */
;
    LDR     r5, [r4]                                ; Pickup current time-slice
    STR     r12, [r1, #8]                           ; Save the thread stack pointer
    CBZ     r5, __tx_ts_new                         ; If not active, skip processing
;
;    /* Time-slice is active, save the current thread's time-slice and clear the global time-slice variable.  */
;
    STR     r5, [r1, #24]                           ; Save current time-slice
;
;    /* Clear the global time-slice.  */
;
    STR     r3, [r4]                                ; Clear time-slice
;
;       
;    /* Executing thread is now completely preserved!!!  */
;
__tx_ts_new:
;
;    /* Now we are looking for a new thread to execute!  */
;
    CPSID   i                                       ; Disable interrupts
    LDR     r1, [r2]                                ; Is there another thread ready to execute?
    CBZ     r1, __tx_ts_wait                        ; No, skip to the wait processing
;
;    /* Yes, another thread is ready for else, make the current thread the new thread.  */
;
    STR     r1, [r0]                                ; Setup the current thread pointer to the new thread
    CPSIE   i                                       ; Enable interrupts
;
;    /* Increment the thread run count.  */
;
__tx_ts_restore:
    LDR     r7, [r1, #4]                            ; Pickup the current thread run count
    MOV32   r4, _tx_timer_time_slice                ; Build address of time-slice variable
    LDR     r5, [r1, #24]                           ; Pickup thread's current time-slice
    ADD     r7, r7, #1                              ; Increment the thread run count
    STR     r7, [r1, #4]                            ; Store the new run count
;
;    /* Setup global time-slice with thread's current time-slice.  */
;
    STR     r5, [r4]                                ; Setup global time-slice

#ifdef TX_ENABLE_EXECUTION_CHANGE_NOTIFY
;
;    /* Call the thread entry function to indicate the thread is executing.  */
;
    PUSH    {r0, r1}                                ; Save r0/r1
    BL      _tx_execution_thread_enter              ; Call the thread execution enter function
    POP     {r0, r1}                                ; Recover r3
#endif
;
;    /* Restore the thread context and PSP.  */
;
    LDR     r12, [r1, #8]                           ; Pickup thread's stack pointer
    LDMIA   r12!, {LR}                              ; Pickup LR
#ifdef __ARMVFP__
    TST     LR, #0x10                               ; Determine if the VFP extended frame is present
    BNE     _skip_vfp_restore                       ; If not, skip VFP restore 
    VLDMIA  r12!, {s16-s31}                         ; Yes, restore additional VFP registers
_skip_vfp_restore:
#endif
    LDMIA   r12!, {r4-r11}                          ; Recover thread's registers
    MSR     PSP, r12                                ; Setup the thread's stack pointer
;
;    /* Return to thread.  */
;       
    BX      lr                                      ; Return to thread!
;
;    /* The following is the idle wait processing... in this case, no threads are ready for execution and the
;       system will simply be idle until an interrupt occurs that makes a thread ready. Note that interrupts 
;       are disabled to allow use of WFI for waiting for a thread to arrive.  */
;
__tx_ts_wait:
    CPSID   i                                       ; Disable interrupts
    LDR     r1, [r2]                                ; Pickup the next thread to execute pointer
    STR     r1, [r0]                                ; Store it in the current pointer
    CBNZ    r1, __tx_ts_ready                       ; If non-NULL, a new thread is ready!
    CPSIE   i                                       ; Enable interrupts
#ifdef TX_ENABLE_WFI
    WFI
#endif
__tx_ts_ISB:
    ISB                                             ; 
    B       __tx_ts_wait                            ; Loop to continue waiting
;
;    /* At this point, we have a new thread ready to go. Clear any newly pended PendSV - since we are 
;       already in the handler!  */
;
__tx_ts_ready:
    MOV     r7, #0x08000000                         ; Build clear PendSV value
    MOV     r8, #0xE000E000                         ; Build base NVIC address
    STR     r7, [r8, #0xD04]                        ; Clear any PendSV 
;
;    /* Re-enable interrupts and restore new thread.  */
;       
    CPSIE   i                                       ; Enable interrupts
    B       __tx_ts_restore                         ; Restore the thread
;}
;
#ifdef __ARMVFP__

    PUBLIC  tx_thread_fpu_enable
tx_thread_fpu_enable:
;
;    /* Automatic VPF logic is supported, this function is present only for 
;       backward compatibility purposes and therefore simply returns.  */
;
    BX      LR                                      ; Return to caller

    PUBLIC  tx_thread_fpu_disable
tx_thread_fpu_disable:
;
;    /* Automatic VPF logic is supported, this function is present only for 
;       backward compatibility purposes and therefore simply returns.  */
;
    BX      LR                                      ; Return to caller

#endif

    END
        
