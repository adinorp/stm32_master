;/**************************************************************************/ 
;/*                                                                        */ 
;/*            Copyright (c) 1996-2018 by Express Logic Inc.               */ 
;/*                                                                        */ 
;/*  This software is copyrighted by and is the sole property of Express   */ 
;/*  Logic, Inc.  All rights, title, ownership, or other interests         */ 
;/*  in the software remain the property of Express Logic, Inc.  This      */ 
;/*  software may only be used in accordance with the corresponding        */ 
;/*  license agreement.  Any unauthorized use, duplication, transmission,  */ 
;/*  distribution, or disclosure of this software is expressly forbidden.  */ 
;/*                                                                        */
;/*  This Copyright notice may not be removed or modified without prior    */ 
;/*  written consent of Express Logic, Inc.                                */ 
;/*                                                                        */ 
;/*  Express Logic, Inc. reserves the right to modify this software        */ 
;/*  without notice.                                                       */ 
;/*                                                                        */ 
;/*  Express Logic, Inc.                     info@expresslogic.com         */
;/*  11423 West Bernardo Court               http://www.expresslogic.com   */
;/*  San Diego, CA  92127                                                  */
;/*                                                                        */
;/**************************************************************************/
;
;
;/**************************************************************************/
;/**************************************************************************/
;/**                                                                       */ 
;/** ThreadX Component                                                     */ 
;/**                                                                       */
;/**   Thread                                                              */
;/**                                                                       */
;/**************************************************************************/
;/**************************************************************************/
;
;
;#define TX_SOURCE_CODE
;
;
;/* Include necessary system files.  */
;
;#include "tx_api.h"
;#include "tx_thread.h"
;#include "tx_timer.h"
;
;
        EXTERN  _tx_execution_isr_exit
;
;
        SECTION `.text`:CODE:NOROOT(2)
        THUMB
;/**************************************************************************/ 
;/*                                                                        */ 
;/*  FUNCTION                                               RELEASE        */ 
;/*                                                                        */ 
;/*    _tx_thread_context_restore                        Cortex-M4/IAR     */ 
;/*                                                           5.7          */ 
;/*  AUTHOR                                                                */ 
;/*                                                                        */ 
;/*    William E. Lamie, Express Logic, Inc.                               */ 
;/*                                                                        */ 
;/*  DESCRIPTION                                                           */ 
;/*                                                                        */ 
;/*    This function restores the interrupt context if it is processing a  */ 
;/*    nested interrupt.  If not, it returns to the interrupt thread if no */ 
;/*    preemption is necessary.  Otherwise, if preemption is necessary or  */ 
;/*    if no thread was running, the function returns to the scheduler.    */ 
;/*                                                                        */ 
;/*  INPUT                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  OUTPUT                                                                */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLS                                                                 */ 
;/*                                                                        */ 
;/*    [_tx_execution_isr_exit]              Execution profiling ISR exit  */ 
;/*                                                                        */ 
;/*  CALLED BY                                                             */ 
;/*                                                                        */ 
;/*    ISRs                                  Interrupt Service Routines    */ 
;/*                                                                        */ 
;/*  RELEASE HISTORY                                                       */ 
;/*                                                                        */ 
;/*    DATE              NAME                      DESCRIPTION             */ 
;/*                                                                        */ 
;/*  10-10-2010     William E. Lamie         Initial Version 5.0           */ 
;/*  07-15-2011     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.1    */ 
;/*  04-15-2012     William E. Lamie         Modified comment(s), and      */ 
;/*                                            added FPU support,          */ 
;/*                                            resulting in version 5.2    */  
;/*  01-01-2014     William E. Lamie         Modified comment(s), moved    */ 
;/*                                            FPU control field, and      */ 
;/*                                            corrected problem saving FPU*/ 
;/*                                            regs without active thread, */ 
;/*                                            resulting in version 5.3    */  
;/*  04-15-2014     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.4    */ 
;/*  09-01-2014     William E. Lamie         Modified comment(s), and      */ 
;/*                                            removed setting FPU save    */
;/*                                            flag from ISR,              */ 
;/*                                            resulting in version 5.5    */ 
;/*  06-01-2017     William E. Lamie         Modified comment(s), and      */ 
;/*                                            changed the code to simply  */ 
;/*                                            return since this function  */ 
;/*                                            is no longer needed,        */ 
;/*                                            resulting in version 5.6    */ 
;/*  03-11-2018     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.7    */ 
;/*                                                                        */ 
;/**************************************************************************/
;VOID   _tx_thread_context_restore(VOID)
;{
    PUBLIC  _tx_thread_context_restore
_tx_thread_context_restore:

#ifdef TX_ENABLE_EXECUTION_CHANGE_NOTIFY
;
;    /* Call the ISR exit function to indicate an ISR is complete.  */
;
    BL      _tx_execution_isr_exit              ; Call the ISR exit function
#endif
;
; /* Preemption has already been addressed - just return!  */
;
    POP     {lr}
    BX      lr
;
;}
        END

