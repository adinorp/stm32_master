;/**************************************************************************/ 
;/*                                                                        */ 
;/*            Copyright (c) 1996-2018 by Express Logic Inc.               */ 
;/*                                                                        */ 
;/*  This software is copyrighted by and is the sole property of Express   */ 
;/*  Logic, Inc.  All rights, title, ownership, or other interests         */ 
;/*  in the software remain the property of Express Logic, Inc.  This      */ 
;/*  software may only be used in accordance with the corresponding        */ 
;/*  license agreement.  Any unauthorized use, duplication, transmission,  */ 
;/*  distribution, or disclosure of this software is expressly forbidden.  */ 
;/*                                                                        */
;/*  This Copyright notice may not be removed or modified without prior    */ 
;/*  written consent of Express Logic, Inc.                                */ 
;/*                                                                        */ 
;/*  Express Logic, Inc. reserves the right to modify this software        */ 
;/*  without notice.                                                       */ 
;/*                                                                        */ 
;/*  Express Logic, Inc.                     info@expresslogic.com         */
;/*  11423 West Bernardo Court               http://www.expresslogic.com   */
;/*  San Diego, CA  92127                                                  */
;/*                                                                        */
;/**************************************************************************/
;
;
;/**************************************************************************/
;/**************************************************************************/
;/**                                                                       */ 
;/** ThreadX Component                                                     */ 
;/**                                                                       */
;/**   Thread                                                              */
;/**                                                                       */
;/**************************************************************************/
;/**************************************************************************/
;
;#define TX_SOURCE_CODE
;
;
;/* Include necessary system files.  */
;
;#include "tx_api.h"
;#include "tx_thread.h"
;#include "tx_timer.h"
;
;
;
        SECTION `.text`:CODE:NOROOT(2)
        THUMB
;/**************************************************************************/ 
;/*                                                                        */ 
;/*  FUNCTION                                               RELEASE        */ 
;/*                                                                        */ 
;/*    _tx_thread_system_return                          Cortex-M4/IAR     */ 
;/*                                                           5.7          */ 
;/*  AUTHOR                                                                */ 
;/*                                                                        */ 
;/*    William E. Lamie, Express Logic, Inc.                               */ 
;/*                                                                        */ 
;/*  DESCRIPTION                                                           */ 
;/*                                                                        */ 
;/*    This function is target processor specific.  It is used to transfer */ 
;/*    control from a thread back to the ThreadX system.  Only a           */ 
;/*    minimal context is saved since the compiler assumes temp registers  */ 
;/*    are going to get slicked by a function call anyway.                 */ 
;/*                                                                        */ 
;/*  INPUT                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  OUTPUT                                                                */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLS                                                                 */ 
;/*                                                                        */ 
;/*    _tx_thread_schedule                   Thread scheduling loop        */ 
;/*                                                                        */ 
;/*  CALLED BY                                                             */ 
;/*                                                                        */ 
;/*    ThreadX components                                                  */ 
;/*                                                                        */ 
;/*  RELEASE HISTORY                                                       */ 
;/*                                                                        */ 
;/*    DATE              NAME                      DESCRIPTION             */ 
;/*                                                                        */ 
;/*  10-10-2010     William E. Lamie         Initial Version 5.0           */ 
;/*  07-15-2011     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.1    */ 
;/*  04-15-2012     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.2    */ 
;/*  01-01-2014     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.3    */ 
;/*  04-15-2014     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.4    */ 
;/*  09-01-2015     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.5    */ 
;/*  06-01-2017     William E. Lamie         Modified comment(s), and      */ 
;/*                                            changed logic to use PendSV */ 
;/*                                            instead of SVC to return to */ 
;/*                                            the scheduler, resulting    */ 
;/*                                            in version 5.6              */ 
;/*  03-11-2018     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.7    */ 
;/*                                                                        */ 
;/**************************************************************************/ 
;VOID   _tx_thread_system_return(VOID)
;{
    PUBLIC  _tx_thread_system_return
_tx_thread_system_return??rA:
_tx_thread_system_return:
;
;    /* Return to real scheduler via PendSV. Note that this routine is often 
;       replaced with in-line assembly in tx_port.h to improved performance.  */
;     
    MOV     r0, #0x10000000                         ; Load PENDSVSET bit
    MOV     r1, #0xE000E000                         ; Load NVIC base
    STR     r0, [r1, #0xD04]                        ; Set PENDSVBIT in ICSR
    MRS     r0, IPSR                                ; Pickup IPSR
    CMP     r0, #0                                  ; Is it a thread returning?
    BNE     _isr_context                            ; If ISR, skip interrupt enable
    MRS     r1, PRIMASK                             ; Thread context returning, pickup PRIMASK
    CPSIE   i                                       ; Enable interrupts
    MSR     PRIMASK, r1                             ; Restore original interrupt posture
_isr_context:
    BX      lr                                      ; Return to caller 
;}
    END  
    
