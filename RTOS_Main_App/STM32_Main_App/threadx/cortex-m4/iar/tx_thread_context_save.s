;/**************************************************************************/ 
;/*                                                                        */ 
;/*            Copyright (c) 1996-2018 by Express Logic Inc.               */ 
;/*                                                                        */ 
;/*  This software is copyrighted by and is the sole property of Express   */ 
;/*  Logic, Inc.  All rights, title, ownership, or other interests         */ 
;/*  in the software remain the property of Express Logic, Inc.  This      */ 
;/*  software may only be used in accordance with the corresponding        */ 
;/*  license agreement.  Any unauthorized use, duplication, transmission,  */ 
;/*  distribution, or disclosure of this software is expressly forbidden.  */ 
;/*                                                                        */
;/*  This Copyright notice may not be removed or modified without prior    */ 
;/*  written consent of Express Logic, Inc.                                */ 
;/*                                                                        */ 
;/*  Express Logic, Inc. reserves the right to modify this software        */ 
;/*  without notice.                                                       */ 
;/*                                                                        */ 
;/*  Express Logic, Inc.                     info@expresslogic.com         */
;/*  11423 West Bernardo Court               http://www.expresslogic.com   */
;/*  San Diego, CA  92127                                                  */
;/*                                                                        */
;/**************************************************************************/
;
;
;/**************************************************************************/
;/**************************************************************************/
;/**                                                                       */ 
;/** ThreadX Component                                                     */ 
;/**                                                                       */
;/**   Thread                                                              */
;/**                                                                       */
;/**************************************************************************/
;/**************************************************************************/
;
;
;#define TX_SOURCE_CODE
;
;
;/* Include necessary system files.  */
;
;#include "tx_api.h"
;#include "tx_thread.h"
;#include "tx_timer.h"
;
;
        EXTERN  _tx_execution_isr_enter
;
;
        SECTION `.text`:CODE:NOROOT(2)
        THUMB
;/**************************************************************************/ 
;/*                                                                        */ 
;/*  FUNCTION                                               RELEASE        */ 
;/*                                                                        */ 
;/*    _tx_thread_context_save                           Cortex-M4/IAR     */ 
;/*                                                           5.7          */ 
;/*  AUTHOR                                                                */ 
;/*                                                                        */ 
;/*    William E. Lamie, Express Logic, Inc.                               */ 
;/*                                                                        */ 
;/*  DESCRIPTION                                                           */ 
;/*                                                                        */ 
;/*    This function saves the context of an executing thread in the       */ 
;/*    beginning of interrupt processing.  The function also ensures that  */ 
;/*    the system stack is used upon return to the calling ISR.            */ 
;/*                                                                        */ 
;/*  INPUT                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  OUTPUT                                                                */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLS                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLED BY                                                             */ 
;/*                                                                        */ 
;/*    ISRs                                                                */ 
;/*                                                                        */ 
;/*  RELEASE HISTORY                                                       */ 
;/*                                                                        */ 
;/*    DATE              NAME                      DESCRIPTION             */ 
;/*                                                                        */ 
;/*  10-10-2010     William E. Lamie         Initial Version 5.0           */ 
;/*  07-15-2011     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.1    */ 
;/*  04-15-2012     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.2    */ 
;/*  01-01-2014     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.3    */ 
;/*  04-15-2014     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.4    */ 
;/*  09-01-2015     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.5    */ 
;/*  06-01-2017     William E. Lamie         Modified comment(s), and      */ 
;/*                                            changed the code to simply  */ 
;/*                                            return since this function  */ 
;/*                                            is no longer needed,        */ 
;/*                                            resulting in version 5.6    */ 
;/*  03-11-2018     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.7    */ 
;/*                                                                        */ 
;/**************************************************************************/ 
;VOID   _tx_thread_context_save(VOID)
;{
    PUBLIC  _tx_thread_context_save
_tx_thread_context_save:
#ifdef TX_ENABLE_EXECUTION_CHANGE_NOTIFY
;
;    /* Call the ISR enter function to indicate an ISR is starting.  */
;   
    PUSH    {lr}                                    ; Save return address
    BL      _tx_execution_isr_enter                 ; Call the ISR enter function
    POP     {lr}                                    ; Recover return address
#endif
;
; /* Context is already saved - just return!  */
;
    BX      lr
;}
    END
        
