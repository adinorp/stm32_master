;/**************************************************************************/ 
;/*                                                                        */ 
;/*            Copyright (c) 1996-2018 by Express Logic Inc.               */ 
;/*                                                                        */ 
;/*  This software is copyrighted by and is the sole property of Express   */ 
;/*  Logic, Inc.  All rights, title, ownership, or other interests         */ 
;/*  in the software remain the property of Express Logic, Inc.  This      */ 
;/*  software may only be used in accordance with the corresponding        */ 
;/*  license agreement.  Any unauthorized use, duplication, transmission,  */ 
;/*  distribution, or disclosure of this software is expressly forbidden.  */ 
;/*                                                                        */
;/*  This Copyright notice may not be removed or modified without prior    */ 
;/*  written consent of Express Logic, Inc.                                */ 
;/*                                                                        */ 
;/*  Express Logic, Inc. reserves the right to modify this software        */ 
;/*  without notice.                                                       */ 
;/*                                                                        */ 
;/*  Express Logic, Inc.                     info@expresslogic.com         */
;/*  11423 West Bernardo Court               http://www.expresslogic.com   */
;/*  San Diego, CA  92127                                                  */
;/*                                                                        */
;/**************************************************************************/
;
;
;/**************************************************************************/
;/**************************************************************************/
;/**                                                                       */ 
;/** ThreadX Component                                                     */ 
;/**                                                                       */
;/**   Initialize                                                          */
;/**                                                                       */
;/**************************************************************************/
;/**************************************************************************/
;
;#define TX_SOURCE_CODE
;
;
;/* Include necessary system files.  */
;
;#include "tx_api.h"
;#include "tx_initialize.h"
;#include "tx_thread.h"
;#include "tx_timer.h"
;
;
        EXTERN  _tx_thread_system_stack_ptr
        EXTERN  _tx_initialize_unused_memory
        EXTERN  _tx_timer_interrupt
        EXTERN  __vector_table
        EXTERN  _tx_execution_isr_enter
        EXTERN  _tx_execution_isr_exit
;
;
SYSTEM_CLOCK      EQU   84000000
SYSTICK_CYCLES    EQU   ((SYSTEM_CLOCK / 1000) -1)
        
    RSEG    FREE_MEM:DATA
    PUBLIC  __tx_free_memory_start
__tx_free_memory_start
    DS32    4        
;
;
        SECTION `.text`:CODE:NOROOT(2)
        THUMB
;/**************************************************************************/ 
;/*                                                                        */ 
;/*  FUNCTION                                               RELEASE        */ 
;/*                                                                        */ 
;/*    _tx_initialize_low_level                          Cortex-M4/IAR     */ 
;/*                                                           5.7          */ 
;/*  AUTHOR                                                                */ 
;/*                                                                        */ 
;/*    William E. Lamie, Express Logic, Inc.                               */ 
;/*                                                                        */ 
;/*  DESCRIPTION                                                           */ 
;/*                                                                        */ 
;/*    This function is responsible for any low-level processor            */ 
;/*    initialization, including setting up interrupt vectors, setting     */ 
;/*    up a periodic timer interrupt source, saving the system stack       */ 
;/*    pointer for use in ISR processing later, and finding the first      */ 
;/*    available RAM memory address for tx_application_define.             */ 
;/*                                                                        */ 
;/*  INPUT                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  OUTPUT                                                                */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLS                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLED BY                                                             */ 
;/*                                                                        */ 
;/*    _tx_initialize_kernel_enter           ThreadX entry function        */ 
;/*                                                                        */ 
;/*  RELEASE HISTORY                                                       */ 
;/*                                                                        */ 
;/*    DATE              NAME                      DESCRIPTION             */ 
;/*                                                                        */ 
;/*  10-10-2010     William E. Lamie         Initial Version 5.0           */ 
;/*  07-15-2011     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.1    */ 
;/*  04-15-2012     William E. Lamie         Modified comment(s), added    */ 
;/*                                            generic vector table        */ 
;/*                                            symbols, and added FPU      */ 
;/*                                            support, resulting in       */ 
;/*                                            version 5.2                 */  
;/*  01-01-2014     William E. Lamie         Modified comment(s), and      */ 
;/*                                            removed unnecessary code in */ 
;/*                                            timer interrupt ISR,        */ 
;/*                                            resulting in version 5.3    */  
;/*  04-15-2014     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.4    */ 
;/*  09-01-2015     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.5    */ 
;/*  06-01-2017     William E. Lamie         Modified comment(s), removed  */ 
;/*                                            VFP control register setup, */ 
;/*                                            and removed default vectors,*/ 
;/*                                            resulting in version 5.6    */ 
;/*  03-11-2018     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 5.7    */ 
;/*                                                                        */ 
;/**************************************************************************/
;VOID   _tx_initialize_low_level(VOID)
;{
    PUBLIC  _tx_initialize_low_level
_tx_initialize_low_level:
;       
;    /* Ensure that interrupts are disabled.  */
;
    CPSID   i                                       ; Disable interrupts
;
;
;    /* Set base of available memory to end of non-initialised RAM area.  */
;     
    LDR     r0, =__tx_free_memory_start             ; Get end of non-initialized RAM area
    LDR     r2, =_tx_initialize_unused_memory       ; Build address of unused memory pointer
    STR     r0, [r2, #0]                            ; Save first free memory address
;
;    /* Enable the cycle count register.  */
;
    LDR     r0, =0xE0001000                         ; Build address of DWT register
    LDR     r1, [r0]                                ; Pickup the current value
    ORR     r1, r1, #1                              ; Set the CYCCNTENA bit
    STR     r1, [r0]                                ; Enable the cycle count register 
;
;    /* Setup Vector Table Offset Register.  */
;    
    MOV     r0, #0xE000E000                         ; Build address of NVIC registers
    LDR     r1, =__vector_table                     ; Pickup address of vector table
    STR     r1, [r0, #0xD08]                        ; Set vector table address   
;
;    /* Set system stack pointer from vector value.  */
;
    LDR     r0, =_tx_thread_system_stack_ptr        ; Build address of system stack pointer
    LDR     r1, =__vector_table                     ; Pickup address of vector table
    LDR     r1, [r1]                                ; Pickup reset stack pointer
    STR     r1, [r0]                                ; Save system stack pointer
;
;    /* Configure SysTick.  */
;
    MOV     r0, #0xE000E000                         ; Build address of NVIC registers
    LDR     r1, =SYSTICK_CYCLES
    STR     r1, [r0, #0x14]                         ; Setup SysTick Reload Value
    MOV     r1, #0x7                                ; Build SysTick Control Enable Value
    STR     r1, [r0, #0x10]                         ; Setup SysTick Control
;
;    /* Configure handler priorities.  */
;
    LDR     r1, =0x00000000                         ; Rsrv, UsgF, BusF, MemM
    STR     r1, [r0, #0xD18]                        ; Setup System Handlers 4-7 Priority Registers

    LDR     r1, =0xFF000000                         ; SVCl, Rsrv, Rsrv, Rsrv
    STR     r1, [r0, #0xD1C]                        ; Setup System Handlers 8-11 Priority Registers
                                                    ; Note: SVC must be lowest priority, which is 0xFF

    LDR     r1, =0x40FF0000                         ; SysT, PnSV, Rsrv, DbgM
    STR     r1, [r0, #0xD20]                        ; Setup System Handlers 12-15 Priority Registers
                                                    ; Note: PnSV must be lowest priority, which is 0xFF
;
;    /* Return to caller.  */
;    
    BX      lr 
;}
;
    PUBLIC  SysTick_Handler
    PUBLIC  __tx_SysTickHandler
__tx_SysTickHandler:
SysTick_Handler:
;
; VOID SysTick_Handler(VOID)
; {
;
    PUSH    {lr}
#ifdef TX_ENABLE_EXECUTION_CHANGE_NOTIFY
    BL      _tx_execution_isr_enter             ; Call the ISR enter function
#endif
    BL      _tx_timer_interrupt
#ifdef TX_ENABLE_EXECUTION_CHANGE_NOTIFY
    BL      _tx_execution_isr_exit              ; Call the ISR exit function
#endif
    POP     {lr}
    BX      LR
; }
    END
                

