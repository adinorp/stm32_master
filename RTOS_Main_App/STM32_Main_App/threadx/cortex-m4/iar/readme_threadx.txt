                          Express Logic's ThreadX for Cortex-M4 

                                   Using the IAR Tools


1.  Installation

ThreadX for the Cortex-M4 is delivered on a single CD-ROM compatible disk. 
The entire distribution can be found in the sub-directory:

\threadx

To install ThreadX to your hard-disk, either run the supplied installer 
program Setup.exe or copy the distribution from the CD manually. 

To copy the ThreadX distribution manually, make a ThreadX directory on your 
hard-disk (we recommend c:\threadx\cortex-m4\iar) and copy all the contents 
of the ThreadX sub-directory on the distribution disk. The following 
is an example MS-DOS copy command from the distribution directory
(assuming source is d: and c: is your hard-drive):


d:\threadx> xcopy /S *.* c:\threadx\cortex-m4\iar


2.  Building the ThreadX run-time Library

Building the ThreadX library is easy. First, open the ThreadX workspace
threadx.eww. Next, make the TX project the "active project" in the 
IAR Embedded Workbench and select the "Make" button. You should observe 
assembly and compilation of a series of ThreadX source files. This 
results in the ThreadX run-time library file tx.a, which is needed by 
the application.


3.  Demonstration System

The ThreadX demonstration is designed to execute under the IAR debugger under 
simulation.

Building the demonstration is easy; simply open the threadx.www workspace file,
make the demo_threadx.ewp project the "active project" in the IAR Embedded 
Workbench, and select the "Make" button.

You should observe the compilation of demo_threadx.c (which is the demonstration 
application) and linking with tx.a. The resulting file demo_threadx.out is a 
binary ELF file that can be downloaded and executed on the IAR Windows-based 
Cortex-M4 simulator.


4.  System Initialization

The entry point in ThreadX for the Cortex-M4 using IAR tools is at label 
__iar_program_start. This is defined within the IAR compiler's startup code. 
In addition, this is where all static and global preset C variable 
initialization processing takes place.

The ThreadX tx_initialize_low_level.s file is responsible for setting up 
various system data structures, and a periodic timer interrupt source. 
By default, the vector area is defined at the top of cstartup_M.s, which is
a slightly modified from the base IAR file. 

The _tx_initialize_low_level function inside of tx_initialize_low_level.s
also determines the first available address for use by the application, which 
is supplied as the sole input parameter to your application definition function, 
tx_application_define. To accomplish this, a section is created in 
tx_initialize_low_level.s called FREE_MEM, which must be located after all 
other RAM sections in memory.


5.  Register Usage and Stack Frames

The following defines the saved context stack frames for context switches
that occur as a result of interrupt handling or from thread-level API calls.
All suspended threads have the same stack frame in the Cortex-M4 version of
ThreadX. The top of the suspended thread's stack is pointed to by 
tx_thread_stack_ptr in the associated thread control block TX_THREAD.

Non-FPU Stack Frame:

  Stack Offset     Stack Contents 

     0x00               LR          Interrupted LR (LR at time of PENDSV)
     0x04               r4          
     0x08               r5          
     0x0C               r6          
     0x10               r7          
     0x14               r8          
     0x18               r9          
     0x1C               r10 (sl)    
     0x20               r11         
     0x24               r0          (Hardware stack starts here!!)
     0x28               r1          
     0x2C               r2          
     0x30               r3          
     0x34               r12         
     0x38               lr          
     0x3C               pc          
     0x40               xPSR        

FPU Stack Frame (only interrupted thread with FPU enabled):

  Stack Offset     Stack Contents 

     0x00               LR          Interrupted LR (LR at time of PENDSV)
     0x04               s0
     0x08               s1
     0x0C               s2
     0x10               s3
     0x14               s4
     0x18               s5
     0x1C               s6
     0x20               s7
     0x24               s8
     0x28               s9
     0x2C               s10
     0x30               s11
     0x34               s12
     0x38               s13
     0x3C               s14
     0x40               s15
     0x44               s16
     0x48               s17
     0x4C               s18
     0x50               s19
     0x54               s20
     0x58               s21
     0x5C               s22
     0x60               s23
     0x64               s24
     0x68               s25
     0x6C               s26
     0x70               s27
     0x74               s28
     0x78               s29
     0x7C               s30
     0x80               s31
     0x84               fpscr
     0x88               r4          
     0x8C               r5          
     0x90               r6          
     0x94               r7          
     0x98               r8          
     0x9C               r9          
     0xA0               r10 (sl)    
     0xA4               r11         
     0xA8               r0          (Hardware stack starts here!!)
     0xAC               r1          
     0xB0               r2          
     0xB4               r3          
     0xB8               r12         
     0xBC               lr          
     0xC0               pc          
     0xC4               xPSR        


6.  Improving Performance

The distribution version of ThreadX is built without any compiler 
optimizations. This makes it easy to debug because you can trace or set 
breakpoints inside of ThreadX itself. Of course, this costs some 
performance. To make it run faster, you can change the ThreadX library
project to enable various compiler optimizations. 

In addition, you can eliminate the ThreadX basic API error checking by 
compiling your application code with the symbol TX_DISABLE_ERROR_CHECKING 
defined. 


7.  Interrupt Handling

The Cortex-M4 vectors start at the label __vector_table and is defined in cstartup_M.s. 
The application may modify the vector area according to its needs.


7.2 Managed Interrupts

From version 5.6 going forward, ISRs for Cortex-M using the IAR tools can be written
completely in C (or assembly language) without any calls to _tx_thread_context_save or 
_tx_thread_context_restore. These ISRs are allowed access to the ThreadX API that is
available to ISRs.

ISRs written in C will take the form (where "your_C_isr" is an entry in the vector table):

void    your_C_isr(void)
{

    /* ISR processing goes here, including any needed function calls.  */
}

ISRs written in assembly language will take the form:

        PUBLIC  your_assembly_isr
your_assembly_isr:

    PUSH    {lr}

    ; ISR processing goes here, including any needed function calls.

    POP     {lr}
    BX      lr

Backward compatibility to the previous style assembly ISRs is maintained, which was of 
the form:

        PUBLIC  __legacy_isr_handler
__legacy_isr_handler:
        PUSH    {lr}
        BL  _tx_thread_context_save
        
;    /* Do interrupt handler work here */
;    /* .... */

        B       _tx_thread_context_restore


8.  IAR Thread-safe Library Support

Thread-safe support for the IAR tools is easily enabled by building the ThreadX library
and the application with TX_ENABLE_IAR_LIBRARY_SUPPORT. Also, the linker control file
should have the following line added (if not already in place):

initialize by copy with packing = none { section __DLIB_PERTHREAD }; // Required in a multi-threaded application


8.  IAR Thread-safe Library Support

Thread-safe support for the IAR tools is easily enabled by building the ThreadX library
and the application with TX_ENABLE_IAR_LIBRARY_SUPPORT. Also, the linker control file
should have the following line added (if not already in place):

initialize by copy with packing = none { section __DLIB_PERTHREAD }; // Required in a multi-threaded application

The project options "General Options -> Library Configuration" should also have the 
"Enable thread support in library" box selected.


9. VFP Support

ThreadX for Cortex-M4 supports automatic ("lazy") VFP support, which means that applications threads 
can simply use the VFP and ThreadX automatically maintains the VFP registers as part of the thread 
context - no additional setup by the application.

In previous versions, the application was required to explicitly enable VFP support on a thread-by-thread
basis. This is no longer required. However, these APIs are maintained for backward compatibility:

void    tx_thread_fpu_enable(void);
void    tx_thread_fpu_disable(void);


10.  Revision History

03/11/2018  ThreadX update of Cortex-M4/IAR port. The following files were 
            changed/added for port specific version 5.7:

            tx_port.h                       Changed version ID.
            tx_thread_stack_build.s         Modified logic to ensure 8-byte stack alignment.
            tx_iar.c                        Added optional _MAX_FLOCK definition for IAR library
                                            support.
            *.s                             Modified comments.

06/01/2017  ThreadX update of Cortex-M4/IAR port. The following files were 
            changed/added for port specific version 5.6:

            tx_port.h                       Added support for new thread-safe IAR libraries,
                                            removed unneeded VFP enable flag, added 
                                            logic to remove the need for context
                                            save/restore calls in ISRs, modified code 
                                            for MISRA compliance, and updated version 
                                            string.
            tx_iar.c                        Added support for new thread-safe IAR libraries.
            tx_initialize_low_level.s       Removed VFP control register setup, and 
                                            removed default vectors.
            tx_thread_context_save.s        Changed the code to simply return since this 
                                            function is no longer needed.
            tx_thread_context_restore.s     Changed the code to simply return since this 
                                            function is no longer needed.
            tx_thread_interrupt_disable.s   Added new function for this release.
            tx_thread_interrupt_restore.s   Added new function for this release.
            tx_thread_schedule.s            Added automatic VFP save/restore, added 
                                            optimizations, and changed logic to use 
                                            PendSV instead of SVC.
            tx_thread_stack_build.s         Added LR to initial stack.
            tx_thread_system_return.s       Changed logic to use PendSV instead of SVC 
                                            to return to the scheduler.
            tx_timer_interrupt.s            Added optimizations, and added logic to set 
                                            PendSV if preemption is necessary.
            *.s                             Modified comments.

09/01/2014  ThreadX update of Cortex-M4/IAR port. The following files were 
            changed/added for port specific version 5.5:

            tx_port.h                       Changed version ID.
            tx_thread_context_restore.s     Removed setting FPU save flag from ISR.
            tx_thread_schedule.s            Changed logic to save FPU registers on 
                                               solicited return from threads.
            *.s                             Modified comments.

04/15/2014  ThreadX update of Cortex-M4/IAR port. The following files were 
            changed/added for port specific version 5.4:

            tx_port.h                       Changed version ID.
            tx_thread_schedule.s            Added code to clear the current thread 
                                              pointer to avoid race condition in 
                                              context restore's setting of the save
                                              FPU regs bit.
            *.s                             Modified comments.

01/01/2014  ThreadX update of Cortex-M4/IAR port. The following files were 
            changed/added for port specific version 5.3:

            tx_port.h                       Added FPU enable/disable struct 
                                              member, and changed version ID.
            tx_initialize_low_level.s       Removed unnecessary code in timer 
                                              interrupt ISR.
            tx_thread_context_restore.s     Moved FPU control field, and corrected 
                                               problem saving FPU regs without 
                                               active thread.
            tx_thread_schedule.s            Removed unnecessary code, added 
                                              optimizations, moved FPU control 
                                              field, added interrupt protection 
                                              over optional thread exit call, 
                                              and placed conditional around WFI 
                                              instruction.
            *.s                             Modified comments.


04/15/2012  ThreadX update of Cortex-M4/IAR port. The following files were 
            changed/added for port specific version 5.2:

            tx_port.h                       Added FPU enable/disable function 
                                            prototypes, and changed version ID.
            tx_initialize_low_level.s       Added generic vector table symbols, 
                                            and added FPU support.
            tx_thread_context_restore.s     Added FPU support.
            tx_thread_schedule.s            Added generic vector table symbols, 
                                            and added FPU support.
            *.s                             Modified comments.

07/15/2011  ThreadX update of Cortex-M4/IAR port. The following files were 
            changed/added for port specific version 5.1:

            tx_port.h                       Changed version ID and added 
                                            IAR thread-safe library support.
            tx_iar.c                        New file for IAR library 
                                            thread-safe support.
            *.s                             Modified comments.

10/10/2010  Initial ThreadX version for Cortex-M4 using IAR's ARM tools.


Copyright(c) 1996-2018 Express Logic, Inc.


Express Logic, Inc.
11423 West Bernardo Court
San Diego, CA  92127

www.expresslogic.com

