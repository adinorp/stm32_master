#include "LF__InternalADC_Driver.h"
#include "LF__Utilities.h"
#include "LF_LCD_Driver.h"

extern ADC_HandleTypeDef hadc1;

char *Power1Error = " Power-Up Test ";
char *Power2Error = "     FAIL      ";

char *Power1Pass = " Power-Up Test ";
char *Power2Pass = "      PASS     ";

uint16_t LF_ReadADCChannel12 (void)
{
  uint16_t rawValue =0;
  /* Start Conversion */
  HAL_ADC_Start(&hadc1);
  /* Poll for Conversion */
  HAL_ADC_PollForConversion(&hadc1, 100);  
   /* Check if the continous conversion of regular channel is finished */
   if((HAL_ADC_GetState(&hadc1) & HAL_ADC_STATE_EOC_REG) == HAL_ADC_STATE_EOC_REG)
   {
      rawValue   = HAL_ADC_GetValue(&hadc1);
           
   }
  return rawValue;
  
}

bool LF_PowerRailsCheck(void)
{
   bool status = false; 
   uint8_t cycleCount =0;
   uint16_t rawValue =0;
   for(cycleCount=0; cycleCount<3;cycleCount++)
   {
      rawValue = LF_ReadADCChannel12();
      if(rawValue == 4095)
      {
        status = true;
        break;
      }
      else if (rawValue < 4095)
      {
         DWT_Delay_us(3000);
      }
     
   }
   
   return status; 
}

void LF_PowerUpCheck(void)
{
    if(LF_PowerRailsCheck())
    {
      LF_LCD_Control(Power1Pass,Power2Pass);
      DWT_Delay_us(3000);
    }
    else
    {
      LF_LCD_Control(Power1Error,Power2Error);
      for(;;);
      
    }
  
}