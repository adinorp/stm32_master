#include <string.h>
#include "stm32f4xx_hal.h"
#include "LF__LED_Driver.h"
#include "LF__Utilities.h"

#define LED_SIZE 24
#define I2C_TIMEOUT 100

extern I2C_HandleTypeDef hi2c2;

static void InitLedControl(void)
{
  uint8_t data[2]={0x3f,COMSEND};
  uint8_t Config[]={0x80,COMSEND,COMSEND,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0x80,0x80};
  HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)LED_ADDRESS, data, 2, I2C_TIMEOUT);
  DWT_Delay_us(LED_DELAY_US);
  
  data[0]= DATASEND;
  data[1]=0xff;
  HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)LED_ADDRESS, data, 2, I2C_TIMEOUT);
  DWT_Delay_us(LED_DELAY_US);
  
  
  HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)LED_ADDRESS, Config,11, I2C_TIMEOUT);
  DWT_Delay_us(LED_DELAY_US);
  
  
  
  
}

void LF_LED_Control(int LedStateData[])
{
 
  
  char hex_arg;
  int i;
  uint8_t data[3]={0};
  
  InitLedControl();
  
    // Declare LEDs to be lit or turned off.

  for(i = 0; i < 24; i++)
  {
    if(LedStateData[i] != 0)
    {
      hex_arg = (char)(0x0A + i);
      data[0] = hex_arg;
      data[1] = 0x00;  
      HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)LED_ADDRESS, data, 2, I2C_TIMEOUT);
      DWT_Delay_us(LED_DELAY_US);
    }
  }
  
    // set pwm current level

  for(i = 0; i < 24; i++)
  {
    if(LedStateData[i] != 0)
    {
      hex_arg = (char) (0x22 + i);
      data[0] = hex_arg;
      if(i < 19)
      {
        data[1]= (0x26);  // level will always be 0.15
      }
      else
      {
        data[1]= (0x1A);  // unless a center green LED
      }
      HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)LED_ADDRESS, data, 2, I2C_TIMEOUT);
      DWT_Delay_us(LED_DELAY_US);
    }
  }
  
    //  Turn LED ON.

  for(i = 0; i < 24; i++)
  {
    if(LedStateData[i] != 0)
    {
      
      hex_arg = (char) (0x0A + i);
      data[0] = hex_arg; // the pin number 
      if(LedStateData[i] == 1)
      {
        data[1]= (0xFF); // turn OFF LED ON
      }
      if(LedStateData[i] == -1)
      {
        data[1]= (0x00); // turn ON LED OFF
	LedStateData[i] = 0;   // set switch state back to 0
	
      }
      HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)LED_ADDRESS, data, 2, I2C_TIMEOUT);
      DWT_Delay_us(LED_DELAY_US);
    }
  }

  DWT_Delay_us(LED_DELAY_US);
  
 
}