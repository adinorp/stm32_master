#include "LF__Utilities.h"
#include "LF_LCD_Driver.h"


extern UART_HandleTypeDef huart2;
unsigned long *uid = (unsigned long *)0x1FFF7A10; 

FIL fil;
FRESULT fres;
FILINFO fno;
FATFS fs;
uint32_t fileIndex = 0;
char fileString[50];
char LogfileName[20];
char CharBuffer[100];
char rtext[10];  
char line[4]; 
/* File read buffer */
uint32_t byteswritten, bytesread;                     /* File write/read counts */
const uint32_t UpperLimit = 1500;
const uint32_t LowerLimit = 2; 
char *File1Error = " SD Card FAIL ";
char *File2Error = " Check Card ";


int f_LineCount(FIL* FileObject) 
{
 int FileEnd = 0;
 int CurrentLine = 0; 
 char* TempChar ;
 

 while ((f_eof(FileObject) == 0))
 {
  TempChar = f_gets((char*)CharBuffer, sizeof(CharBuffer), FileObject);
  CurrentLine ++;
  FileEnd = FileEnd + strlen(TempChar) + 1;	
 }
 return CurrentLine; 
}





uint32_t LF_GetFileIndex(void)
{
  
  return fileIndex;
}

void LF_GetLogFileName(char *filename)
{
   strncpy(filename,LogfileName,strlen(LogfileName));
  
  
}

FRESULT open_append (
    FIL* fp,            /* [OUT] File object to create */
    const char* path    /* [IN]  File name to be opened */
)
{
    FRESULT fr;

    /* Opens an existing file. If not exist, creates a new file. */
    fr = f_open(fp, path, FA_WRITE | FA_OPEN_ALWAYS);
    if (fr == FR_OK) {
        /* Seek to end of the file to append data */
        fr = f_lseek(fp, f_size(fp));
        if (fr != FR_OK)
            f_close(fp);
    }
    return fr;
}



FRESULT LF_OpenAppendFile (
    FIL* fp,            /* [OUT] File object to create */
    const char* path,    /* [IN]  File name to be opened */
    const char* data    /* [IN]  Data be written to file */
)
{
  
    uint32_t wbytes;
    /* Opens an existing file. If not exist, creates a new file. */
    fres = f_open(fp, path, FA_WRITE | FA_OPEN_ALWAYS);
    /* Seek to end of the file to append data */
    fres = f_lseek(fp, f_size(fp));
    fres = f_write(fp, data, sizeof(data), (void *)&wbytes);
    f_close(fp);
    
    return fres;
  
}



FRESULT LF_OpenAppendFileAndBLE (
    FIL* fp,            /* [OUT] File object to create */
    const char* path,    /* [IN]  File name to be opened */
    const char* data    /* [IN]  Data be written to file */
)
{
  
    uint32_t wbytes;
    /* Opens an existing file. If not exist, creates a new file. */
    fres = f_open(fp, path, FA_WRITE | FA_OPEN_ALWAYS);
    /* Seek to end of the file to append data */
    fres = f_lseek(fp, f_size(fp));
    fres = f_write(fp, data, strlen(data), (void *)&wbytes);
    f_close(fp);
    LF_SendToBluetooth((char*)data);
    
    return fres;
  
}

void LF_OpenLifeFile(FIL* fp)
{

  //uint32_t index = (rand()%(UpperLimit - LowerLimit + 1)) + LowerLimit;
  uint32_t wbytes; 
  FRESULT filehandle;
 
  
  fres = f_mount(&fs, "/", 1); //1=mount now
  if(fres == FR_OK)
  {
    filehandle = f_open(fp,"MASTER.txt",FA_READ);
    f_close(fp);
  }
  else
  {
     LF_LCD_Control(File1Error,File2Error);
     for(;;);
  }
  
  
  
  
  switch(filehandle)
  {
    case FR_NO_FILE:
    //File does not exist. Should be created. 
    fres = f_open(fp,"MASTER.txt",FA_CREATE_ALWAYS|FA_WRITE); 
    fileIndex = 1;  
    sprintf(fileString,"%d\r\n",fileIndex);
    fres = f_write(fp, fileString, sizeof(fileString), (void *)&wbytes);
    f_close(fp);
    
    
    sprintf(LogfileName,"LOG%d.txt",fileIndex);
    fres = f_open(fp,LogfileName,FA_CREATE_ALWAYS|FA_WRITE); 
    sprintf(fileString,"START of DATA LOG with INDEX: %d!\r\n",fileIndex);
    fres = f_write(fp, fileString, sizeof(fileString), (void *)&wbytes);
    /* Seek to end of the file to append data */
    fres = f_lseek(fp, f_size(fp));
    sprintf(fileString,"\r\nUnique ID: %08X %08X %08X \r\n", uid[0], uid[1], uid[2]);
    fres = f_write(fp, fileString, sizeof(fileString), (void *)&wbytes);
    f_close(fp);
    
    break;
      
    case FR_OK:
    
    fres = f_open(fp,"MASTER.txt",FA_READ|FA_WRITE);
    
    //f_read(fp, rtext, sizeof(rtext), (void *)&bytesread);
    fileIndex = f_LineCount(fp);
    fileIndex++;
    sprintf(fileString,"%d\r\n",fileIndex);
    //Seek to end of the file to append data 
    fres = f_lseek(fp, f_size(fp));
    fres = f_write(fp, fileString, sizeof(fileString), (void *)&wbytes);
    f_close(fp);
    
    sprintf(LogfileName,"LOGFILE%d.txt",fileIndex);
    fres = f_open(fp,LogfileName,FA_CREATE_ALWAYS|FA_WRITE); 
    sprintf(fileString,"START of DATA LOG with INDEX: %d!\r\n",fileIndex);
    fres = f_write(fp, fileString, sizeof(fileString), (void *)&wbytes);
    /* Seek to end of the file to append data */
    fres = f_lseek(fp, f_size(fp));
    sprintf(fileString,"\r\nUnique ID: %08X %08X %08X \r\n", uid[0], uid[1], uid[2]);
    fres = f_write(fp, fileString, sizeof(fileString), (void *)&wbytes);
    f_close(fp);
    break;
    
    case FR_DISK_ERR:
    LF_LCD_Control(File1Error,File2Error);  
    break; 
      
    default:
    //Error state    
    break; 
   
  }
}



void LF_CloseFile(FIL* fp)
{
  f_close(fp);
}






uint32_t DWT_Delay_Init(void) 
{
  /* Disable TRC */
  CoreDebug->DEMCR &= ~CoreDebug_DEMCR_TRCENA_Msk; // ~0x01000000;
  /* Enable TRC */
  CoreDebug->DEMCR |=  CoreDebug_DEMCR_TRCENA_Msk; // 0x01000000;
     
  /* Disable clock cycle counter */
  DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk; //~0x00000001;
  /* Enable  clock cycle counter */
  DWT->CTRL |=  DWT_CTRL_CYCCNTENA_Msk; //0x00000001;
     
  /* Reset the clock cycle counter value */
  DWT->CYCCNT = 0;
     
     /* 3 NO OPERATION instructions */
     __ASM volatile ("NOP");
     __ASM volatile ("NOP");
  __ASM volatile ("NOP");

  /* Check if clock cycle counter has started */
     if(DWT->CYCCNT)
     {
       return 0; /*clock cycle counter started*/
     }
     else
  {
    return 1; /*clock cycle counter not started*/
  }
}


void DWT_Delay_us(volatile uint32_t microseconds)
{
  uint32_t clk_cycle_start = DWT->CYCCNT;

  /* Go to number of cycles for system */
  microseconds *= (HAL_RCC_GetHCLKFreq() / 1000000);

  /* Delay till end */
  while ((DWT->CYCCNT - clk_cycle_start) < microseconds);
}




void LF_SendToBluetooth(char *msg)
{
    //Add mutex to this
  HAL_UART_Transmit(&huart2,(uint8_t*)msg,strlen(msg),1000);
  
  

}




