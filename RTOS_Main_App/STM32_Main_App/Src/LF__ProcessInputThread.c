#include "main.h"
#include "stm32f4xx_hal.h"
#include "LF_PWM_Driver.h"
#include "LF_LCD_Driver.h"
#include "LF__Utilities.h"


extern TX_QUEUE Event_Queue;
extern ADC_HandleTypeDef hadc1;
volatile uint8_t convCompleted = 0;
uint32_t sensorValues[3];

//---------------------------------------------------------------
// Private Variables
//---------------------------------------------------------------
static uint8_t StartSwitch_state = 0;

//---------------------------------------------------------------
// Private Functions
//---------------------------------------------------------------

static void Process_StartAbortSwitchState(void)
{
    
   static bool startState = false; 
   if(!startState)
   {
      LF__SendMailToEventThread(START_MSG);
      startState = true;
   }
   else
   {
       //Abort 
      PumpPwm__Stop();
      ValvePwm__Stop();
      if(tx_thread_wait_abort(&EventThread)==TX_SUCCESS)
      {
         LF__ST7036__LCDclear();
	  LF__ST7036__LCDwrite("  ABORT CMD ",0);
         tx_thread_sleep(5000);
         NVIC_SystemReset();
        
      }
      
      //LF__SendMailToEventThread(ABORT_MSG);
      startState = false;
   }
  
 
}


static void SwitchHandler__Process(void)
{
   uint8_t debounce_count = 0;
   uint8_t press_count    = 0;
    
   
   StartSwitch_state = LF_GPIO_StartSwitch();
   if (StartSwitch_state)
   {
      while (StartSwitch_state)
      {
         if (debounce_count == 0)
         {
            press_count++;
            Process_StartAbortSwitchState();	
            debounce_count++;
         }
         
         StartSwitch_state = LF_GPIO_StartSwitch();
      }
      debounce_count = 0;
   }
}


void    ProcessInputThread_entry(ULONG thread_input)
{
    //HAL_ADC_Start_DMA(&hadc1,sensorValues,3);
    while(1)
    {
        SwitchHandler__Process();
        tx_thread_sleep(1);
    }
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
  convCompleted = 1;
  
}

/** GPIO Intererupt Handler 
        
        * EXTI
*/

/*
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
   ULONG EventThread_messages_sent;
  
   switch(GPIO_Pin)
   {
     //SW Start
     case GPIO_PIN_10:
     //LF__ST7036__LCDclear();
     //PumpPwm__Start();
     //ValvePwm__Start();
     //LF__ST7036__LCDwrite("START!",1);
     //LF__SendMailToEventThread(START_MSG);  
     EventThread_messages_sent = START_MSG;
     tx_queue_send(&Event_Queue,(&EventThread_messages_sent), TX_NO_WAIT);
     break;
     //SW Util1  
     case GPIO_PIN_11:
     //LF__ST7036__LCDclear();
     //PumpPwm__Stop();
     //ValvePwm__Stop();
     //LF__ST7036__LCDwrite("ABORT!",2);
      //LF__SendMailToEventThread(ABORT_MSG);
     break;
     //SW Util2 
     case GPIO_PIN_12:
     
     //LF__ST7036__LCDclear();
     break;  
       
   }
   
}*/




