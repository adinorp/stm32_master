#include "main.h"
#include <stdbool.h>
#include "stm32f4xx_hal.h"
#include "LF_PWM_Driver.h"
#include "LF_LCD_Driver.h"
#include "LF_GPIO_Driver.h"
#include <string.h>

#define COMMAND_TABLE_SIZE 20

ULONG           NetworkManagerThread_counter;
extern UART_HandleTypeDef huart2;

static  uint8_t rx_data[10]={0};
static bool dataRdy=false; 

/* Private function prototypes -----------------------------------------------*/
static void commandLCDTest(uint8_t *data);
static void commandDisableBacklight(uint8_t *data);
static void commandEnableBacklight(uint8_t *data);
static void commandClearLCD(uint8_t *data);
static void commandStartValve(uint8_t *data);
static void commandStopValve(uint8_t *data);
static void commandStartPump(uint8_t *data);
static void commandStopPump(uint8_t *data);
static void commandPumpPWMStep1(uint8_t *data);
static void commandPumpPWMStep2(uint8_t *data);
static void commandPumpPWMStep3(uint8_t *data);
static void commandPumpPWMStep4(uint8_t *data);
static void commandDisableAnalog(uint8_t *data);
static void commandEnableAnalog(uint8_t *data);
static void commandGetDeviceId(uint8_t *data);

bool Parse_Incoming_Command(uint8_t * buffer);

typedef struct
{
    uint8_t cmd;
    void (*function)(uint8_t *data);
}command_t;

command_t const gCommandTable[COMMAND_TABLE_SIZE] =
{
   {0x01,commandLCDTest},
   {0x02,commandDisableBacklight},
   {0x03,commandEnableBacklight},
   {0x04,commandClearLCD},
   {0x05,commandStartValve},
   {0x06,commandStopValve},
   {0x07,commandStartPump},
   {0x08,commandStopPump},
   {0x09,commandPumpPWMStep1},
   {0x10,commandPumpPWMStep2},
   {0x11,commandPumpPWMStep3},
   {0x12,commandPumpPWMStep4},
   {0x13,commandDisableAnalog},
   {0x14,commandEnableAnalog},
   {0x15,commandGetDeviceId},
   {0xFF,    NULL,}
};



void    NetworkManagerThread_entry(ULONG thread_input)
{

    //UINT    status;
    //ULONG   actual_flags;
    HAL_UART_Receive_DMA(&huart2,(uint8_t*)rx_data,1);

    /* This thread simply waits for an event in a forever loop.  */
    while(1)
    {

        if(dataRdy)
        {
           Parse_Incoming_Command(rx_data);
           dataRdy = false;    
        }
       
         /* Sleep for 2 ticks to hold the mutex.  */
        tx_thread_sleep(1);
    }
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

  HAL_UART_Receive_DMA(&huart2,(uint8_t*)rx_data,1);
  dataRdy = true;
	
}



bool Parse_Incoming_Command(uint8_t * buffer)
{
    int idx;
    bool valid_cmd_status = false;
    for (idx = 0; gCommandTable[idx].cmd != 0xFF; idx++)
    {
       if (gCommandTable[idx].cmd == buffer[0])
       {
          valid_cmd_status = true;
          break;
       }
    }
    if(valid_cmd_status)
    {
      (*gCommandTable[idx].function)(buffer);
    }
    
    return valid_cmd_status;
}


static void commandLCDTest(uint8_t *data)
{
  const char *dataOutput = "LCD CMD Received";
  LF__ST7036__LCDwrite("LIFE CUFF !",1);
  LF__ST7036__LCDwrite("NEXT GEN",2);
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
 
}

static void commandClearLCD(uint8_t *data)
{
  const char *dataOutput = "LCD Clear Received";
  LF__ST7036__Clear();
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}


static void commandDisableBacklight(uint8_t *data)
{
  const char *dataOutput = "LCD Disable BackLight";
  LF__ST7036__DisableBacklight();
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
  
  
}
static void commandEnableBacklight(uint8_t *data)
{
  const char *dataOutput = "LCD Disable BackLight";
  LF__ST7036__EnableBacklight();
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}

static void commandStartValve(uint8_t *data)
{
  const char *dataOutput = "Start Valve PWM";
  ValvePwm__Start();
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}
static void commandStopValve(uint8_t *data)
{
  const char *dataOutput = "Stop Valve PWM";
  ValvePwm__Stop();
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}

static void commandStartPump(uint8_t *data)
{
  const char *dataOutput = "Start Pump PWM";
  PumpPwm__Start();
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}
static void commandStopPump(uint8_t *data)
{
  const char *dataOutput = "Stop Pump PWM";
  PumpPwm__Stop();
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}

static void commandPumpPWMStep1(uint8_t *data)
{
  const char *dataOutput = "Update Pump DutyCycle 25%";
  PumpPwm_UpdateDutyCycle(DutyCycle_25);
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}
static void commandPumpPWMStep2(uint8_t *data)
{
  const char *dataOutput = "Update Pump DutyCycle 50%";
  PumpPwm_UpdateDutyCycle(DutyCycle_50);
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}
static void commandPumpPWMStep3(uint8_t *data)
{
   const char *dataOutput = "Update Pump DutyCycle 75%";
   PumpPwm_UpdateDutyCycle(DutyCycle_75);
   HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}
static void commandPumpPWMStep4(uint8_t *data)
{
   const char *dataOutput = "Update Pump DutyCycle 100%";
   PumpPwm_UpdateDutyCycle(DutyCycle_100);
   HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}

static void commandDisableAnalog(uint8_t *data)
{
  const char *dataOutput = "Disable Analog Supply";
  LF_GPIO_DisableAnalogSupply();
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}

static void commandEnableAnalog(uint8_t *data)
{
  const char *dataOutput = "Enable Analog Supply";
  LF_GPIO_EnableAnalogSupply();
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
}

static void commandGetDeviceId(uint8_t *data)
{
  uint8_t i;
  char msg[20];
  const char *dataOutput = "[LF:SN ";
  uint32_t *uniqueID = (uint32_t*)0x1FFF7A10;
  HAL_UART_Transmit(&huart2,(uint8_t*)dataOutput,strlen(dataOutput),1000);
  for(i=0; i<12; i++)
  {
      i < 11 ? sprintf(msg,"%x", uniqueID[i]) : sprintf(msg,"%d]\r\n", uniqueID[i]);
      HAL_UART_Transmit(&huart2,(uint8_t*)msg,strlen(msg),1000);
  }
  
}
