#include "LF_LCD_Driver.h"
#include "LF__Utilities.h"
#include "LF_GPIO_Driver.h"
#include "stm32f4xx_hal.h"
#include <string.h>
#include   "tx_api.h"

extern I2C_HandleTypeDef hi2c2;

static void Write_Instruction(uint8_t cmd);
static void Write_Data(uint8_t data);
uint8_t displayOnOffSetting = (DISPLAY_ON_OFF | DISPLAY_ON_OFF_D);
uint8_t contrast = 0x18;
 





void LF__ST7036__Clear() 
{ //clear display
  Write_Instruction(CLEAR_DISPLAY);
  DWT_Delay_us(HOME_CLEAR_DELAY_MS);
}

void LF__ST7036__Home() 
{ //return to first line address 0
  Write_Instruction(RETURN_HOME);
  DWT_Delay_us(HOME_CLEAR_DELAY_MS);
}

void LF__ST7036__SetCursor(uint8_t line, uint8_t pos) 
{
  uint8_t p;
  if(pos > 15) 
  {
    pos = 0;
  }
    
  if(line == 0)
  { 
    p = LINE_1_ADR + pos;
  }
  else 
  {
    p = LINE_2_ADR + pos;
  }
  Write_Instruction(SET_DDRAM_ADDRESS | p);
}

void LF__ST7036__DisplayOn() //turn on display
{ 
  displayOnOffSetting |= DISPLAY_ON_OFF_D;
  Write_Instruction(displayOnOffSetting);
}

void LF__ST7036__DisplayOff() //turn off display
{ 
  displayOnOffSetting &= ~DISPLAY_ON_OFF_D;
  Write_Instruction(displayOnOffSetting);
}

void LF__ST7036__CursorUnderline() //display underline cursor
{ 
  displayOnOffSetting |= DISPLAY_ON_OFF_C;
  Write_Instruction(displayOnOffSetting);
}

void LF__ST7036__NoCursor() //stop display underline cursor
{ 
  displayOnOffSetting &= ~DISPLAY_ON_OFF_C;
  Write_Instruction(displayOnOffSetting);
}

void LF__ST7036__BlinkCursor() //cursor block blink
{ 
  displayOnOffSetting |= DISPLAY_ON_OFF_B;
  Write_Instruction(displayOnOffSetting);
}

void LF__ST7036__NoCursorBlink() //stop cursor block blink
{ 
  displayOnOffSetting &= ~DISPLAY_ON_OFF_B;
  Write_Instruction(displayOnOffSetting);
}

void LF__ST7036__Setcontrast(uint8_t val) 
{
  if (val > CONTRAST_MAX)
  { 
    val = CONTRAST_MIN;
  }
  else if (val < CONTRAST_MIN)
  { 
    val = CONTRAST_MAX;
  }
  Write_Instruction(CONTRAST_SET | (val & 0x0f));
  Write_Instruction((val >> 4) | POWER_ICON_BOST_CONTR | POWER_ICON_BOST_CONTR_Bon);
  contrast = val;
}

void LF__ST7036__Adjcontrast(uint8_t val) 
{
  LF__ST7036__Setcontrast(val + contrast);
}

uint8_t LF__ST7036__Getcontrast() 
{
  return contrast;
}




uint8_t  LF__ST7036__Write(uint8_t chr)
{
  Write_Data(chr);
  return 1; 
}



static void Write_Instruction(uint8_t cmd)
{
   
  uint8_t data[2]={0};
  data[0] = CNTRBIT_CO;
  data[1] = cmd;
        
  HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)Write_Address, data, 2, 100);
  DWT_Delay_us(WRITE_DELAY_MS);
  
  
}
static void Write_Data(uint8_t data)
{
  uint8_t buffer[2]={0};
  buffer[0] = CNTRBIT_RS;
  buffer[1] = data;
  HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)Write_Address, buffer, 2, 1000);
  DWT_Delay_us(WRITE_DELAY_MS);
  
}



void LF__ST7036__InitLCD()
{
     DWT_Delay_Init();
     LF__ST7036__LCDcommand(0x38);      //� function set  basic
     DWT_Delay_us(300);
     LF__ST7036__LCDcommand(0x39);      //� function set extended
     DWT_Delay_us(300);
     LF__ST7036__LCDcommand(0x14);     // � internal OCS frequency adjustment
     DWT_Delay_us(300);
     LF__ST7036__LCDcommand(0x78);      //� contrast set low nible
     DWT_Delay_us(300);
     LF__ST7036__LCDcommand(0x54);     // � contrast set high nible / icon / power
     DWT_Delay_us(300);
     LF__ST7036__LCDcommand(0x6f);       //�  follower control
     DWT_Delay_us(200000);
     LF__ST7036__LCDcommand(0x0c);      //�  display on
     DWT_Delay_us(300);
     LF__ST7036__LCDcommand(0x01);      //� clear display
     DWT_Delay_us(350000);
     LF__ST7036__LCDcommand(0x06);      //� entry mode set
     DWT_Delay_us(300);
     LF__ST7036__LCDcommand(0x02);     // � home
     DWT_Delay_us(1000000);
  
}
void send_i2c(uint8_t value,uint8_t mode)
{
  uint8_t buffer[2]={0};
  buffer[0] = mode;
  buffer[1] = value;
  HAL_I2C_Master_Transmit(&hi2c2,(uint16_t)Write_Address, buffer, 2, 1000);
 
  
}
void LF__ST7036__LCDcommand(uint8_t value)
{
  send_i2c(value, 0x00);
  
}
void LF__ST7036__LCDwrite(char *value,uint8_t line)
{
  LF__ST7036__SetCursor(line,0);
  uint8_t length = strlen(value);
  uint8_t index;
  
  for(index = 0; index<length; index++)
  {
    
    send_i2c(value[index], 0x40);
  }
  
  
}
void LF__ST7036__LCDclear(void)
{
  LF__ST7036__LCDcommand(0x01);
  DWT_Delay_us(350000);
  
}

void LF__ST7036__EnableBacklight(void)
{
   HAL_GPIO_WritePin(GPIOB, BL_LED_Pin, GPIO_PIN_SET);
}
void LF__ST7036__DisableBacklight(void)
{
  HAL_GPIO_WritePin(GPIOB, BL_LED_Pin, GPIO_PIN_RESET);
  
}

void LF_LCD_Control(char *Line1, char *Line2)
{
  
   LF__ST7036__LCDwrite(Line1,0);
   LF__ST7036__LCDwrite(Line2,1);
   
   tx_thread_sleep(4000);
  
}
