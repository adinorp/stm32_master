#include "main.h"
#include "LF__ADS1115_Driver.h"
#include "LF_LCD_Driver.h"
#include "LF_PWM_Driver.h"
#include "LF_Constants.h"
#include "LF__Utilities.h"
#include "LF__LED_Driver.h"
#include "LF_GPIO_Driver.h"
#include <math.h>
#include "fatfs.h"

typedef enum
{
  STD_RIC = 0,
  FIX_RIC,
  MODE_C,
  MODE_D,
  MODE_E,
  MODE_F,
  MODE_G,
  MODE_H
}Therapy;


FIL fp; 
char LogFileName[20]; 

extern TX_QUEUE Event_Queue;
TX_TIMER        DataAcquire_Timer;
TX_TIMER        LedFlasher_Timer;
TX_TIMER        Inflation_Timer;

ULONG   EventThread_counter;
extern TIM_HandleTypeDef htim5;



char version[20],date[20],string1[20],string2[20];
int LED_state[24];
int intervals[200];
int dptr = 0;
int npeak = 0;
int MAP_index = 0;
int korot1 = 0;
int korot2 = 0;
int cycle_num=0;
int tock = 0;         // 400 Hz counter
int  running = 0;
int testBatteryModulus = 0;
int testBatteryLowCount = 0;
int inflation_duration_counter = 0;
void update_LCD(uint8_t cycleCount);
float batt_level; 
float test_value = 0.0;

static uint8_t mode_code =0; 
 
uint32_t RIC_inflation = 0;
uint32_t RIC_deflation = 0;
uint32_t done_flashing = 0;
uint32_t pulse = 0;

struct tuckData
{
  int   tval;
  float amp;
} peak_data[200];


char BluetoothMsg[200];

char line1[20];
char line2[20];
char korot_string[20];

float result = 0.0;
float pressure = 0.0;
float sys_press = 0.0;
float avg_pressure = 0.0;
float MAP = 0.0;
float MAP_amp = 0.0;
float dias_press = 0.0;
int deflating = 0;
int dias_on_deflate = 0;     // Flag to control cuff pressure release.
float acf[900];
int startButtonAbort = 0;
// check Tock
int tockPrevious = -9999;
int tockStuckCount = 0;
int ADC_data[CIRC_BUF_SIZE];
float data[CIRC_BUF_SIZE]; 
float filt_data[CIRC_BUF_SIZE];  // Bandpass filtered data
int tock_data[CIRC_BUF_SIZE];
char Separator[] ="====================================\r\n";

static bool process_mail(Event_e Event);
static bool Battery_Test(float threshold);
static void RunTherapy__StdRIC(void);
static void RunTherapy__FixedRIC(void);
static void RunTherapy__MODE_C(void);
static void RunTherapy__MODE_D(void);
static void RunTherapy__MODE_E(void);
static void RunTherapy__MODE_F(void);
static void RunTherapy__MODE_G(void);
static void RunTherapy__MODE_H(void);

void StdRIC__Algorithm(void);
void StdFixed__Algorithm(void);

float butterlp(float data);
void step_inflate(float max_pressure);
void pump_to_pressure(float target_pressure,int pulsewidth);
void bleed_to_pressure(float target_pressure, int pulsewidth);
void dither(void);
void NIBP(void);
void RIC_shutdown(void);
float calc_heart_rate(int reset_data);
int report(int heart_rate);
int find_lag_acf_max(void);
void make_output_string(int pressure, char pressure_string[]);
void compute_tuck_acf(void);
void calc_sys_dither(float osc_data[NUMBER_DITHER_LEVELS+1][2]);
double binary_chop(double a,double mu,double sigma,double frac);
double model(double a,double mu,double sigma,double x);
void RIC_report(void);
void step_deflate(void);
int get_osc_amp(int scr_ptr, float *avg_osc_amp, int control, int inflating);
int check_for_MAP(float osc_data[MAX_NUM_LEVELS][2], int num_levels);
float measure_fuzz(float data[]);
void calc_dias_inflate(float osc_data[MAX_NUM_LEVELS][2],int num_levels);
void calc_sys_inflate(float osc_data[MAX_NUM_LEVELS][2], int num_levels);
void check_tock();
void shutdown(void);
void shutdownStuck(void);
void shutdownAbort(void);
void testBattery(void);
void unit_RIC_abort(void);
void AcquireData(void);
void flashy(ULONG param);
void acquire(ULONG param);
void DurationTimer(ULONG param);
void calc_dias_deflate(float osc_data[MAX_NUM_LEVELS][2],int num_levels);
int check_for_MAP_deflate(float osc_data[MAX_NUM_LEVELS][2], int num_levels);
void convex_MAP(float osc_data[MAX_NUM_LEVELS][2],int maxLoc);
void calc_sys_deflate(float osc_data[MAX_NUM_LEVELS][2], int num_levels);
void teflon(uint8_t on_time, uint8_t off_time, int num_clicks);
int SP10(char version[20],char string1[20],char string2[20]);

static void Init_Sequence(void);

float deriv_data[BUFFER_SIZE]={0.0};
float f_data[BUFFER_SIZE]={0.0};
int int_osc_data[BUFFER_SIZE]={0};
int working_time[BUFFER_SIZE]={0};



void    EventThread_entry(ULONG thread_input)
{
    Event_e   received_message;
    
    static bool initState = false; 
    
    /* This thread simply sits in while-forever-sleep loop.  */
    while(1)
    {
        if(!initState)
        {
          Init_Sequence();
          initState = true; 
        }
        
        tx_queue_receive(&Event_Queue,&received_message,2);
        process_mail(received_message);
        
      
        tx_thread_sleep(10);
    }
}

static bool process_mail(Event_e Event)
{
	bool return_val = true;
        
	if(Event > END_OF_EVENTS)
        {
          return false;
	}
	switch(Event)
	{
		case START_MSG:
                        
       
                        //Read Switch Mode
                        mode_code = LF_GPIO_ReadModeSwitch();
                        switch(mode_code)
                       {
                         case STD_RIC :
                         LF__ST7036__LCDclear();
			  LF__ST7036__LCDwrite("MODE0 Initiated ",0);
                         RunTherapy__StdRIC();
                         break; 
                         case FIX_RIC:
                         LF__ST7036__LCDclear();
			  LF__ST7036__LCDwrite("MODE1 Initiated ",0);
                         RunTherapy__FixedRIC();
                         break; 
                         case MODE_C:
                         LF__ST7036__LCDclear();
			  LF__ST7036__LCDwrite("MODE2 Initiated ",0);
                         RunTherapy__MODE_C();
                         break; 
                         case MODE_D:
                         LF__ST7036__LCDclear();
			  LF__ST7036__LCDwrite("MODE3 Initiated ",0);
                         RunTherapy__MODE_D();
                         break; 
                         case MODE_E:
                         LF__ST7036__LCDclear();
			  LF__ST7036__LCDwrite("MODE4 Initiated ",0);
                         RunTherapy__MODE_E();
                         break; 
                         case MODE_F:
                         LF__ST7036__LCDclear();
			  LF__ST7036__LCDwrite("MODE5 Initiated ",0);
                         RunTherapy__MODE_F();
                         break; 
                         case MODE_G:
                         LF__ST7036__LCDclear();
			  LF__ST7036__LCDwrite("MODE6 Initiated ",0);
                         RunTherapy__MODE_G();
                         break; 
                         case MODE_H:
                         LF__ST7036__LCDclear();
			  LF__ST7036__LCDwrite("MODE7 Initiated ",0); 
                         RunTherapy__MODE_H();
                         break; 
                         default:
                         sprintf(BluetoothMsg,"Not a valid mode selection.\r\n");
                         LF_SendToBluetooth(BluetoothMsg);
                         break; 
                         
                       }
                        
                          
			break;
                        
                case ABORT_MSG:
                        LF_CloseFile(&fp);
                        LF__ST7036__LCDclear();
			LF__ST7036__LCDwrite("ABORT !",0);
			break;

		default: break;
	}
	return return_val;
}



static void RunTherapy__StdRIC(void)
{
      
     sprintf(BluetoothMsg,"Mode A - automatic adaptive remote ischemic conditioning\n\r");
     LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
     sprintf(BluetoothMsg,"    RIC parameters:\r\n");
     LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
     sprintf(BluetoothMsg,"         OCCLUSION_PRESSURE_INC = %f\n\r",OCCLUSION_PRESSURE_INC);
     LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
     sprintf(BluetoothMsg,"         DITHER_PRESSURE_STEP = %d\n\r",DITHER_PRESSURE_STEP);
     LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
     //Test of Battery Voltage is adequate for specified therapy
     //RIC_MIN_BATT_VOLTAGE
     StdRIC__Algorithm();
     /*
     if(Battery_Test(RIC_MIN_BATT_VOLTAGE))
     {
       StdRIC__Algorithm();
     }
     else
     {
       LF__ST7036__LCDclear();
       LF__ST7036__LCDwrite("  Low Battery.  ",0);
       LF__ST7036__LCDwrite("Replace Battery.",1);
     }*/
    
    
}


static void RunTherapy__FixedRIC(void)
{
     sprintf(BluetoothMsg,"Mode B - fixed pressure remote ischemic conditioning\n");
     LF_SendToBluetooth(BluetoothMsg);
     //Test of Battery Voltage is adequate for specified therapy
     StdFixed__Algorithm();
     /*
     if(Battery_Test(RIC_MIN_BATT_VOLTAGE))
     {
       //Run Fixed RIC
       StdFixed__Algorithm();
     }
     else
     {
       LF__ST7036__LCDclear();
       LF__ST7036__LCDwrite("  Low Battery.  ",0);
       LF__ST7036__LCDwrite("Replace Battery.",1);
     }*/
    
}


static void RunTherapy__MODE_C(void)
{
     sprintf(BluetoothMsg,"Mode C - sham remote conditioning\n");
     sprintf(BluetoothMsg,"Mode not yet implemented\n");
     LF_SendToBluetooth(BluetoothMsg);
}


static void RunTherapy__MODE_D(void)
{
     sprintf(BluetoothMsg,"Mode D - blood pressure and heart rate measurement (NIBP mode)");
     sprintf(BluetoothMsg,"Mode not yet implemented\n");
     LF_SendToBluetooth(BluetoothMsg);
}

static void RunTherapy__MODE_E(void)
{
    sprintf(BluetoothMsg,"Mode E - NIBP validation mode\n");
    //Test of Battery Voltage is adequate for specified therapy
    if(Battery_Test(NIBP_MIN_BATT_VOLTAGE))
    {
       //Run Fixed SP10
      SP10(version,string1,string2);
    }
    else
    {
       LF__ST7036__LCDclear();
       LF__ST7036__LCDwrite("  Low Battery.  ",0);
       LF__ST7036__LCDwrite("Replace Battery.",1);
    }
  

}

static void RunTherapy__MODE_F(void)
{
     sprintf(BluetoothMsg,"Mode F - static pressure measurement\n");
     LF_SendToBluetooth(BluetoothMsg);
     //Pressure Test 
}

static void RunTherapy__MODE_G(void)
{
     sprintf(BluetoothMsg,"Not a valid mode selection.\r\n");
     LF_SendToBluetooth(BluetoothMsg);
}



static void RunTherapy__MODE_H(void)
{
     sprintf(BluetoothMsg,"Not a valid mode selection.\r\n");
     LF_SendToBluetooth(BluetoothMsg);
}


static bool Battery_Test(float threshold)
{
   float BattLevel = 0.0;
   bool status = true; 
   BattLevel = LF__ADS115__ReadBattVoltage();
   sprintf(BluetoothMsg,"BattLevel = %f  Threshold = %f\n\r",BattLevel,threshold);
   LF_SendToBluetooth(BluetoothMsg);
   
   if(BattLevel < threshold)
   {
        LF__ST7036__LCDclear();
	LF__ST7036__LCDwrite("  Low Battery.  ",0);
        LF__ST7036__LCDwrite("Replace Battery.",1);
        status = false;     
   }
   return status; 
}

void StdRIC__Algorithm(void)
{
  
  int j=0;
  int RIC_high_start=0,RIC_high_end=0;
  float time_at_max;
  int dither_wait_end;
  int led_no=0;
  //int num_valid_peaks=0,lag_of_max=0;
  char RIC_string[20],cycle_string[2];
  float batt_level;
  uint8_t count =0; 

  //Set Valve Parameters. Temporarily set to %100 Duty cycle
  ValvePwm_UpdateDutyCycle(DutyCycle_100);
 
  

  for(count = 1; count <= NUMBER_CYCLES; count++)
  {
    
    ValvePwm__Start();
    ValvePwm_UpdateDutyCycle(DutyCycle_100);
    
    tx_thread_sleep(2000);
    
    led_no = count + 1;
    LED_state[led_no] = 1;
    LED_state[1] = 1;
    LF_LED_Control(LED_state);

    strcpy(line1,"DETERMINING     ");
    strcpy(line2,"SYSTOLE         ");
    
    
    tx_timer_activate(&LedFlasher_Timer);
    
    sprintf(BluetoothMsg,"%s \r\n",line1);
    sprintf(BluetoothMsg,"%s \r\n",line2);
    
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    //LF_LCD_Control(line1,line2);
   
    step_inflate(MAX_PREAMBLE_PRESSURE);
    
    if (pressure < sys_press + OCCLUSION_PRESSURE_INC)
    {
      pump_to_pressure(sys_press + OCCLUSION_PRESSURE_INC,250);
    }
    else
    {
      bleed_to_pressure(sys_press + OCCLUSION_PRESSURE_INC,0);
    }

    RIC_high_start = tock;
    RIC_high_end = RIC_high_start + SAMPLE_RATE * TIME_AT_MAX;
    sprintf(BluetoothMsg, "RIC_high_start = %d   RIC_high_end = %d\n",RIC_high_start,RIC_high_end);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

    time_at_max = 0.0f;
    
    RIC_inflation = 1;
    tx_timer_create (&Inflation_Timer, "Duration_Timer", DurationTimer,
    0x1234, 360000, 0,
    TX_AUTO_ACTIVATE);

    while(time_at_max < TIME_AT_MAX)
    {

      dither();
      dither_wait_end = tock + SAMPLE_RATE * DITHER_INTERVAL;
      if(done_flashing && count == NUMBER_CYCLES)
      {
	RIC_shutdown();
      }
      else if(done_flashing) 
      {
        break;
      }

      if (tock >= RIC_high_end)
      {  
	sprintf(BluetoothMsg, "RIC_high_end = %d   tock = %d\n",RIC_high_end,tock);
        LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
	break;
      }
      // Do the pass-through do-si-do
      bleed_to_pressure(sys_press - 5.0,0);
      
      tx_thread_sleep(2000);
    
      if (pressure < sys_press + OCCLUSION_PRESSURE_INC)
      {
	pump_to_pressure(sys_press + OCCLUSION_PRESSURE_INC,250);
      }
      else
      {
	bleed_to_pressure(sys_press + OCCLUSION_PRESSURE_INC,0);
      }

      if(done_flashing && count == NUMBER_CYCLES)
      {
	RIC_shutdown();
      }
      else if(done_flashing)
      {
        break;
      }

      pulse = (int) calc_heart_rate(1);
      
     
      strcpy(line1,"OCCLUDING       ");
      strcpy(RIC_string,"RIC cycle ");
      
      cycle_string[0] = (char) (0x30 + count);
      cycle_string[1] = '\0';
      strcat(RIC_string,cycle_string);
      while(strlen(RIC_string) < 16)
      {
	strcat(RIC_string," ");
      }

      sprintf(BluetoothMsg,"RIC_string = %s  len = %d\n\r",RIC_string,strlen(RIC_string));
      LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
      strcpy(line2,RIC_string);
      sprintf(BluetoothMsg,"line1 = %s  length = %d\n\r",line1,strlen(line1));
      //LF_LCD_Control(line1,line2);
      tx_thread_sleep(3000);
      LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
      sprintf(BluetoothMsg,"line2 = %s  length = %d\n\r",line2,strlen(line2));
      LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

    
      if(done_flashing && count == NUMBER_CYCLES)
      {
	RIC_shutdown();
      }
      else if(done_flashing) 
      {
        break;
      }
      sprintf(BluetoothMsg, "sys_press = %f\n",sys_press);
      LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
      sprintf(BluetoothMsg, "time_at_max = %f   count = %d\n",time_at_max,count);
      LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

      while (tock < dither_wait_end)
      {
	if (tock >= RIC_high_end)
        {
	  sprintf(BluetoothMsg,"RIC_high_end = %d   tock = %d\n\r",RIC_high_end,tock);
          LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
	  break;
	}
        //tx_thread_sleep(5);
        tx_thread_sleep(5);
      }

      time_at_max += (float) DITHER_INTERVAL; 
      
      if (tock >= RIC_high_end)
      {  
	sprintf(BluetoothMsg, "RIC_high_end = %d   tock = %d\n",RIC_high_end,tock);
        LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

	if(done_flashing && count == NUMBER_CYCLES)
        {
	  RIC_shutdown();
        }
	else if(done_flashing) 
        {
          break;
        }

	break;
      }

    }
    
    RIC_inflation = 0;
    tx_timer_deactivate(&Inflation_Timer);
    tx_timer_delete(&Inflation_Timer);
    done_flashing = 0;
    sprintf(BluetoothMsg, "RIC_high_end = %d   tock = %d\n",RIC_high_end,tock);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    strcpy(line1,"DEFLATING       ");
    strcpy(line2,RIC_string);        
    bleed_to_pressure(MIN_PRESSURE,0);
    RIC_report();
    tx_thread_sleep(3000);
    sprintf(BluetoothMsg,"about to switch inflate/deflate LEDs\n\r");
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    
    
    tx_timer_deactivate(&LedFlasher_Timer);

    for(j = 0; j < 24; j++)
    {
      LED_state[j] = -1;
    }
    LF_LED_Control(LED_state);
    
    tx_thread_sleep(1);
    LED_state[led_no] = 1;
    LED_state[0] = 1;
    LED_state[1] = -1;
    
    LF_LED_Control(LED_state);

    //sprintf(BluetoothMsg,"battery level = %f\n",batt_level);
    //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);


    tx_timer_activate(&LedFlasher_Timer);
  
    //strcpy(line1," V batt =       ");
    //sprintf(line2," %8.4f  ",batt_level);
    while(strlen(line2) < 16)
    {
      strcat(line2," ");
    }
  
    sprintf(BluetoothMsg,"%s\n",line1);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg,"%s\n",line2);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    //LF_LCD_Control(line1,line2);
  
    tx_thread_sleep(5000);

    strcpy(line1,"Deflation phase.");
    strcpy(line2,"                ");

    //LF_LCD_Control(line1,line2);

    if(count == NUMBER_CYCLES) 
    {
      RIC_shutdown();
    }
    RIC_deflation = 1;
    ValvePwm__Stop();
    
    tx_thread_sleep(TIMER_DELAY);
    RIC_deflation = 0;
    //timer2.detach();
    tx_timer_deactivate(&LedFlasher_Timer);

    LED_state[led_no] = -1;
    LED_state[0] = -1;
    for(j = 0; j < 24; j++)
    {
      LED_state[j] = -1;
    }
    LF_LED_Control(LED_state);
  
  }
}

void StdFixed__Algorithm(void)
{
  
  int j;
  char RIC_string[20],cycle_string[2];

  //Set Valve Parameters. Temporarily set to %100 Duty cycle
  ValvePwm_UpdateDutyCycle(DutyCycle_100);
  //Start Valve 15Khz PWM
  ValvePwm__Start();
  
  //Set Motor Parameters. Temporarily set to %50 Duty cycle
  PumpPwm_UpdateDutyCycle(DutyCycle_50);
  //Start Motor 15Khz PWM
  PumpPwm__Start();
  cycle_num = 1;
  pump_to_pressure(200.0,250);
  sprintf(BluetoothMsg, "battery = %f V, system time = %d\n",LF__ADS115__ReadBattVoltage(),tock);
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg,"#5  %d\n\r",cycle_num);
  LF_SendToBluetooth(BluetoothMsg);
  LF__ST7036__LCDclear();
  LF__ST7036__LCDwrite("OCCLUDING           ",0);
  strcpy(RIC_string,"RIC cycle ");
  cycle_string[0] = (char) (0x30 + cycle_num);
  cycle_string[1] = '\0';
  strcat(RIC_string,cycle_string);
  for (j = strlen(RIC_string)-1; j < 19; j++)
  {
    strcat(RIC_string," ");
  }
  RIC_string[19] = '\0';
  LF__ST7036__LCDwrite(RIC_string,1);
  tx_thread_sleep(TIMER_DELAY);
  
  bleed_to_pressure(10.0,0);
  LF__ST7036__LCDwrite("CUFF RELEASED       ",0);
  strcpy(RIC_string,"RIC cycle ");
  cycle_string[0] = (char) (0x30 + cycle_num);
  cycle_string[1] = '\0';
  strcat(RIC_string,cycle_string);
  for (j = strlen(RIC_string)-1; j < 19; j++)
  {
    strcat(RIC_string," ");
  }
  RIC_string[19] = '\0';
  LF__ST7036__LCDwrite(RIC_string,1);
  tx_thread_sleep(TIMER_DELAY);
 
  bleed_to_pressure(10.0,0);
  cycle_num = 2;
  pump_to_pressure(200.0,250);
  sprintf(BluetoothMsg, "battery = %f V, system time = %d\n",LF__ADS115__ReadBattVoltage(),tock);
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg,"#5  %d\n\r",cycle_num);
  LF_SendToBluetooth(BluetoothMsg);
  LF__ST7036__LCDwrite("OCCLUDING           ",0);
  strcpy(RIC_string,"RIC cycle ");
  cycle_string[0] = (char) (0x30 + cycle_num);
  cycle_string[1] = '\0';
  strcat(RIC_string,cycle_string);
  for (j = strlen(RIC_string)-1; j < 19; j++)
  {
    strcat(RIC_string," ");
  }
  RIC_string[19] = '\0';
  LF__ST7036__LCDwrite(RIC_string,1);
  
  tx_thread_sleep(TIMER_DELAY);
  
  bleed_to_pressure(10.0,0);
  LF__ST7036__LCDwrite("CUFF RELEASED       ",0);
  strcpy(RIC_string,"RIC cycle ");
  cycle_string[0] = (char) (0x30 + cycle_num);
  cycle_string[1] = '\0';
  strcat(RIC_string,cycle_string);
  for (j = strlen(RIC_string)-1; j < 19; j++)
  {
    strcat(RIC_string," ");
  }
  RIC_string[19] = '\0';
  LF__ST7036__LCDwrite(RIC_string,1);
  
  tx_thread_sleep(TIMER_DELAY);
  
  cycle_num =  3;
  pump_to_pressure(200.0,250);
  sprintf(BluetoothMsg, "battery = %f V, system time = %d\n",LF__ADS115__ReadBattVoltage(),tock);
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg,"#5  %d\n\r",cycle_num);
  LF_SendToBluetooth(BluetoothMsg);
  LF__ST7036__LCDclear();
  LF__ST7036__LCDwrite("OCCLUDING           ",0);
  strcpy(RIC_string,"RIC cycle ");
  cycle_string[0] = (char) (0x30 + cycle_num);
  cycle_string[1] = '\0';
  strcat(RIC_string,cycle_string);
  for (j = strlen(RIC_string)-1; j < 19; j++)
  {
    strcat(RIC_string," ");
  }
  RIC_string[19] = '\0';
  LF__ST7036__LCDwrite(RIC_string,1);

  tx_thread_sleep(TIMER_DELAY);
  
  bleed_to_pressure(10.0,0);
  LF__ST7036__LCDwrite("CUFF RELEASED       ",0);
  strcpy(RIC_string,"RIC cycle ");
  cycle_string[0] = (char) (0x30 + cycle_num);
  cycle_string[1] = '\0';
  strcat(RIC_string,cycle_string);
  for (j = strlen(RIC_string)-1; j < 19; j++)
  {
    strcat(RIC_string," ");
  }
  RIC_string[19] = '\0';
  LF__ST7036__LCDwrite(RIC_string,1);
  tx_thread_sleep(TIMER_DELAY);
  cycle_num =  4;
  pump_to_pressure(200.0,250);
  sprintf(BluetoothMsg, "battery = %f V, system time = %d\n",LF__ADS115__ReadBattVoltage(),tock);
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg,"#5  %d\n\r",cycle_num);
  LF_SendToBluetooth(BluetoothMsg);
  LF__ST7036__LCDwrite("OCCLUDING           ",0);
  strcpy(RIC_string,"RIC cycle ");
  cycle_string[0] = (char) (0x30 + cycle_num);
  cycle_string[1] = '\0';
  strcat(RIC_string,cycle_string);
  for (j = strlen(RIC_string)-1; j < 16; j++)
  {
    strcat(RIC_string," ");
  }
  RIC_string[1] = '\0';
  LF__ST7036__LCDwrite(RIC_string,1);
  tx_thread_sleep(TIMER_DELAY);
  
  bleed_to_pressure(10.0,0);
  LF__ST7036__LCDwrite("CUFF RELEASED       ",0);
  strcpy(RIC_string,"RIC cycle ");
  cycle_string[0] = (char) (0x30 + cycle_num);
  cycle_string[1] = '\0';
  strcat(RIC_string,cycle_string);
  for (j = strlen(RIC_string)-1; j < 16; j++)
  {
    strcat(RIC_string," ");
  }
  RIC_string[16] = '\0';
  LF__ST7036__LCDwrite(RIC_string,1);
  tx_thread_sleep(TIMER_DELAY);
 
  ValvePwm__Stop(); 
}


void step_inflate(float max_pressure)
{

  int i;
  int inflating = 1;
  int num_levels = 0;
  int haveMAP = 0;
  int max_num_levels = MAX_NUM_LEVELS;
  int num_pulses;
  int scr_ptr;
  float amp;
  float pressure_target = 40.0;  // Point at which steps begin.
  float osc_data[MAX_NUM_LEVELS][2];
  
  char showThis[20];
  memset(showThis,'\0',20);

  sprintf(BluetoothMsg,"in step_inflaten\n\r");
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  sprintf(BluetoothMsg,"battery level = %f\n\r",batt_level);
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
 

  if(mode_code == MODE_E)
  {
    sprintf(showThis,"NIBP MODE: #%d",LF_GetFileIndex());
  }
  else
  {
    sprintf(showThis,"RIC MODE: #%d",LF_GetFileIndex());
  }

  while(strlen(showThis) < 16)
  {
    strcat(showThis," ");
  }

  strcpy(line1,showThis);
  LF_SendToBluetooth(showThis);
  strcpy(line2,"                ");
  //LF_LCD_Control(line1,line2);

  sprintf(BluetoothMsg,"Target Pressure %f\n",pressure_target);
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  pump_to_pressure(pressure_target,250);

  while (inflating)
  {
    tx_thread_sleep(BUFFER_TIME_MS);
    
    //check_tock();
     
    scr_ptr = dptr - 2;
    if(scr_ptr < 0) scr_ptr += 2200;

#ifdef COLLECT_ON_INFLATE
    num_pulses = get_osc_amp(scr_ptr,&amp,1,inflating&&haveMAP);
    
#else 
    sprintf(BluetoothMsg,"about to call get_osc_amp\n");
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    num_pulses = get_osc_amp(scr_ptr,&amp,0,inflating&&haveMAP);
#endif

    osc_data[num_levels][0] = avg_pressure;
    osc_data[num_levels][1] = amp;

    num_levels++;

    for (i = 0; i < num_levels; i++)
    {
      sprintf(BluetoothMsg,"%f\t %f\n",osc_data[i][0],osc_data[i][1]);
      LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    }

    if ((pressure > 110) && (haveMAP == 0) && (num_levels >= 3))
    {

      haveMAP = check_for_MAP(osc_data,num_levels);

      if (haveMAP)
      {

	sprintf(BluetoothMsg,"MAP solution = %f\n",MAP);
        LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
        
	max_num_levels = MAP_index + 5;  // Need sufficient data for systolic solution.

	sprintf(BluetoothMsg,"MAP_index = %d   max_num_levels = %d\n",MAP_index,max_num_levels);
        LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

	if (max_num_levels > MAX_NUM_LEVELS)
        {
	  max_num_levels = MAX_NUM_LEVELS;
        }

      }
    }

    pressure_target += INFLATION_PRESSURE_STEP;

    if (num_levels > max_num_levels || pressure_target > max_pressure || num_levels >= MAX_NUM_LEVELS)
    {
      inflating = 0;
    }
    else
    {
      pump_to_pressure(pressure_target,250);
    }
  }

  calc_sys_inflate(osc_data,num_levels);

  sprintf(BluetoothMsg,"MAP_index = %d   num_levels = %d\n",MAP_index,num_levels);
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

  for (i = 0; i < num_levels; i++)
  {
    sprintf(BluetoothMsg,"%f   %f\n",osc_data[i][0],osc_data[i][1]);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  }

  LF_OpenAppendFileAndBLE(&fp,LogFileName,"\r\n");

#ifdef COLLECT_ON_INFLATE
  sprintf(BluetoothMsg,"Raw data collected on step-inflate\n");
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
 
  dias_on_deflate = 0;

  for (i = 0; i < BUFFER_SIZE; i++)
  {
    sprintf(BluetoothMsg,"%f\n",level_data[i]);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  }
       
  //Stop Motor
   PumpPwm__Stop();


  sprintf(BluetoothMsg,"Shutting down.\r\n");
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
#endif
}

void pump_to_pressure(float target_pressure,int pulsewidth)
{
    int tock_now;
    float pressure_target;

    //pc.printf("in pump_to_pressure, pressure = %f\n\r",pressure);

    if(target_pressure > 250.0) 
    {
      pressure_target = 250.0;
    }
    else
    {
      pressure_target = target_pressure;
    }

    tock_now = tock;
    //motorpwm.pulsewidth_us(pulsewidth);
    PumpPwm_UpdateDutyCycle(DutyCycle_50);
    PumpPwm__Start();
    while(tock < tock_now+100000 && pressure < pressure_target)
    {
        tx_thread_sleep(15);
        
       
        //check_tock();
        
	//if(INT_1 == 0) overpressure_abort();
    }
    
    //Stop Motor
    PumpPwm__Stop();
    return;
} 

void bleed_to_pressure(float target_pressure, int pulsewidth)
{

  //ValvePwm_UpdateDutyCycle(DutyCycle_50);
   
  ValvePwm__Stop();

  while(pressure > target_pressure)
  {
    tx_thread_sleep(25);
   
    //check_tock();
    
  }
  
  ValvePwm_UpdateDutyCycle(DutyCycle_100);
  ValvePwm__Start();
  sprintf(BluetoothMsg,"1st kototkoff = %d  2nd korotkoff = %d\n",korot1,korot2);
  LF_SendToBluetooth(BluetoothMsg);
  return;
 }
 

 
void dither(void)
{
 
    int level,i;
    int scr_ptr,num_pulses;
    float amp;
    float osc_data[6][2];

    strcpy(line1,"TRACKING        ");
    strcpy(line2,"SYSTOLE         ");
    //LF_LCD_Control(line1,line2);
    
    osc_data[0][0] = MAP;
    osc_data[0][1] = MAP_amp;
    
    level = 1;
    while(level <= NUMBER_DITHER_LEVELS)
    {
        
        tx_thread_sleep(BUFFER_TIME_MS);
        scr_ptr = dptr - 2;
	if(scr_ptr < 0) dptr += 2200;
        num_pulses = get_osc_amp(scr_ptr,&amp,0,0);
        osc_data[level][0] = avg_pressure;
        osc_data[level][1] = amp;

	sprintf(BluetoothMsg, "pressure = %f  avg_pressure = %f\n",pressure,avg_pressure);
        LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
        bleed_to_pressure(avg_pressure-5.0,25);
    
        level++;
    }

    
    LF_OpenAppendFileAndBLE(&fp,LogFileName,Separator);
    
    for(i=0;i<=NUMBER_DITHER_LEVELS;i++)
    {
      sprintf(BluetoothMsg,"%f    %f\n\r",osc_data[i][0],osc_data[i][1]);
      LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

    }
    //LF_OpenAppendFileAndBLE(&fp,LogFileName,Separator);

    calc_sys_dither(osc_data);
    sprintf(BluetoothMsg,"dithered systolic pressure = %f\n\r",sys_press);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

    return; 
} 

void RIC_shutdown(void)
{

  int i=0;
  float batt_level= 0.0;
  // Open Valve
  PumpPwm__Stop();
  ValvePwm__Stop();
  
  sprintf(BluetoothMsg,"exit afterlast inflated segment.\r\n");
  strcpy(line1,"  RIC therapy   ");
  strcpy(line2,"   complete.    ");
  LF_LCD_Control(line1,line2);
  sprintf(BluetoothMsg,"%s\n",line1);
  sprintf(BluetoothMsg,"%s\n",line2);
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  
  
  for(i = 0; i < 24; i++)
  {
    LED_state[i] = -1;   // switch off all LEDs
  }
  
  LF_LED_Control(LED_state);
  
  //enable_PA_11 = 1;
  //timer.detach();
  //batt_level = LF__ADS115__ReadBattVoltage();
  //sprintf(BluetoothMsg,"battery level = %f\n",batt_level);
  //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  //enable_PB_2 = 0;  // Turn off BL.
  //stop timer 2
  //timer2.detach();
  tx_timer_deactivate(&LedFlasher_Timer);
  

}



float calc_heart_rate(int reset_data)
{

  float heart_rate = 0.0;
  int i,sum_period,num_valid_peaks;
  int lag_of_max;

  sprintf(BluetoothMsg,"npeak = %d\n\r",npeak);
  sprintf(BluetoothMsg,"list for heart-rate calculation\n\r");
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

  sprintf(BluetoothMsg, "npeak = %d\n",npeak);
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  sprintf(BluetoothMsg,"list for heart-rate calculation\n"); 
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
 

  for(i=0;i<npeak;i++)
  {
    sprintf(BluetoothMsg,"%d    %f\n",peak_data[i].tval,peak_data[i].amp);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  }
  
  compute_tuck_acf();
  
  sprintf(BluetoothMsg,"ACF data\n");
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);

  for(i=0;i<900;i++)
  {
    sprintf(BluetoothMsg,"%f\n",acf[i]);
    LF_SendToBluetooth(BluetoothMsg);
   
  }
      
  for(i=0;i<200;i++)
  {
    intervals[i] = 0;
  }

  sum_period = 0;
  sum_period = 0;
  num_valid_peaks = 0;

  for(i=1;i<npeak;i++)
  {
    intervals[i] = peak_data[i].tval - peak_data[i-1].tval;
    if(intervals[i] < MIN_LAG || intervals[i] > MAX_LAG)
    {
      intervals[i] = 0;
    }
    else
    {
      sum_period += intervals[i];
      num_valid_peaks++;
    }
  }

  lag_of_max = find_lag_acf_max();

  heart_rate = 30000/lag_of_max;
     
 
  sprintf(BluetoothMsg,"heart_rate = %f\n",heart_rate);
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  sprintf(BluetoothMsg,"report return = %d\n",report((int) heart_rate));
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  sprintf(BluetoothMsg,"pulse intervals:\r\n");
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  
  for(i=0;i<npeak;i++)  
  {
    sprintf(BluetoothMsg,"%f\n",acf[i]);
    LF_SendToBluetooth(BluetoothMsg);
  }

  if(reset_data)
  {
    // Reset the heart rate calculation data.
    npeak = 0;
    for(i = 0; i < 900; i++)
    {
      acf[i] = 0.0;
    }
  }

  return(heart_rate);
}

int find_lag_acf_max(void)
{

  int i,lag_of_max;
  float max_acf;

  max_acf = acf[MIN_LAG];
  lag_of_max = MIN_LAG;

  for(i = MIN_LAG+1; i <= MAX_LAG; i++)
  {
    if(max_acf < acf[i])
    {
      max_acf = acf[i];
      lag_of_max = i;
    }
  }

  sprintf(BluetoothMsg,"max_acf = %f   lag_of_max = %d\n",max_acf,lag_of_max);
  LF_SendToBluetooth(BluetoothMsg);

  return lag_of_max;
}


int report(int heart_rate)
{

  //char string1[20],string2[20];
  char sys_string[4],dias_string[4],MAP_string[4],pulse_string[4];
  char hr_string[4];
  

  make_output_string((int) (sys_press+0.5),sys_string);
  make_output_string((int) (MAP+0.5),MAP_string);
  make_output_string((int) (dias_press+0.5),dias_string);
  make_output_string((int) (pulse+0.5),pulse_string);
  make_output_string(heart_rate,hr_string);

  strcpy(string1,"BP");
  strcat(string1,sys_string);
  strcat(string1,"/");
  strcat(string1,dias_string);
  strcat(string1," HR");
  strcat(string1,hr_string);
  strcat(string1,"   ");

  
  while(strlen(string1)<16)
  {
    strcat(string1," ");
  }
  string1[16] = '\0';

  sprintf(BluetoothMsg,"in report, string1 = %s len = %d\n\r",string1,strlen(string1));
  LF_SendToBluetooth(BluetoothMsg);
  strcpy(line1,string1);
  sprintf(BluetoothMsg,"in report, line1 = %s len = %d\n\r",line1,strlen(string1));
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg,"early: string1 = %s\n",string1);
  LF_SendToBluetooth(BluetoothMsg);

  line1[16] = '\0';

  if(mode_code == MODE_E)
  {
    strcpy(string2,"      ");
  }

  strcat(string2," ");

  if(mode_code == 4)
  {
    strcat(string2,korot_string);
    sprintf(BluetoothMsg,"test: korot_string = %s\n",korot_string);
    LF_SendToBluetooth(BluetoothMsg);
  }

  
  while(strlen(string2)<16)
  {
    strcat(string2," ");
  }
  string2[16] = '\0';
  
  strcpy(line2,"                ");
  sprintf(BluetoothMsg,"line2 = %s,  len = %d\n\r",line2,strlen(line2));
  LF_SendToBluetooth(BluetoothMsg);
  
  //LF_LCD_Control(line1,line2);
  tx_thread_sleep(2000);
  

  return 0;
}


void make_output_string(int pressure, char pressure_string[])
{
  // Also used to make string of pulse rate. 
    int digit1,digit2,digit3;
    int working_pressure;

    working_pressure = pressure;
    
    if(pressure <= 10)
    {
        pressure_string[0] = '-';
        pressure_string[1] = '\0';
        return;
    }
    
    if(pressure >= 100)
    {
        digit1 = pressure / 100;
        working_pressure = working_pressure - digit1*100;
    }
    else
    {
        working_pressure = pressure;
        digit1 = 0;
    }
    
    digit2 = working_pressure / 10;
    digit3 = working_pressure - digit2*10;
    
    // Build the string.
    
    if(digit1 != 0)
    {
        pressure_string[0] = (char) (0x30 + digit1);
        pressure_string[1] = (char) (0x30 + digit2);
        pressure_string[2] = (char) (0x30 + digit3);
        pressure_string[3] = '\0';
    }
    else
    {
        pressure_string[0] = (char) (0x30 + digit2);
        pressure_string[1] = (char) (0x30 + digit3);
        pressure_string[2] = '\0';
    }
    return;
}

void compute_tuck_acf(void)
{

  int i,j,lag;

  for(i = 0; i < npeak ; i++)
  {
    for(j=i+1 ;j < npeak; j++)
    {
      lag = peak_data[j].tval - peak_data[i].tval;
      if(lag > MAX_LAG) 
      {
        break;
      }
      if(lag >= MIN_LAG)
      {
      acf[lag] += peak_data[i].amp * peak_data[j].amp;
      }
    }
  }
  return;
}

void calc_sys_dither(float osc_data[NUMBER_DITHER_LEVELS+1][2])
{
    
    int i,iter,max_iter;
    float x_scale[MAX_NUM_LEVELS],a_scale[MAX_NUM_LEVELS],last_systolic;
    double a,mu,sigma,x;
    double a_last,mu_last,sigma_last,eps,chisq,chisq_last,resid;
    double deriv_a,deriv_mu,deriv_sigma;
    
#ifdef DEBUG_DATA_GATHERING
    double a_data[20],mu_data[20],sigma_data[20],chisq_data[20];
#endif
    
    last_systolic = sys_press;

    // Scale the data. Will always have the MAP entries at the zero index.
    
    for(i = 0; i <= NUMBER_DITHER_LEVELS; i++)
    {
      //      x_scale[i] = osc_data[i][0]/osc_data[0][0];
      x_scale[i] = (osc_data[i][0] -osc_data[0][0])/osc_data[0][0];
      a_scale[i] = osc_data[i][1]/osc_data[0][1];
    }
    
    LF_OpenAppendFileAndBLE(&fp,LogFileName,Separator);
    
    for(i = 0; i<=NUMBER_DITHER_LEVELS; i++)
    {
        sprintf(BluetoothMsg,"x = %f   a = %f\n\r",x_scale[i],a_scale[i]);
        LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    }
        
    LF_OpenAppendFileAndBLE(&fp,LogFileName,Separator);     
    
    a = 1.0;
    a_last = 1.0;
    mu = 0.55;
    mu_last = mu;
    sigma = 0.25;
    sigma_last = sigma;
    iter = 0;
    max_iter = 200;
    eps = 1.0;
    chisq = 1000;
    chisq_last = chisq;
    
    while(iter < max_iter && eps >= 1.0e-5)
    {
        
        chisq = 0.0;
        deriv_a = 0.0;
        deriv_mu = 0.0;
        deriv_sigma = 0.0;
        
        for(i = 0;i < NUMBER_DITHER_LEVELS; i++)
        {
            resid = a_scale[i]-a_last-(1.0/M_PI)*atan((mu_last-x_scale[i])/sigma_last);
            chisq += resid*resid;
            deriv_a += -2.0*resid;
            deriv_mu += -2.0*resid*(sigma_last/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
            deriv_sigma += -2.0*resid*(-(mu_last-x_scale[i])/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
        }
        
        a = a_last -0.01*deriv_a;
        mu = mu_last - 0.01*deriv_mu;
        sigma = sigma_last - 0.01*deriv_sigma;
        
#ifdef DEBUG_DATA_GATHERING
        if (iter < 20)
        {
            chisq_data[iter] = chisq;
            a_data[iter] = a;
            mu_data[iter] = mu;
            sigma_data[iter] = sigma;
        }
#endif
        // 
        eps = fabs(chisq -chisq_last)/chisq_last;
        
        // Check if chi squared is increasing and exit the loop if it is.
        
        if (chisq > chisq_last || sigma < 0)
        {
            a = a_last;
            mu = mu_last;
            sigma = sigma_last;
            chisq = chisq_last;
            iter = 1000;  // This value is diagnostic for this loop exit.
            break;
        }
        
        a_last = a;
        mu_last = mu;
        sigma_last = sigma;
        chisq_last = chisq;
        
        iter++;
    }
    
    x = binary_chop(a,mu,sigma,0.4);

    sys_press = (float) (1.0 + x)*MAP;
  
    LF_OpenAppendFileAndBLE(&fp,LogFileName,"\r\n");
    sprintf(BluetoothMsg, "iter = %d\n",iter);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg, "chisq = %lf\n",chisq);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg, "a = %lf\n",a);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg, "mu = %lf\n",mu);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg, "sigma = %lf\n",sigma);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg, "MAP = %f\n",MAP);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg, "systolic pressure = %f\n",sys_press);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,"\r\n");
    
    if(chisq > 1.0 || chisq == 0.0 || sigma < 0.0)
    {
      LF_OpenAppendFileAndBLE(&fp,LogFileName,"rejection systolic fit\n\r");
      sys_press = last_systolic;   // coast on last good value
    }

}

double binary_chop(double a,double mu,double sigma,double frac)
{

  double x_left,x_middle,x_right;
  double f_left,f_middle;
  int iter = 0;

  x_left = 0.0;
  x_middle = 1.0;
  x_right = 2.0;

  f_left = model(a,mu,sigma,x_left);
  f_middle = model(a,mu,sigma,x_middle);


  while (x_right - x_left > 1.0e-5)
  {
    if (f_middle == frac) 
    {
      return x_middle;
    }
    if (f_middle > frac && f_left > frac)
    {
      f_left = f_middle;
      x_left = x_middle;
      x_middle = (x_left + x_right)/2.0;
      f_middle = model(a,mu,sigma,x_middle);
    }
    else
    {
      x_right = x_middle;
      x_middle = (x_left + x_right)/2.0;
      f_middle = model(a,mu,sigma,x_middle);
    }
    iter++;
  }
 
  return x_middle;
}

double model(double a,double mu,double sigma,double x)
{

  return (a + (1.0/M_PI)*atan((mu-x)/sigma));
}


int SP10(char version[20],char string1[20],char string2[20])
{

  float batt_level;
  float heart_rate;
  int i;
    
  while(1)
  {   // Loop forever
    
    sprintf(BluetoothMsg, "%s\n",version);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg, "Beginning main loop.\r\n");
    LF_SendToBluetooth(BluetoothMsg);

    deflating = 0;
    strcpy(string1,"                    ");
    strcpy(string2,"                    ");
    LF_LCD_Control(string1,string2);
    //batt_level = LF__ADS115__ReadBattVoltage();

    //sprintf(BluetoothMsg, "battery level = %f\n",batt_level);
    //LF_SendToBluetooth(BluetoothMsg);

    if( batt_level < NIBP_MIN_BATT_VOLTAGE ) 
    { 
      shutdown();
    }    

    sprintf(BluetoothMsg, "Beginning startup.\r\n");
    LF_SendToBluetooth(BluetoothMsg);
    
    //  Initialization code
    running = 1;
    
    ValvePwm_UpdateDutyCycle(DutyCycle_100);
    //Start Valve PWM
    ValvePwm__Start();
    
    PumpPwm_UpdateDutyCycle(DutyCycle_50);
    //Start Motor PWM
    PumpPwm__Start();
        
    LF_SendToBluetooth("Startup complete\n");
    sprintf(BluetoothMsg, "battery = %f V, system time = %d\n",batt_level,tock);
    LF_SendToBluetooth(BluetoothMsg);

    dias_on_deflate = 0;
    LF_SendToBluetooth("Beginning NIBP processing.\r\n");
    
    NIBP();
    
    tx_thread_sleep(3000);
    

    //  Shut down
    PumpPwm__Stop();
    ValvePwm__Stop();
   
    LF_SendToBluetooth("Shutting down.\r\n");
     
    heart_rate = calc_heart_rate(1);
    sprintf(BluetoothMsg,"heart_rate = %f\n",heart_rate);
    LF_SendToBluetooth(BluetoothMsg);
    running = 0;
      
    running = 0;
    deflating = 0;
    korot1 = 0;
    korot2 = 0;
    korot_string[0] = ' ';
    korot_string[1] = '\0';
    sys_press = 0.0;
    dias_press = 0.0;

    tx_thread_sleep(1000);
    
    // Reset the heart rate calculation data.

    npeak = 0;
    for(i = 0; i < 900; i++)
    {
      acf[i] = 0.0;
    }

  }  // Closing brace for unending while

    
}



void convex_MAP(float osc_data[MAX_NUM_LEVELS][2],int maxLoc)
{

  int i;
  double x[5],y[5];
  double Y,Y_X,Y_X2,X,X2,X3,X4,xx;
  double detN,detN1,detN2,detN3;
  double a,b,c;
  int npts = 5;      // Will assume fit is always to 5 data points symmetrically about the maximum measured 
                     // oscilometric amplitude.

  // Put the data in convenient arrays.

  for (i = 0; i < npts; i++)
  {
    x[i] = (double) (osc_data[maxLoc + i - 2][0]/100.0);
    y[i] = (double) osc_data[maxLoc + i - 2][1];
  }

  sprintf(BluetoothMsg,"maxLoc = %d\n",maxLoc);
  LF_SendToBluetooth(BluetoothMsg);

  for (i = 0; i < npts; i++)
  {
    sprintf(BluetoothMsg,"  %lf    %lf\n",x[i],y[i]);
    LF_SendToBluetooth(BluetoothMsg);
  }

  // Create all the entries for the moment equations.

  Y = 0.0;
  Y_X = 0.0;
  Y_X2 = 0.0;
  X = 0.0;
  X2 = 0.0;
  X3 = 0.0;
  X4 = 0.0;

  for (i = 0; i < npts; i++)
  {
    Y += y[i];
    Y_X += y[i]*x[i];
    Y_X2 += y[i]*x[i]*x[i];
    X += x[i];
    X2 += x[i]*x[i];
    X3 += pow(x[i],3);
    X4 += pow(x[i],4);
  }

  sprintf(BluetoothMsg,"Y = %lf   Y_X = %lf   Y_X2 = %lf\n",Y,Y_X,Y_X2);
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg,"X = %lf   X2 = %lf   X3 = %lf   X4 = %lf\n",X,X2,X3,X4);
  LF_SendToBluetooth(BluetoothMsg);

  // Will solve by Cramer's Rule. Because it's only a 3x3 system, we can write out the determinants directly.

  detN = npts*X2*X4 + 2*X*X2*X3 - (npts*X3*X3 + X*X*X4 + pow(X2,3));

  detN1 = npts*X2*Y_X2 + X*X2*Y_X + X*X3*Y - (Y*X2*X2 + Y_X2*X*X + npts*Y_X*X3);

  detN2 = npts*X4*Y_X + Y*X3*X2 + Y_X2*X2*X - (Y_X*X2*X2 + Y*X*X4 + npts*Y_X2*X3);

  detN3 = X4*X2*Y + X3*X*Y_X2 + X2*Y_X*X3 - (Y_X2*X2*X2 + X4*X*Y_X + Y*X3*X3);

  sprintf(BluetoothMsg,"detN = %lf   detN1 = %lf   detN2 = %lf   detN3 = %lf\n",detN,detN1,detN2,detN3);
  LF_SendToBluetooth(BluetoothMsg);

  a = detN1/detN;

  b = detN2/detN;

  c = detN3/detN;

  sprintf(BluetoothMsg,"a = %lf   b = %lf\n",a,b);
  LF_SendToBluetooth(BluetoothMsg);

  
  MAP = (float) -b*100.0/(2*a);

  xx = -b/(2*a);

  MAP_amp = a*xx*xx + b*xx + c;
}





int check_for_MAP_deflate(float osc_data[MAX_NUM_LEVELS][2], int num_levels)
{

  int i,maxLoc;
  float maxAmp,minAmp,peak_dist;
  float old_MAP,old_amp;

  maxAmp = osc_data[0][1];
  minAmp = osc_data[0][1];
  maxLoc = 0;

  for (i = 1; i < num_levels; i++)
  {
    if (osc_data[i][1] > maxAmp)
    {
      maxAmp = osc_data[i][1];
      maxLoc = i;
    }
    if (osc_data[i][1] < minAmp)
    {
      minAmp = osc_data[i][1];
    }
  }

  if (maxAmp - minAmp < 1.0) 
    {
      return(0);
    }

  if (num_levels - maxLoc >= 3)
    {
    MAP_amp = maxAmp;
    MAP_index = maxLoc;
    MAP = osc_data[MAP_index][0];

    if (MAP < 70.0) 
    {
      return(0);
    }

    // Try convex fit for MAP and compare with old algorithm.

    old_MAP = MAP;
    old_amp = MAP_amp;

    convex_MAP(osc_data,maxLoc);
    // Also need to update MAP_index.
    peak_dist = fabs(osc_data[MAP_index][0] - MAP);
    for(i = num_levels - 1; i > 0; i--)
    {
      if(fabs(osc_data[i][0] - MAP) < peak_dist)
      {
	peak_dist = fabs(osc_data[i][0] - MAP);
	MAP_index = i;
      }
    }

    sprintf(BluetoothMsg,"old MAP = %f  convex MAP = %f\n",old_MAP,MAP);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"old amp = %f  new amp = %f\n",old_amp,MAP_amp);
    LF_SendToBluetooth(BluetoothMsg);

    // Have enough information to calculate systolic pressure.

    calc_sys_deflate(osc_data, num_levels);

    return(1);
  }
  else
    return(0);
}




void calc_sys_deflate(float osc_data[MAX_NUM_LEVELS][2], int num_levels)
{
    
    int i,iter,max_iter;
    float x_scale[MAX_NUM_LEVELS],a_scale[MAX_NUM_LEVELS];
    double a,mu,sigma,x,convergence;
    double a_last,mu_last,sigma_last,chisq,chisq_last,resid;
    double deriv_a,deriv_mu,deriv_sigma,delta_a,delta_mu,delta_sigma;
#ifdef DEBUG_DATA_GATHERING
    double a_data[20],mu_data[20],sigma_data[20],chisq_data[20];
#endif
    for(i = 0; i<= MAP_index; i++)
    {
      x_scale[i] = (osc_data[i][0]-MAP)/MAP;
      a_scale[i] = osc_data[i][1]/MAP_amp;
    }
    
    LF_SendToBluetooth("====================================\r\n");
    LF_SendToBluetooth("In deflation systolic determination.\r\n");
    
    for(i = 0; i<MAP_index; i++)
    {
        sprintf(BluetoothMsg,"x = %f   a = %f\n",x_scale[i],a_scale[i]);
        LF_SendToBluetooth(BluetoothMsg);
    }
        
    LF_SendToBluetooth("====================================\r\n");        
    
    a = 0.5;
    a_last = 0.5;
    mu = 0.55;
    mu_last = mu;
    sigma = 0.25;
    sigma_last = sigma;
    iter = 0;
    max_iter = 1000;
    convergence = 1.0;
    chisq = 1000;
    chisq_last = chisq;
    
    while(iter < max_iter && convergence >= 1.0e-6){
        
        chisq = 0.0;
        deriv_a = 0.0;
        deriv_mu = 0.0;
        deriv_sigma = 0.0;
    for (i = 0; i <= MAP_index; i++)
    {
         resid = a_scale[i]-a_last-(1.0/M_PI)*atan((mu_last-x_scale[i])/sigma_last);
         chisq += resid*resid;
         deriv_a += -2.0*resid;
         deriv_mu += -2.0*resid*(sigma_last/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
         deriv_sigma += -2.0*resid*(-(mu_last-x_scale[i])/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
    }

     a = a_last -0.01*deriv_a;
     mu = mu_last - 0.01*deriv_mu;
     sigma = sigma_last - 0.01*deriv_sigma;
        
#ifdef DEBUG_DATA_GATHERING
     if (iter < 20)
     {
          chisq_data[iter] = chisq;
          a_data[iter] = a;
          mu_data[iter] = mu;
          sigma_data[iter] = sigma;
      }
#endif

    delta_a = fabs(a - a_last)/a_last;
    delta_mu = fabs(mu - mu_last)/mu_last;
    delta_sigma = fabs(sigma - sigma_last)/sigma_last;
    convergence = delta_a;
    if(convergence < delta_mu) convergence = delta_mu;
    if(convergence < delta_sigma) convergence = delta_sigma;
               
    if (chisq > chisq_last || sigma < 0)
    {
       a = a_last;
       mu = mu_last;
       sigma = sigma_last;
       chisq = chisq_last;
       iter = 2000;  // This value is diagnostic for this loop exit.
       break;
     }
        
     a_last = a;
     mu_last = mu;
     sigma_last = sigma;
     chisq_last = chisq;
     iter++;
     
    }

    x = binary_chop(a,mu,sigma,0.45);

    sys_press = (float) (1 + x)*MAP;
  
    LF_SendToBluetooth("\r\n");
    sprintf(BluetoothMsg,"iter = %d\r\n",iter);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"chisq = %lf\r\n",chisq);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"a = %lf\r\n",a);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"mu = %lf\r\n",mu);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"sigma = %lf\r\n",sigma);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"MAP = %f\r\n",MAP);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"systolic pressure = %f\r\n",sys_press);
    LF_SendToBluetooth(BluetoothMsg);
    LF_SendToBluetooth("\r\n");

    if(chisq > 1.0 || chisq == 0.0 || sigma < 0.0)
    {
      LF_SendToBluetooth("rejection model fit\n\r");
      sys_press = 0.0;
    }
}






void NIBP(void)
{

  PumpPwm_UpdateDutyCycle(DutyCycle_50);
  //Start Motor PWM
  PumpPwm__Start();

  LF__ST7036__LCDwrite("NIBP mode           ",0);
  sprintf(BluetoothMsg,"battery = %f V, system time = %d\n",LF__ADS115__ReadBattVoltage(),tock);
  LF_SendToBluetooth(BluetoothMsg);

  step_inflate(MAX_PREAMBLE_PRESSURE);
  sprintf(BluetoothMsg,"battery = %f V, system time = %d\n",LF__ADS115__ReadBattVoltage(),tock);
  LF_SendToBluetooth(BluetoothMsg);
  deflating = 1;
  step_deflate();
  sprintf(BluetoothMsg,"battery = %f V, system time = %d\n",LF__ADS115__ReadBattVoltage(),tock);
  LF_SendToBluetooth(BluetoothMsg);
  LF_SendToBluetooth("Going back to main.\r\n");

}



void step_deflate(void)
{

  int i;
  int num_levels = 0;
  int scr_ptr,num_pulses;
  float osc_data[MAX_NUM_LEVELS][2];
  float pressure_target,amp,peak_dist;
  int deflate_state = 1;
  int haveMAP = 0;

  sprintf(BluetoothMsg,"In step deflation.\r\n");
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg,"sys_press = %f\n",sys_press);
  LF_SendToBluetooth(BluetoothMsg);
  
  if (pressure < sys_press + OCCLUSION_PRESSURE_INC)
  {
    sprintf(BluetoothMsg,"pump up branch, pressure = %f\n",pressure);
    LF_SendToBluetooth(BluetoothMsg);
    pump_to_pressure(sys_press + OCCLUSION_PRESSURE_INC,250);
  }
  else
  {
    sprintf(BluetoothMsg,"bleed branch, pressure = %f\n",pressure);
    LF_SendToBluetooth(BluetoothMsg);
    bleed_to_pressure(sys_press + OCCLUSION_PRESSURE_INC,0);
  }
  
  pressure_target = pressure;

  while (deflate_state)
  {

    tx_thread_sleep(BUFFER_TIME_MS);
    //check_tock();
    
    scr_ptr = dptr - 2;
    if(scr_ptr < 0) scr_ptr += 2200;

#ifdef COLLECT_ON_DEFLATE
    num_pulses = get_osc_amp(scr_ptr,&amp,1,0);
#else 
    num_pulses = get_osc_amp(scr_ptr,&amp,0,0);
#endif

    sprintf(BluetoothMsg,"Returned from get_osc_amp, num_pulses = %d\n",num_pulses);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"num_levels = %d\n",num_levels);
    LF_SendToBluetooth(BluetoothMsg);
    osc_data[num_levels][0] = avg_pressure;
    osc_data[num_levels][1] = amp;

    for (i = 0; i < num_levels; i++)
    {
      sprintf(BluetoothMsg,"%f\t%f\n",osc_data[i][0],osc_data[i][1]);
      LF_SendToBluetooth(BluetoothMsg);
    }
    num_levels++;
    
    if (haveMAP == 0)
    {
      haveMAP = check_for_MAP_deflate(osc_data,num_levels);
    }
    else
    {
      calc_sys_deflate(osc_data,num_levels);
      if(dias_on_deflate) 
      {
        return;
      }
    }

    pressure_target -= MINOR_DEFLATE_STEP;

    sprintf(BluetoothMsg,"pressure target = %f   num_levels = %d\n",pressure_target,num_levels);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"MAP_index = %d\n",MAP_index);
    LF_SendToBluetooth(BluetoothMsg);
    if (pressure_target < 35.0 || num_levels >= MAX_NUM_LEVELS)
    {
      deflate_state = 0;
    }
    else
    {
      bleed_to_pressure(pressure_target,0);
    }

  }

  // Recompute the MAP and systolic and diastolic pressures using all the oscillometric data.

  LF_SendToBluetooth("Global solution.:\r\n  ");
 
  MAP_amp = 0.0;
  MAP_index = 0;
  for(i = 0; i < num_levels; i++)
  {
    if(osc_data[i][1] > MAP_amp)
    {
      MAP_amp = osc_data[i][1];
      MAP_index = i;
    }
  }
  convex_MAP(osc_data,MAP_index);
  sprintf(BluetoothMsg,"MAP_amp = %f\n\r",MAP_amp);
  LF_SendToBluetooth(BluetoothMsg);

  // Also need to update MAP_index.
  peak_dist = fabs(osc_data[MAP_index][0] - MAP);
  for(i = num_levels - 1; i > 0; i--)
  {
    if(fabs(osc_data[i][0] - MAP) < peak_dist)
    {
      peak_dist = fabs(osc_data[i][0] - MAP);
      MAP_index = i;
    }
  }

  calc_sys_deflate(osc_data,num_levels);
  sprintf(BluetoothMsg,"diastolic calculation, num_levels = %d\n",num_levels);
  LF_SendToBluetooth(BluetoothMsg);
  calc_dias_deflate(osc_data,num_levels);
  sprintf(BluetoothMsg,"Finished global experiment.\r\n");
  LF_SendToBluetooth(BluetoothMsg);

  ValvePwm__Stop(); // Shut off valve to save battery.

#ifdef COLLECT_ON_DEFLATE
  LF_SendToBluetooth("Raw data collected on step-deflate\n");
 
  for (i = 0; i < BUFFER_SIZE; i++)
  {
    sprintf(BluetoothMsg,"%f\n",level_data[i]);
    LF_SendToBluetooth(BluetoothMsg);
  }

  //  Shut down
       
  PumpPwm__Stop();
  ValvePwm__Stop();   
 

  
#endif
}







void RIC_report(void)
{

  char string1[20],string2[20];
  char sys_string[4],dias_string[4],MAP_string[4],pulse_string[4];
  char number_string[2];
  int i,string_length;

  make_output_string((int) (sys_press+0.5),sys_string);
  make_output_string((int) (MAP+0.5),MAP_string);
  make_output_string((int) (dias_press+0.5),dias_string);
  make_output_string((int) (pulse+0.5),pulse_string);

  tx_thread_sleep(2000);
  

  strcpy(string1,"SYS ");
  strcat(string1,sys_string);
  strcat(string1,"   DIA ");
  strcat(string1,dias_string);

  string_length = strlen(string1);

  for (i=string_length;i<16;i++)
  {
    strcat(string1," ");
  }
  string1[16] = '\0';

  //strcpy(line1,string1);

#ifdef SERIAL
  sprintf(BluetoothMsg,"early: string1 = %s\n\r",string1);
  LF_SendToBluetooth(BluetoothMsg);
#endif

  sprintf(BluetoothMsg, "early: string1 = %s\n",string1);

  strcpy(string2,"MAP ");
  strcat(string2,MAP_string);
  
  number_string[0] = (char) (0x30 + cycle_num);
  number_string[1] = '\0';
  strcat(string2," cycle ");
  strcat(string2,number_string);
  string_length = strlen(string2);
  for (i=string_length-1;i<16;i++)
  {
    strcat(string2," ");
  }
  string2[16] = '\0';
  //strcpy(line2,string2);
 
#ifdef SERIAL
  sprintf(BluetoothMsg,"LCD: %s\n\r",string1);
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg,"LCD: %s\n\r",string2);
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg,"AA  %d\n\r",cycle_num);
  LF_SendToBluetooth(BluetoothMsg);
#endif

  sprintf(BluetoothMsg, "LCD: %s\n",string1);
  LF_SendToBluetooth(BluetoothMsg);
  sprintf(BluetoothMsg, "LCD: %s\n",string2);
  LF_SendToBluetooth(BluetoothMsg);
  
  return;
}

int get_osc_amp(int scr_ptr, float *avg_osc_amp, int control, int inflating)
{
     
  int  i,pulse1,pulse2,pulse_locs[10][2],number_pulses;
  
  int in_pulse,pulse_done;
  float fuzz;
 
    
  for(i=BUFFER_SIZE-1;i>=0;i--)
  {
    int_osc_data[i] = ADC_data[scr_ptr];
    f_data[i] = filt_data[scr_ptr];
    working_time[i] = tock_data[scr_ptr];
    scr_ptr--;
    if(scr_ptr < 0) scr_ptr = 2019;
  }
  
    
  sprintf(BluetoothMsg,"#####   %d\n",tock);
  //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  LF_SendToBluetooth(BluetoothMsg);
  for (i=100; i < BUFFER_SIZE; i++)
  {
    sprintf(BluetoothMsg,"%05u   %d\n",(unsigned int) int_osc_data[i],working_time[i]);
    //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    LF_SendToBluetooth(BluetoothMsg);
  }
  //LF_OpenAppendFileAndBLE(&fp,LogFileName,"\r\n");
  sprintf(BluetoothMsg,"^^^^^   %d\n",tock);
  //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
LF_SendToBluetooth(BluetoothMsg);
  deriv_data[0] = 0.0;
  for (i = 1; i < BUFFER_SIZE; i++)
  {
    deriv_data[i] = f_data[i] - f_data[i-1];  
  }

  // osc_data and deriv_data have the same time ordering as the buffers used in LabVIEW, i.e. increasing time with increasing index.
     
  // Start processing outside a pulse to simplify the pulse identification logic.
     
  fuzz = measure_fuzz(deriv_data);
  sprintf(BluetoothMsg,"fuzz = %f\n",fuzz);
  //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  LF_SendToBluetooth(BluetoothMsg);

  i = 100;
  while(i<BUFFER_SIZE && deriv_data[i]>0 && i < 150)
  {  // Clip off beginning of data buffer due to motor inertia.
    i++;
  }
     
  in_pulse = 0;
  pulse_done = 0;
  number_pulses = 0;
     
  while(i<BUFFER_SIZE && number_pulses<10)
  {
    if(f_data[i]-f_data[i-4]> fuzz && in_pulse==0)
    {
      in_pulse = 1;
      pulse1 = i;
      pulse_done = 0;
    }
    if(f_data[i]-f_data[i-4]<=-fuzz && in_pulse==1)
    {
      in_pulse = 0;
      pulse2 = i;
      pulse_done = 1;
    }
    if(pulse_done==1)
    {
      if(pulse2-pulse1 >= 5)
      {
        pulse_locs[number_pulses][0] = pulse1;
        pulse_locs[number_pulses][1] = pulse2;
        number_pulses++;
        in_pulse = 0;
        pulse_done = 0;
      }            
    }
    i++;
  }
    
  sprintf(BluetoothMsg,"number pulses = %d\n",number_pulses);
  //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
 LF_SendToBluetooth(BluetoothMsg);   
  // Pulses identified. Now find oscillometric amplitudes   
    
  float amps[10];

  for(i = 0; i < number_pulses; i++)
  {
    amps[i] = f_data[pulse_locs[i][1]] - f_data[pulse_locs[i][0]];
    //        pc.printf("amps = %f\n\r",amps[i]);
    if(npeak<200)
    {
      peak_data[npeak].tval = working_time[pulse_locs[i][1]];
      peak_data[npeak].amp = amps[i];
      npeak++;
    }
  }
        
  *avg_osc_amp = (float) 0.0;
  for(i = 0; i < number_pulses; i++)
  {
    *avg_osc_amp += amps[i];
  }
      
  if(number_pulses > 0)
  {
    *avg_osc_amp /= number_pulses;
    sprintf(BluetoothMsg,"avg_osc_amp = %f\n",*avg_osc_amp);
    LF_SendToBluetooth(BluetoothMsg);
    //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg,"number_pulses = %d\n",number_pulses);
    LF_SendToBluetooth(BluetoothMsg);
    //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    //LF_OpenAppendFileAndBLE(&fp,LogFileName,Separator);
    for(i = 0; i < number_pulses; i++)
    {
      sprintf(BluetoothMsg,"amp = %f  loc1 = %d  loc2 = %d\n",amps[i],pulse_locs[i][0],pulse_locs[i][1]);
      //LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
      LF_SendToBluetooth(BluetoothMsg);
    }
    //LF_OpenAppendFileAndBLE(&fp,LogFileName,Separator);

    return number_pulses;
  }
  else
  {
    return 0;
  }
}

float measure_fuzz(float data[])
{
  int i;
  float fuzz=0.0;

  for(i = 100; i < BUFFER_SIZE; i++) 
  {// Clip off beginning of data buffer due to motor inertia.
   fuzz += data[i]*data[i];
  }
  
  test_value = fuzz; 
  

  // Only approximate standard deviation because of round-off error.

  return sqrt(fuzz/(BUFFER_SIZE - 101)); 
}

void check_tock()
{
    if( tock != tockPrevious ) 
    { 
      tockPrevious = tock; 
      tockStuckCount=0; 
    }
    else
    {
        if(++tockStuckCount > 3)
        { 
          shutdownStuck();
        }
    }
    if( startButtonAbort == 9999 ) 
    { 
      shutdownAbort(); 
    }
    testBattery();
}


void testBattery()
{
    if( (++testBatteryModulus)%10 == 0)
    {
      if( (LF__ADS115__ReadBattVoltage()) < (float) RIC_MIN_BATT_VOLTAGE ) 
      {   
          if( ++testBatteryLowCount > 2 )
          {
              shutdown();
          }
          else if( testBatteryLowCount > 0 )
          {    testBatteryLowCount--;
          }    
      }           
    }
}




void shutdownAbort(void)
{
    
  running = 0;  
  PumpPwm__Stop();
  ValvePwm__Stop();
  sprintf(BluetoothMsg," SHUTTING DOWN - ABORT\n");
  LF_SendToBluetooth(BluetoothMsg);
  LF_LCD_Control("ABORT-SHUT DOWN","");
  NVIC_SystemReset();
}


void shutdownStuck(void)
{
  running = 0;
  PumpPwm__Stop();
  ValvePwm__Stop();
  sprintf(BluetoothMsg," SHUTTING DOWN - STUCK\n");
  LF_SendToBluetooth(BluetoothMsg);
  LF_LCD_Control("STUCK - SHUT DOWN","");
  NVIC_SystemReset();
}

void shutdown(void)
{
  running = 0;
  sprintf(BluetoothMsg, "BATTERY VOLTAGE LOW\n");
  LF_SendToBluetooth(BluetoothMsg); 
  LF_LCD_Control("REPLACE BATTERY ","");
  NVIC_SystemReset();
 
}

int check_for_MAP(float osc_data[MAX_NUM_LEVELS][2], int num_levels)
{

  int i,maxLoc;
  float maxAmp;

  maxAmp = osc_data[0][1];
  maxLoc = 0;

  for (i = 1; i < num_levels; i++)
  {
    if (osc_data[i][1] > maxAmp)
    {
      maxAmp = osc_data[i][1];
      maxLoc = i;
    }
  }

  if (num_levels - maxLoc >= 3)
  {
    MAP_amp = maxAmp;
    MAP_index = maxLoc;
    MAP = osc_data[MAP_index][0];

    // Try convex fit for MAP and compare with old algorithm.

    calc_dias_inflate(osc_data, num_levels);

    return(1);
  }
  else
  {
    return(0);
  }
}


void calc_dias_inflate(float osc_data[MAX_NUM_LEVELS][2],int num_levels)
{
    
    int i,iter,max_iter;
    float x_scale[MAX_NUM_LEVELS],a_scale[MAX_NUM_LEVELS];
    double a,mu,sigma;
    double a_last,mu_last,sigma_last,eps,chisq,chisq_last,resid;
    double deriv_a,deriv_mu,deriv_sigma;
#ifdef DEBUG_DATA_GATHERING
    double a_data[20],mu_data[20],sigma_data[20],chisq_data[20];
#endif
    double x;
    
    // Scale the data. 
    for(i = 0; i < MAP_index; i++)
    {
      x_scale[i] = 1.0 - (osc_data[i][0]/MAP);
      a_scale[i] = osc_data[i][1]/MAP_amp;
    }

    sprintf(BluetoothMsg,"MAP_index = %d  num_levels = %d\n",MAP_index,num_levels);
    LF_SendToBluetooth(BluetoothMsg);
    LF_SendToBluetooth("====================================\r\n");
    LF_SendToBluetooth("diastolic pressure determination\n");
    
    for(i = 0; i < MAP_index; i++)
    {
        sprintf(BluetoothMsg,"x = %f   a = %f\n",x_scale[i],a_scale[i]);
        LF_SendToBluetooth(BluetoothMsg);
    }
        
    LF_SendToBluetooth("====================================\r\n");        
    
    a = 1.0;
    a_last = 1.0;
    mu = 0.2;
    mu_last = mu;
    sigma = 0.075;
    sigma_last = sigma;
    iter = 0;
    max_iter = 200;
    eps = 1.0;
    chisq = 1000;
    chisq_last = chisq;
    
    while(iter < max_iter && eps >= 1.0e-5)
    {
        
        chisq = 0.0;
        deriv_a = 0.0;
        deriv_mu = 0.0;
        deriv_sigma = 0.0;
        
        for(i = 0;i < MAP_index; i++)
        {
            resid = a_scale[i]-a_last-(1.0/M_PI)*atan((mu_last-x_scale[i])/sigma_last);
            chisq += resid*resid;
            deriv_a += -2.0*resid;
            deriv_mu += -2.0*resid*(sigma_last/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
            deriv_sigma += -2.0*resid*(-(mu_last-x_scale[i])/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
        }
        
        a = a_last -0.01*deriv_a;
        mu = mu_last - 0.01*deriv_mu;
        sigma = sigma_last - 0.01*deriv_sigma;
        
#ifdef DEBUG_DATA_GATHERING
        if (iter < 20)
        {
            chisq_data[iter] = chisq;
            a_data[iter] = a;
            mu_data[iter] = mu;
            sigma_data[iter] = sigma;
        }
#endif
        
        eps = fabs(chisq -chisq_last)/chisq_last;
        
        // Check if chi squared is increasing and exit the loop if it is.
        
        if (chisq > chisq_last || sigma < 0)
        {
            a = a_last;
            mu = mu_last;
            sigma = sigma_last;
            chisq = chisq_last;
            iter = 1000;  // This value is diagnostic for this loop exit.
            break;
        }
        
        a_last = a;
        mu_last = mu;
        sigma_last = sigma;
        chisq_last = chisq;
        
        iter++;
    }
    
    LF_SendToBluetooth("\r\n");
    sprintf(BluetoothMsg,"iter = %d\n",iter);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"chisq = %lf\n",chisq);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"a = %lf\n",a);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"mu = %lf\n",mu);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"sigma = %lf\n",sigma);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"MAP = %f\n",MAP);
    LF_SendToBluetooth(BluetoothMsg);

    if (iter <= 2)
    {
      dias_press = (float) (1.0 - mu)*MAP;
    }
    else 
    {
      x = binary_chop(a,mu,sigma,0.66);
      dias_press = (float) (1.0 - x)*MAP;
    }

    sprintf(BluetoothMsg,"diastolic pressure = %f\n",dias_press);
    LF_SendToBluetooth(BluetoothMsg);
    LF_SendToBluetooth("\r\n");
}


void calc_dias_deflate(float osc_data[MAX_NUM_LEVELS][2],int num_levels)
{
    
    int i,iter,max_iter,counter;
    float x_scale[MAX_NUM_LEVELS],a_scale[MAX_NUM_LEVELS];
    double a,mu,sigma,convergence;
    double a_last,mu_last,sigma_last,chisq,chisq_last,resid;
    double deriv_a,deriv_mu,deriv_sigma;
    double delta_a,delta_mu,delta_sigma;
#ifdef DEBUG_DATA_GATHERING
    double a_data[20],mu_data[20],sigma_data[20],chisq_data[20];
#endif
    double x;
    
    // Scale the data. 

    counter = 0;
    
    for(i = 0; i < num_levels-MAP_index; i++)
    {
      x_scale[counter] = 1.0 - (osc_data[i+MAP_index][0]/MAP);
      a_scale[counter] = osc_data[i+MAP_index][1]/MAP_amp;
      counter++;
    }

    sprintf(BluetoothMsg,"MAP_index = %d  num_levels = %d\n",MAP_index,num_levels);
    LF_SendToBluetooth(BluetoothMsg);
    LF_SendToBluetooth("====================================\r\n");
    LF_SendToBluetooth("diastolic pressure determination\n");
    for(i = 0; i < counter; i++)
    {
      sprintf(BluetoothMsg,"x = %f   a = %f\n",x_scale[i],a_scale[i]);
      LF_SendToBluetooth(BluetoothMsg);
      
    }
        
    LF_SendToBluetooth("====================================\r\n");      
    
    a = 0.5;
    a_last = 0.5;
    mu = 0.2;
    mu_last = mu;
    sigma = 0.075;
    sigma_last = sigma;
    iter = 0;
    max_iter = 1000;
    convergence = 1.0;
    chisq = 1000;
    chisq_last = chisq;
    
    while(iter < max_iter && convergence >= 1.0e-6)
    {
        
        chisq = 0.0;
        deriv_a = 0.0;
        deriv_mu = 0.0;
        deriv_sigma = 0.0;
        
        for(i = 0;i < counter; i++)
        {
            resid = a_scale[i]-a_last-(1.0/M_PI)*atan((mu_last-x_scale[i])/sigma_last);
            chisq += resid*resid;
            deriv_a += -2.0*resid;
            deriv_mu += -2.0*resid*(sigma_last/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
            deriv_sigma += -2.0*resid*(-(mu_last-x_scale[i])/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
        }
      
        a = a_last - 0.01*deriv_a;
        mu = mu_last - 0.01*deriv_mu;
        sigma = sigma_last - 0.01*deriv_sigma;

#ifdef DEBUG_DATA_GATHERING
        if (iter < 20){
            chisq_data[iter] = chisq;
            a_data[iter] = a;
            mu_data[iter] = mu;
            sigma_data[iter] = sigma;
        }
#endif
        
        // Check if chi squared is increasing and exit the loop if it is.
        
        if (chisq > chisq_last || sigma < 0)
        {
            a = a_last;
            mu = mu_last;
            sigma = sigma_last;
            chisq = chisq_last;
            iter = (2 * max_iter);  // This value is diagnostic for this loop exit.
            break;
        }

	delta_a = fabs(a - a_last)/a_last;
	delta_mu = fabs(mu - mu_last)/mu_last;
	delta_sigma = fabs(sigma - sigma_last)/sigma_last;
        
	convergence = delta_a;
	if(delta_mu > convergence) 
        {
          convergence = delta_mu;
        }
	if(delta_sigma > convergence)
        {
          convergence = delta_sigma;
        }

        a_last = a;
        mu_last = mu;
        sigma_last = sigma;
        chisq_last = chisq;
        
        iter++;
    }

  
    LF_SendToBluetooth("\r\n");
    
    sprintf(BluetoothMsg,"iter = %d\r\n",iter);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"chisq = %lf\r\n",chisq);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"a = %lf\r\n",a);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"mu = %lf\r\n",mu);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"sigma = %lf\r\n",sigma);
    LF_SendToBluetooth(BluetoothMsg);
    sprintf(BluetoothMsg,"MAP = %f\r\n",MAP);
    LF_SendToBluetooth(BluetoothMsg);
    if (iter <= 2)
    {
       dias_press = (float) (1.0 - mu)*MAP;
    }
    else 
    {
      x = binary_chop(a,mu,sigma,0.72);
      dias_press = (float) (1.0 - x)*MAP;
    }

    sprintf(BluetoothMsg,"diastolic pressure = %f\n",dias_press);
    LF_SendToBluetooth(BluetoothMsg);

    if(deflating)
    {
      dias_on_deflate = 1;
    }

    LF_SendToBluetooth("\r\n");

    if(chisq > 1)
    {
      LF_SendToBluetooth("rejection diastolic fit\n\r");
      dias_press = 0.0;
    }
}


void calc_sys_inflate(float osc_data[MAX_NUM_LEVELS][2], int num_levels)
{
    
    int i,iter,max_iter;
    float x_scale[MAX_NUM_LEVELS],a_scale[MAX_NUM_LEVELS];
    double a,mu,sigma,x;
    double a_last,mu_last,sigma_last,eps,chisq,chisq_last,resid;
    double deriv_a,deriv_mu,deriv_sigma;
#ifdef DEBUG_DATA_GATHERING
    double a_data[20],mu_data[20],sigma_data[20],chisq_data[20];
#endif    
    // Scale the data.

 
    x_scale[0] = 0.0;
    a_scale[0] = 1.0;
    for(i = 1;i < num_levels-MAP_index; i++)
    {
      x_scale[i] = (osc_data[i+MAP_index][0]-MAP)/MAP;
      a_scale[i] = osc_data[i+MAP_index][1]/MAP_amp;
    }
    
    
    LF_OpenAppendFileAndBLE(&fp,LogFileName,Separator);
   
    LF_OpenAppendFileAndBLE(&fp,LogFileName,
                        "In systolic determination.\r\n");
    
    for(i = 0; i<num_levels-MAP_index; i++)
    {
        sprintf(BluetoothMsg,"x = %f   a = %f\n",x_scale[i],a_scale[i]);
        LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    }
        
    LF_OpenAppendFileAndBLE(&fp,LogFileName,Separator);     
    
    a = 1.0;
    a_last = 1.0;
    mu = 0.55;
    mu_last = mu;
    sigma = 0.25;
    sigma_last = sigma;
    iter = 0;
    max_iter = 1000;
    eps = 1.0;
    chisq = 1000;
    chisq_last = chisq;
    
    while(iter < max_iter && eps >= 1.0e-5)
    {
        
        chisq = 0.0;
        deriv_a = 0.0;
        deriv_mu = 0.0;
        deriv_sigma = 0.0;
        
        for(i = 0;i < num_levels-MAP_index; i++)
        {
            resid = a_scale[i]-a_last-(1.0/M_PI)*atan((mu_last-x_scale[i])/sigma_last);
            chisq += resid*resid;
            deriv_a += -2.0*resid;
            deriv_mu += -2.0*resid*(sigma_last/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
            deriv_sigma += -2.0*resid*(-(mu_last-x_scale[i])/(M_PI*(pow(sigma_last,2)+pow(mu_last-x_scale[i],2))));
        }
        
        a = a_last -0.01*deriv_a;
        mu = mu_last - 0.01*deriv_mu;
        sigma = sigma_last - 0.01*deriv_sigma;
        
#ifdef DEBUG_DATA_GATHERING
        if (iter < 20)
        {
            chisq_data[iter] = chisq;
            a_data[iter] = a;
            mu_data[iter] = mu;
            sigma_data[iter] = sigma;
        }
#endif
        
        eps = fabs(chisq -chisq_last)/chisq_last;
        
        // Check is chi squared is increasing and exit the loop if it is.
        
        if (chisq > chisq_last || sigma < 0)
        {
            a = a_last;
            mu = mu_last;
            sigma = sigma_last;
            chisq = chisq_last;
            iter = 2000;  // This value is diagnostic for this loop exit.
            break;
        }
        
        a_last = a;
        mu_last = mu;
        sigma_last = sigma;
        chisq_last = chisq;
        
        iter++;
    }

    x = binary_chop(a,mu,sigma,0.33);

    sys_press = (float) (1 + x)*MAP;
  
    LF_OpenAppendFileAndBLE(&fp,LogFileName,"\r\n");
    sprintf(BluetoothMsg,"iter = %d\r\n",iter);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg,"chisq = %lf\r\n",chisq);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg,"a = %lf\r\n",a);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg,"mu = %lf\r\n",mu);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg,"sigma = %lf\r\n",sigma);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg,"MAP = %f\r\n",MAP);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    sprintf(BluetoothMsg,"systolic pressure = %f\r\n",sys_press);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
    LF_SendToBluetooth("\r\n");
    LF_OpenAppendFileAndBLE(&fp,LogFileName,"\r\n");
}

float butterlp(float data)
{
 /* Floating point filter in time domain. */
    static float a[3] = {1.0,-1.6707, 0.7173};
    static float b[3] = {0.117571,0.235142,0.117571};
    static float x[2] = {0.0,0.0};
    static float y[2] = {0.0,0.0};
    float value= 0.0;
    int i=0;
    
    value = b[0] * data;
    for(i = 0; i < 2; i++)
    {
        value += b[i+1]*x[i];
    }
        
    for(i = 0; i < 2; i++)
    {
        value -= a[i+1]*y[i];
    }
        
    x[1] = x[0];
    y[1] = y[0];
    x[0] = data;
    y[0] = value;
    
    return(value/10.0);   // Divide out digital filter gain.
}

void unit_RIC_abort(void)
{

  int i;
  PumpPwm__Stop();
  ValvePwm__Stop();

  strcpy(line1,"Aborted by Unit ");
  strcpy(line2,"                ");
  LF_LCD_Control(line1,line2);

  for(i = 0; i < 24; i++)
  {
    LED_state[i] = -1;
  }

  LF_LED_Control(LED_state);
  sprintf(BluetoothMsg,"RIC inflation aborted, duration > 6 minutes.\r\n");
  LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  //timer.detach();
  //timer2.detach();
  tx_timer_deactivate(&DataAcquire_Timer);
  tx_timer_deactivate(&LedFlasher_Timer);
  
  LF_GPIO_DisableBoost();
  LF_GPIO_DisableAnalogSupply();
}

void DurationTimer(ULONG param)
{
  if(RIC_inflation)
  {
     unit_RIC_abort();
  }
}

void flashy(ULONG param)
{
  
  HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
  
  static int counter = 0;
  static int bcounter = 0;
  static int mult = -1;

  if(RIC_inflation)
  {
    if(counter < 60)
    {
      mult *= -1;      // flips leds on and off
      LED_state[19] = mult;
      LED_state[20] = 1;
      LED_state[21] = 1;
      LED_state[22] = 1;
      LED_state[23] = 1;
      LF_LED_Control(LED_state);
      counter++;
      LF_LCD_Control(line1,line2);
      return;
    }
    if(counter < 120)
    {
      mult *= -1;
      LED_state[19] = -1;
      LED_state[20] = mult;
      LED_state[21] = 1;
      LED_state[22] = 1;
      LED_state[23] = 1;
        LF_LED_Control(LED_state);
      counter++;
      LF_LCD_Control(line1,line2);
      return;
    }
    if(counter < 180)
    {
      mult *= -1;
      LED_state[19] = -1;
      LED_state[20] = -1;
      LED_state[21] = mult;
      LED_state[22] = 1;
      LED_state[23] = 1;
        LF_LED_Control(LED_state);
      counter++;
      LF_LCD_Control(line1,line2);
      return;
    }
    if(counter < 240)
    {
      mult *= -1;
      LED_state[19] = -1;
      LED_state[20] = -1;
      LED_state[21] = -1;
      LED_state[22] = mult;
      LED_state[23] = 1;
        LF_LED_Control(LED_state);
      counter++;
      LF_LCD_Control(line1,line2);
      return;
    }
    if(counter < 300)
    {
      mult *= -1;
      LED_state[19] = -1;
      LED_state[20] = -1;
      LED_state[21] = -1;
      LED_state[22] = -1;
      LED_state[23] = mult;
        LF_LED_Control(LED_state);
      counter++;
      LF_LCD_Control(line1,line2);
      return;
    }
    if(counter == 600)
    {
      done_flashing = 1;
     LF_LCD_Control(line1,line2);
    }
  }

  if(RIC_deflation)
  {
    if(bcounter < 60)
    {
      mult *= -1;
      LED_state[18] = mult;
      LED_state[17] = 1;
      LED_state[16] = 1;
      LED_state[15] = 1;
      LED_state[14] = 1;
      LF_LED_Control(LED_state);
      bcounter++;
      LF_LCD_Control(line1,line2);
      return;
    }
    if(bcounter < 120)
    {
      mult *= -1;
      LED_state[18] = -1;
      LED_state[17] = mult;
      LED_state[16] = 1;
      LED_state[15] = 1;
      LED_state[14] = 1;
      LF_LED_Control(LED_state);
      bcounter++;
      LF_LCD_Control(line1,line2);
      return;
    }
    if(bcounter < 180)
    {
      mult *= -1;
      LED_state[18] = -1;
      LED_state[17] = -1;
      LED_state[16] = mult;
      LED_state[15] = 1;
      LED_state[14] = 1;
      LF_LED_Control(LED_state);
      bcounter++;
      LF_LCD_Control(line1,line2);
      return;
    }
    if(bcounter < 240)
    {
      mult *= -1;
      LED_state[18] = -1;
      LED_state[17] = -1;
      LED_state[16] = -1;
      LED_state[15] = mult;
      LED_state[14] = 1;
      LF_LED_Control(LED_state);
      bcounter++;
      LF_LCD_Control(line1,line2);
      return;
    }
    if(bcounter < 300)
    {
      mult *= -1;
      LED_state[18] = -1;
      LED_state[17] = -1;
      LED_state[16] = -1;
      LED_state[15] = -1;
      LED_state[14] = mult;
      LF_LED_Control(LED_state);
      bcounter++;
      LF_LCD_Control(line1,line2);
      return;
    }
  }

  if(RIC_inflation == 0 && RIC_deflation == 0)
  {
    counter = 0;
    bcounter = 0;
    done_flashing = 0;
    mult = -1;
  
      LF_LCD_Control(line1,line2);
  }
  
  
  
}
    
void AcquireData(void)
{
 
     
}

void teflon(uint8_t on_time, uint8_t off_time, int num_clicks)
{

  uint8_t i =0;
  const uint16_t timerConversion = 10; 

  ValvePwm_UpdateDutyCycle(DutyCycle_75);

  
  for(i = 0; i < num_clicks; i++)
  {
    ValvePwm__Start();
    tx_thread_sleep(on_time);
    ValvePwm__Stop();
    tx_thread_sleep(off_time);
  }
  return;
}


void update_LCD(uint8_t cycleCount)
{
     
     char string1[20],string2[20];
     char sys_string[4],dias_string[4],MAP_string[4],pulse_string[4];
     char sep_string[]="/";
     //static int call_state = 0;
     char number_string[2];
     int i,string_length;
     
     make_output_string((int) (sys_press+0.5),sys_string);   // round to nearest integer
     make_output_string((int) (MAP+0.5),MAP_string);
     make_output_string((int) (dias_press+0.5),dias_string);
     make_output_string((int) pulse,pulse_string);
    
   
    strcpy(string1,sys_string);
    strcat(string1,sep_string);
    strcat(string1,MAP_string);
    strcat(string1,sep_string);
    strcat(string1,dias_string);
    
    strcat(string1," ");
    strcat(string1," ");

    strcat(string1,pulse_string);

    string_length = strlen(string1);

    for(i=string_length;i<16;i++)
    {
        strcat(string1," ");
    }
    string1[15] = '\0';
        
    strcpy(line1,string1);

    strcpy(string2,"cycle number ");
    number_string[0] = (char) (0x30 + cycleCount);
    number_string[1] = '\0';
    strcat(string2,number_string);
    
    string_length = strlen(string2);
    for(i=string_length;i<16;i++)
    {
        strcat(string2," ");
    }
    string2[15] = '\0';
        
    sprintf(BluetoothMsg,"in update_LCD, line1 = %s, len = %d\n",line1,strlen(line1));
    sprintf(BluetoothMsg,"in update_LCD, line2 = %s, len = %d\n",line2,strlen(line2));
    
    tx_thread_sleep(2000);
    
    return;
}  


void acquire(ULONG param)
{
      bool call = false; 
    int AtoDcount=0;
    int dptr_last=0;
  
    if(!call)
    {
     
    korot_string[0] = ' ';
    korot_string[1] = '\0';
    
    call = true;
    }       
    
    tock++;
   
    pressure = LF__ADS115__ReadPressure();
    
    ADC_data[dptr] = AtoDcount;
    data[dptr] = pressure;  // Write to circular buffer.
    filt_data[dptr] = butterlp(pressure);
    tock_data[dptr] = tock;
     dptr_last = dptr;  //  500 Hz mods next few lines
    dptr++;
    //    dptr &= CIRC_BUF_MASK;
    if(dptr >= 2200) 
    {
      dptr = 0;
    }
    avg_pressure = filt_data[dptr_last]/256 + (avg_pressure*255)/256;

    // Service RIC inflation counter.
    /*
    if(RIC_inflation)
    {
      if(inflation_duration_counter >= 144000)
      {
	unit_RIC_abort();
      }
      else
      {
	inflation_duration_counter++;
      }
    }
    else
    {
      inflation_duration_counter = 0;
    }*/
  
  
  
  
  //HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
  
}

static void Init_Sequence(void)
{
    batt_level = LF__ADS115__ReadBattVoltage();
   
    if(batt_level < 1.5)
    {
      LF__ST7036__LCDclear();
      LF__ST7036__LCDwrite("Analog PWR FAIL",0);  
      for(;;);
      
    }
  
    /* Create Led Flash Timer */
    tx_timer_create (&LedFlasher_Timer, "LedFlash_Timer", flashy,
    0x1234, 1000, 1000,
    TX_NO_ACTIVATE);
    
    
    tx_timer_create (&DataAcquire_Timer, "DataAquire_Timer", acquire,
     0x1234, 2, 2,
     TX_AUTO_ACTIVATE);
    
 
    LF_OpenLifeFile(&fp);
    LF_GetLogFileName(LogFileName);
    
    sprintf(BluetoothMsg,"Batery Level = %f\n\r",batt_level);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,BluetoothMsg);
  
  
    strcpy(line1," V batt =       ");
    sprintf(line2," %8.4f  ",batt_level);
    
    LF_OpenAppendFileAndBLE(&fp,LogFileName,line1);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,line2);
    LF_LCD_Control(line1,line2);
  
    //tx_thread_sleep(5000);
    
    teflon(250,250,5);
    
    memset(LED_state,0,24);
    
   
    strcpy(version,"   Proto19-RC3 ");
    strcpy(date,"  March. 5, 2019   ");
    LF_LCD_Control(version,date);
    
    sprintf(BluetoothMsg,"%s\n\r",version);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,version);
    sprintf(BluetoothMsg,"%s\n\r",date);
    LF_OpenAppendFileAndBLE(&fp,LogFileName,date);
    
    tx_thread_sleep(1000);
    
    strcpy(line1,"LifeCuff Tech tm");
    strcpy(line2,"    READY       ");   
    LF_LCD_Control(line1,line2);
  
  
  
}


