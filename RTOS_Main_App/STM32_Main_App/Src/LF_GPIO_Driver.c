#include "LF_GPIO_Driver.h"
#include "LF_LCD_Driver.h"
#include "LF_PWM_Driver.h"


uint8_t SwitchMode[3]={0};
uint8_t SwitchModeResult = 0;


/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void LF_GPIO_Init(void)
{

    GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, Boost_EN_PWR_Pin|SD_CD_Pin|INT_1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, VCC_A2_EN_PWR_Pin|LED_OE_Pin|CPU_CPU_IO_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, SPI1_CS_Pin|BL_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : Boost_EN_PWR_Pin SD_CD_Pin INT_1_Pin */
  GPIO_InitStruct.Pin = Boost_EN_PWR_Pin|SD_CD_Pin|INT_1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : CNTR_Pump_Control_Pin */
  GPIO_InitStruct.Pin = CNTR_Pump_Control_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
  HAL_GPIO_Init(CNTR_Pump_Control_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : VCC_A2_EN_PWR_Pin LED_OE_Pin CPU_CPU_IO_Pin */
  GPIO_InitStruct.Pin = VCC_A2_EN_PWR_Pin|LED_OE_Pin|CPU_CPU_IO_Pin|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : D_SW3_Dip_Switch_Pin */
  GPIO_InitStruct.Pin = D_SW3_Dip_Switch_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(D_SW3_Dip_Switch_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : CNTR_V1_Valve_Control_Pin */
  GPIO_InitStruct.Pin = CNTR_V1_Valve_Control_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
  HAL_GPIO_Init(CNTR_V1_Valve_Control_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : SPI1_CS_Pin BL_LED_Pin */
  GPIO_InitStruct.Pin = SPI1_CS_Pin|BL_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

   /*Configure GPIO pins : PC10 PC11 PC12 */
  GPIO_InitStruct.Pin = SW_Start|SW_Util1|SW_Util2;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins :  PB6 PB7 */
  
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  
  /*Configure GPIO pins : D_SW1_Dip_Switch_Pin D_SW2_Dip_Switch_Pin */
  GPIO_InitStruct.Pin = D_SW1_Dip_Switch_Pin|D_SW2_Dip_Switch_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  
 
 
}


static uint8_t LF_GPIO_SwitchOneOn(void);
static uint8_t LF_GPIO_SwitchTwoOn(void);
static uint8_t LF_GPIO_SwitchThreeOn(void);


 /**
* @brief  Enable Boost
*/

void LF_GPIO_EnableBoost(void)
{
    HAL_GPIO_WritePin(GPIOC, Boost_EN_PWR_Pin, GPIO_PIN_SET);
}

 /**
* @brief  Disable Boost
*/
void LF_GPIO_DisableBoost(void)
{
  HAL_GPIO_WritePin(GPIOC, Boost_EN_PWR_Pin, GPIO_PIN_RESET);
  
}

 /**
* @brief  Enable Analog Supply
*/
void LF_GPIO_EnableAnalogSupply(void)
{
  HAL_GPIO_WritePin(GPIOA, VCC_A2_EN_PWR_Pin, GPIO_PIN_SET);
}


 /**
* @brief  Disable Analog Supply
*/
void LF_GPIO_DisableAnalogSupply(void)
{
  HAL_GPIO_WritePin(GPIOA, VCC_A2_EN_PWR_Pin, GPIO_PIN_RESET);
}



 /**
* @brief  Disable Analog Supply
*/


 /**
* @brief  Read Switch One State
*/
static uint8_t LF_GPIO_SwitchOneOn(void)
{
   uint8_t value = 0;
   if(HAL_GPIO_ReadPin(GPIOB,D_SW1_Dip_Switch_Pin)==GPIO_PIN_SET)
   {
     value = 1;
   }
   return value; 
   
}
 /**
* @brief  Read Switch Two State
*/
static uint8_t LF_GPIO_SwitchTwoOn(void)
{
  uint8_t value = 0;
   if(HAL_GPIO_ReadPin(GPIOB,D_SW2_Dip_Switch_Pin)==GPIO_PIN_SET)
   {
      value = 1;
   }
   return value;
  
}
 /**
* @brief  Read Switch Three State
*/
static uint8_t LF_GPIO_SwitchThreeOn(void)
{
  uint8_t value = 0;
   if(HAL_GPIO_ReadPin(GPIOA,D_SW3_Dip_Switch_Pin)==GPIO_PIN_SET)
   {
      value = 1;
   }
  return value;
 
}


uint8_t LF_GPIO_ReadModeSwitch(void)
{
  SwitchMode[0]=LF_GPIO_SwitchOneOn() ; 
  SwitchMode[1]=LF_GPIO_SwitchTwoOn() ; 
  SwitchMode[2]=LF_GPIO_SwitchThreeOn() ; 
  SwitchModeResult = (SwitchMode[0]) + (2*SwitchMode[1]) + (4*SwitchMode[2]);
  return SwitchModeResult; 
}



 /**
* @brief  Read Start Switch
*/

bool LF_GPIO_StartSwitch(void)
{
  bool status = true;
   if(HAL_GPIO_ReadPin(GPIOC,SW_Start)==GPIO_PIN_SET)
   {
     status = false;
   }
   return status;
 
}

 /**
* @brief  Read SD Card
*/

bool LF_GPIO_SDCardIO(void)
{
  bool status = true;
   if(HAL_GPIO_ReadPin(SD_CD_GPIO_Port,GPIO_PIN_8)==GPIO_PIN_SET)
   {
     status = false;
   }
   return status;
 
}

void LF_SDCardPresentCheck(void)
{
  char *Msg1 ="Insert SD Card";
  char *Msg2 ="  To Proceed! ";
  bool init = false; 
  
   do
   {
        if(!init)
        {
          LF_LCD_Control(Msg1,Msg2);
          init = true; 
        }
         
   }while(LF_GPIO_SDCardIO());

}
