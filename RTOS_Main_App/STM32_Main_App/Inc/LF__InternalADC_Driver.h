#ifndef __INTERNALADC_H__
#define __INTERNALADC_H__

#include "stm32f4xx_hal.h"

uint16_t LF_ReadADCChannel12 (void);
bool LF_PowerRailsCheck(void);
void LF_PowerUpCheck(void);



#endif // __INTERNALADC_H__