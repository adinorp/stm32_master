
#ifndef __UTILITIES_H__
#define __UTILITIES_H__
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "fatfs.h"

uint32_t DWT_Delay_Init(void);
void DWT_Delay_us(volatile uint32_t microseconds);
void LF_SendToBluetooth(char *msg);
void LF_OpenLifeFile(FIL* fp);
void LF_CloseFile(FIL* fp);
void LF_GetLogFileName(char *filename);
FRESULT LF_OpenAppendFile (FIL* fp,const char* path,const char* data);
FRESULT LF_OpenAppendFileAndBLE (FIL* fp,const char* path,const char* data );
uint32_t LF_GetFileIndex(void);


#endif // __UTILITIES_H__