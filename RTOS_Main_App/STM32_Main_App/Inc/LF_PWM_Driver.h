#ifndef _PWM_DRIVER_H
#define _PWM_DRIVER_H
#include <stdint.h>

enum DutyCycle
{
  /*
    TIM_Period = 84000000 / 15000 - 1 = 5599
    pulse_length = ((TIM_Period + 1) * DutyCycle) / 100 - 1
    
    where DutyCycle is in percent, between 0 and 100%
    
    25% duty cycle:     pulse_length = ((5599 + 1) * 25) / 100 - 1 = 1414
    50% duty cycle:     pulse_length = ((5599 + 1) * 50) / 100 - 1 = 2828
    75% duty cycle:     pulse_length = ((5599 + 1) * 75) / 100 - 1 = 4242
    100% duty cycle:    pulse_length = ((5599 + 1) * 100) / 100 - 1 = 5656
 */
  
  DutyCycle_25  = 1414, 
  DutyCycle_50  = 2828,
  DutyCycle_75  = 4242,
  DutyCycle_100 = 5656,
 
};


void PumpPwm__Init(void);
void ValvePwm__Init(void);
void PumpPwm__Start(void);
void ValvePwm__Start(void);
void PumpPwm__Stop(void);
void ValvePwm__Stop(void);
void PumpPwm_UpdateDutyCycle(uint16_t dutyCycle);
void ValvePwm_UpdateDutyCycle(uint16_t dutyCycle);

#endif