/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/
#include   "tx_api.h"
#include <stdint.h>
#include <stdbool.h>
/* Private define ------------------------------------------------------------*/

#define Boost_EN_PWR_Pin GPIO_PIN_13
#define Boost_EN_PWR_GPIO_Port GPIOC
#define CNTR_Pump_Control_Pin GPIO_PIN_0
#define CNTR_Pump_Control_GPIO_Port GPIOA
#define VCC_A2_EN_PWR_Pin GPIO_PIN_1
#define VCC_A2_EN_PWR_GPIO_Port GPIOA
#define D_SW3_Dip_Switch_Pin GPIO_PIN_4
#define D_SW3_Dip_Switch_GPIO_Port GPIOA
#define CNTR_V1_Valve_Control_Pin GPIO_PIN_0
#define CNTR_V1_Valve_Control_GPIO_Port GPIOB
#define SPI1_CS_Pin GPIO_PIN_1
#define SPI1_CS_GPIO_Port GPIOB
#define BL_LED_Pin GPIO_PIN_2
#define BL_LED_GPIO_Port GPIOB
#define SD_CD_Pin GPIO_PIN_8
#define SD_CD_GPIO_Port GPIOC
#define INT_1_Pin GPIO_PIN_9
#define INT_1_GPIO_Port GPIOC
#define LED_OE_Pin GPIO_PIN_11
#define LED_OE_GPIO_Port GPIOA
#define CPU_CPU_IO_Pin GPIO_PIN_15
#define CPU_CPU_IO_GPIO_Port GPIOA
#define SW_Start_Pin GPIO_PIN_10
#define SW_Start_GPIO_Port GPIOC
#define SW_Util1_Pin GPIO_PIN_11
#define SW_Util1_GPIO_Port GPIOC
#define SW_Util2_Pin GPIO_PIN_12
#define SW_Util2_GPIO_Port GPIOC
#define D_SW1_Dip_Switch_Pin GPIO_PIN_8
#define D_SW1_Dip_Switch_GPIO_Port GPIOB
#define D_SW2_Dip_Switch_Pin GPIO_PIN_9
#define D_SW2_Dip_Switch_GPIO_Port GPIOB
#define SD_CS_Pin GPIO_PIN_1
#define SD_CS_GPIO_Port GPIOB


#define     EVENT_STACK_SIZE       4096
#define     DATAACQUIRE_STACK_SIZE       2048 
#define     APP_STACK_SIZE         1024
#define     APP_BYTE_POOL_SIZE     9120
#define     APP_BLOCK_POOL_SIZE    100
#define     APP_QUEUE_SIZE         100
    
    
 extern TX_THREAD               EventThread;   
    
    
    
    
extern TX_MUTEX                mutex_0;
extern TX_SEMAPHORE            semaphore_0;
   
    
//Mail type values
typedef enum 
{
	NULL_MSG = 0,
	START_MSG =0x55,
	ABORT_MSG =0xaa,
	END_OF_EVENTS	
} Event_e;



    
    
    
    
 
/* Define the ThreadX object control blocks...  */


void    EventThread_entry(ULONG thread_input);
void    DataAcquireThread_entry(ULONG thread_input);
void    ProcessInputThread_entry(ULONG thread_input);
void    NetworkManagerThread_entry(ULONG thread_input);
void    LF__SendMailToEventThread(Event_e Event);



/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
