#ifndef _GPIO_DRIVER_H
#define _GPIO_DRIVER_H

#include "stm32f4xx_hal.h"
#include <stdbool.h>
#include <stdint.h>

#define Boost_EN_PWR_Pin GPIO_PIN_13
#define Boost_EN_PWR_GPIO_Port GPIOC
#define CNTR_Pump_Control_Pin GPIO_PIN_0
#define CNTR_Pump_Control_GPIO_Port GPIOA
#define VCC_A2_EN_PWR_Pin GPIO_PIN_1
#define VCC_A2_EN_PWR_GPIO_Port GPIOA
#define D_SW3_Dip_Switch_Pin GPIO_PIN_4
#define D_SW3_Dip_Switch_GPIO_Port GPIOA
#define CNTR_V1_Valve_Control_Pin GPIO_PIN_0
#define CNTR_V1_Valve_Control_GPIO_Port GPIOB
#define SPI1_CS_Pin GPIO_PIN_1
#define SPI1_CS_GPIO_Port GPIOB
#define BL_LED_Pin GPIO_PIN_2
#define BL_LED_GPIO_Port GPIOB
#define SD_CD_Pin GPIO_PIN_8
#define SD_CD_GPIO_Port GPIOC
#define INT_1_Pin GPIO_PIN_9
#define INT_1_GPIO_Port GPIOC
#define LED_OE_Pin GPIO_PIN_11
#define LED_OE_GPIO_Port GPIOA
#define CPU_CPU_IO_Pin GPIO_PIN_15
#define CPU_CPU_IO_GPIO_Port GPIOA
#define D_SW1_Dip_Switch_Pin GPIO_PIN_8
#define D_SW1_Dip_Switch_GPIO_Port GPIOB
#define D_SW2_Dip_Switch_Pin GPIO_PIN_9
#define D_SW2_Dip_Switch_GPIO_Port GPIOB
#define SW_Start GPIO_PIN_10
#define SW_Util1 GPIO_PIN_11
#define SW_Util2 GPIO_PIN_12


void LF_GPIO_Init(void);
void LF_GPIO_EnableAnalogSupply(void);
void LF_GPIO_DisableAnalogSupply(void);
void LF_GPIO_DisableBoost(void);
void LF_GPIO_EnableBoost(void);
bool LF_GPIO_StartSwitch(void);
bool LF_GPIO_SDCardIO(void);
void LF_SDCardPresentCheck(void);
uint8_t LF_GPIO_ReadModeSwitch(void);

#endif